Other guy opens server
Open via jar
	java -jar HailTheGrail.jar client 131.152.xxx.xxx:9001
	
Open game
	take sweet time with choosing map.
	names, description, different sizes (Dead Man's Chest, Minos Marathon)
	not time for all maps, in fact not even for 1 -> demo map of Unison Raid.
	
let people join
	Blue: Me, Oliver
	Red: Philipp, Patrick
	
show chat (quick)
	global, game, team, whisper
	(be whispered to)
	
explain people GUI
	displays, controls, tooltips
	


DETAIL:
Simon:
	Move till see grail.
Oliver:
	Move down left.
Philipp:
	Move up left.
Patrick:
	Move up.

Simon:
	Place dispel above grail. Move 1 left. End turn.
Oliver:
	Move down, place reveal left of exit to grail, go up left (red goal).
Philipp:
	Walk into reveal; go up right.
Patrick:
	Go up, take grail, walk on dispel trap over pedestal. Punch Simon twice.
	
Simon:
	Kill Patrick, take grail.
Oliver:
	go up right of orange goal, hide.
Philipp:
	go back down, place blind left of exit of grail room. Step left, step up, end turn.
Patrick:
	Go up left.
	
Simon:
	Walk over blind trap; end turn.
Oliver:
	Go back down. Place stun trap on tile down right of orange goal.
Philipp:
	Punch Simon once(!). Place dispel trap, go left.
Patrick:
	Move up.

Simon:
	Step left on dispel, die.
Oliver:
	Move further down, close to Patrick; go into position to get nuked.
Philipp:
	Move right, place poison trap for Simon.
Patrick:
	Place purge trap.
	
Simon:
	Go down, walk over poison. End turn to show damage.
Oliver:
	Kill Patrick and walk over purge.
Philipp:
	place prone trap, go down a bit.
Patrick:
	Go up towards grail.
	
Simon:
	Walk over prone trap, walk towards blue goal (still in reach of Philipp)
Oliver:
	Skip turn.
Philipp:
	Punch Simon once(!).
Patrick:
	Walk up and take grail.
	
Simon:
	Murder Philipp, go next to stun trap.
Oliver:
	Skip turn.
Philipp:
	Skip turn.
Patrick:
	Walk over stun trap bottom right of orange goal.
	
Simon:
	Kill stunned Patrick, take grail.
Oliver:
	Skip turn.
Philipp:
	Skip turn.
Patrick:
	Skip turn.
	
Simon:
	Win game.
