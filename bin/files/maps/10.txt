Dead Man's Chest
A mighty pirate captain once held reign on this small archipelago. Though the pirates are gone, tales of a particular treasure still attract many grail seekers.