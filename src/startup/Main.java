package startup;

import javax.swing.JOptionPane;

import network.Client;
import network.Server;

public class Main {

	public static int port = 0;
	public static String ip = null;
	
	/**
	 * This is the main method, to be executed on startup of the runnable jar.<br>
	 * If no or incomplete command line arguments are given, inputs are requested GUI based.
	 */
	public static void main(String[] args) {
		if (args.length > 0) {
			//parse arguments
			if (args[0].toLowerCase().equals("client") || args[0].toLowerCase().equals("c")) {
				//client path
				if (args.length >= 2) {
					parseIPPort(args[1]);
				}
				Client.main(null);
			} else if (args[0].toLowerCase().equals("server") || args[0].toLowerCase().equals("s")|| args[0].toLowerCase().equals("host")) {
				//server path
				if (args.length >= 2) {
					parsePort(args[1]);
				}
				Server.main(null);
			} else {
				System.err.println("Couldn't parse first argument: \"" + args[0] + "\"");
				System.err.println("Use \"client\" or \"server\".");
				System.exit(0);
			}
			
		} else {
			//run entirely GUI based
			Object[] options = {"Client", "Server"};
            int n = JOptionPane.showOptionDialog(null,
                            "Do you want to launch as client or server?",
                            "Hail The Grail",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            options,
                            options[0]);
            if (n == JOptionPane.YES_OPTION) {
            	//client path
            	Client.main(null);
            } else if (n == JOptionPane.NO_OPTION) {
            	//server path
            	Server.main(null);
            } else {
            	//cancel
                System.exit(0);
            }

		}
	}
	
	
	/**
	 * parses the given string as IPv4 address and port and writes it to Main.ip and Main.port
	 * or terminates the program with error message in standard output on failure.
	 * @param arg argument to parse
	 */
	private static void parseIPPort(String arg) {
		if (arg == null || arg.length() <= 0) {
			//unreachable with correct code.
			System.err.println("Couldn't parse given IP/Port.");
			System.exit(0);
		}
		
		int breakpoint = arg.indexOf(':');
		if (breakpoint <= 0 || breakpoint >= arg.length() - 1) {
			System.err.println("Couldn't parse given IP/Port: " + arg);
			System.err.println("Entry must be of form \"IP:Port\".");
			System.err.println("Example: 127.0.0.1:9001");
			System.exit(0);
		}
		parseIP(arg.substring(0, breakpoint));
		parsePort(arg.substring(breakpoint + 1));
	}
	

	/**
	 * parses the given string as port and writes it to Main.port
	 * or terminates the program with error message in standard output on failure.
	 * @param arg argument to parse
	 */
	private static void parsePort(String arg) {
		if (arg == null || arg.length() <= 0) {
			//unreachable with correct code.
			System.err.println("Couldn't parse given Port.");
			System.exit(0);
		}
		
		try {
			port = Integer.parseInt(arg);
			if (port < 1024) {
				if (port <= 0) {
					System.err.println("Couldn't parse given Port: " + arg);
					System.err.println("Please enter a positive number.");
					System.exit(0);
				} else {
					System.out.println("Warning: Use of System Ports is highly discouraged.");
				}
			}
		} catch (NumberFormatException e) {
			System.err.println("Couldn't parse given Port: " + arg);
			System.err.println("Please enter a valid positive number.");
			System.exit(0);
		}
	}

	
	/**
	 * parses the given string as IP and writes it to Main.ip
	 * or terminates the program with error message in standard output on failure.
	 * @param arg argument to parse
	 */
	private static void parseIP(String arg) {
		if (arg == null || arg.length() <= 0) {
			//unreachable with correct code.
			System.err.println("Couldn't parse given IP.");
			System.exit(0);
		}
		
		if (arg.toLowerCase().equals("localhost")) {
			ip = "127.0.0.1";
		} else {
			int[] breakpoints = new int[3];
			int startIndex = 0;
			for (int i = 0; i < breakpoints.length; i++) {
				breakpoints[i] = arg.indexOf('.', startIndex);
				if (breakpoints[i] <= startIndex) {
					System.err.println("Couldn't parse given IP: " + arg);
					System.err.println("(invalid seperation/amount of numbers)");
					System.err.println("Please enter a valid IPv4 address and port.");
					System.err.println("Example: 127.0.0.1:9001");
					System.exit(0);
				}
				startIndex = breakpoints[i]+1;
			}
			int[] entries = new int[4];
			try {
				entries[0] = Integer.parseInt(arg.substring(0, breakpoints[0]));
				entries[1] = Integer.parseInt(arg.substring(breakpoints[0]+1, breakpoints[1]));
				entries[2] = Integer.parseInt(arg.substring(breakpoints[1]+1, breakpoints[2]));
				entries[3] = Integer.parseInt(arg.substring(breakpoints[2]+1));
			} catch (NumberFormatException e) {
				System.err.println("Couldn't parse given IP: " + arg);
				System.err.println("(invalid numbers)");
				System.err.println("Please enter a valid IPv4 address and port.");
				System.err.println("Example: 127.0.0.1:9001");
				System.exit(0);
			}
			for (int i = 0; i < entries.length; i++) {
				if (entries[i] < 0 || entries[i] > 255) {
					System.err.println("Couldn't parse given IP: " + arg);
					System.err.println("(number out of bounds at number " + i + ": " + entries[i] + ")");
					System.err.println("Please enter a valid IPv4 address and port.");
					System.err.println("Example: 127.0.0.1:9001");
					System.exit(0);
				}
			}
			
			ip = entries[0] + "." + entries[1] + "." + entries[2] + "." + entries[3];
		}
	}
}
