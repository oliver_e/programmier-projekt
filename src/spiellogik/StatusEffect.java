package spiellogik;

public class StatusEffect {

	public static final int POISONED = 1;
	public static final int REVEALED = 2;
	public static final int BLINDED = 3;
	public static final int SLOWED = 4;
	public static final int STUNNED = 5;
	public static final int PRONE = 6;
	
	private Knight target;
	private int type;
	private int duration;
	
	/**
	 * 
	 * @param target
	 * the affected Knight.
	 * @param type
	 * accepts StatusEffect.POISONED, REVEALED, BLINDED, SLOWED, STUNNED.
	 * @param duration
	 * amount of turns the effect stays active. Must be >1.
	 */
	public StatusEffect (Knight target, int type, int duration) {
		this.target = target;
		this.type = type;
		this.duration = duration;
	}
	/**
	 * Applies status effects that fire at the start of a turn.
	 */
	public void tickFirst() {
		switch (type) {		
		case REVEALED:
			/*
			 * give away position; will probably be moved to be used
			 * with hasEffect(StatusEffect.REVEALED) somewhere else.
			 */
			break;
			
		case BLINDED:
			/*
			 * reduce vision; will probably be moved to be used
			 * with hasEffect(StatusEffect.BLINDED) somewhere else.
			 */
			break;
			
		case STUNNED:
			target.addAction(-10);
			break;

		default:
			break;
		}
	}
	
	/**
	 * Applies status effects that fire at the end of a turn and reduces timer.
	 */
	public void tickLast() {
		switch (type) {
		case POISONED:
			target.addLife(-10, true);
			target.getMap().sendToNearbyPlayers(HTGEvent.DAMAGE_POISON, target, target.getX(), target.getY(), 5.25);
			break;
			
		default:
			break;
		}
		
		duration -= 1;
		if (duration <= 0) {
			remove();
		}
		
	}

	/**
	 * removes the effect at the end of the Knight's effect application.
	 */
	public void remove() {
		target.removeEffect(this);
	}
	
	/**
	 * Returns the duration left on the effect in turns.
	 */
	public int getDuration() {
		return duration;
	}
	
	/**
	 * @return
	 * may return StatusEffect.POISONED, REVEALED, BLINDED, SLOWED, STUNNED.
	 */
	public int getType() {
		return type;
	}
	
}
