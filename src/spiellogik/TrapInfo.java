package spiellogik;

public class TrapInfo {
	
	public static final byte TEAM1 = 0;
	public static final byte TEAM2 = 64;
	public static final byte POISON = 1;
	public static final byte REVEAL = 2;
	public static final byte BLIND = 3;
	public static final byte SLOW = 4;
	public static final byte STUN = 5;
	public static final byte DISPEL = 6;
	public static final byte PURGE = 7;
	public static final byte PRONE = 8;
	public static final byte POISON_COST = 4;
	public static final byte REVEAL_COST = 1;
	public static final byte BLIND_COST = 3;
	public static final byte SLOW_COST = 3;
	public static final byte STUN_COST = 5;
	public static final byte DISPEL_COST = 4;
	public static final byte PURGE_COST = 10;
	public static final byte PRONE_COST = 5;
	
	/*
	 * Example: Stun Trap placed by Team 2 = TEAM2 + STUN
	 */
	
	/**
	 * Applies a trap. Does nothing when the trap belongs to the target's team. Call when a trap is stepped on.
	 * @param target
	 * the Knight that stepped on the trap.
	 * @param trapType
	 * the numerical type of the trap. Includes team alignment.
	 * @return
	 * true if trap was set off. false if trap wasn't triggered (same team/wrong id).
	 */
	public static boolean trigger(Knight target, byte trapType){
		if (trapType > TEAM1 && trapType < TEAM2){
			//team 1
			if (target.getTeam() != 1){
				return apply(target, (byte) (trapType-TEAM1)); //returns true is successful
			} else {
				return false; //same team
			}
		} else if (trapType > TEAM2) {
				if (target.getTeam() != 2){
					return apply(target, (byte) (trapType-TEAM2)); //returns true is successful
				} else {
					return false; //same team
				}
		}  else {
				return false; // invalid trap type
			}
		}

	
	private static boolean apply(Knight target, byte trapEffect) {
		target.getMap().sendToNearbyPlayers(HTGEvent.TRAP_TRIGGER, target, target.getX(), target.getY(), 10.25);
		switch (trapEffect) {
		case POISON:
			target.getMap().sendToNearbyPlayers(HTGEvent.DAMAGE_POISON, target, target.getX(), target.getY(), 10.25);
			target.addEffect(new StatusEffect(target, StatusEffect.POISONED, 5));
			break;
			
		case REVEAL:
			target.addEffect(new StatusEffect(target, StatusEffect.REVEALED, 10));
			break;
			
		case BLIND:
			target.addEffect(new StatusEffect(target, StatusEffect.BLINDED, 3));
			break;
			
		case SLOW:
			target.addEffect(new StatusEffect(target, StatusEffect.SLOWED, 3));
			break;
			
		case STUN:
			target.addEffect(new StatusEffect(target, StatusEffect.STUNNED, 2));
			break;
			
		case DISPEL:
			target.getMap().sendToNearbyPlayers(HTGEvent.DAMAGE_DISPEL, target, target.getX(), target.getY(), 12.25);
			target.addLife(-40);
			break;
			
		case PURGE:
			target.getMap().sendToSpecificPlayer(HTGEvent.DAMAGE_PURGE_SELF, target, target.getX(), target.getY());
			target.getMap().sendToNearbyPlayers(HTGEvent.DAMAGE_PURGE, target, target.getX(), target.getY(), 20.25, true);
			target.addLife(-100);
			break;
			
		case PRONE:
			target.addEffect(new StatusEffect(target, StatusEffect.PRONE, 4));
			break;

		default:
			return false;
		}
		return true;
	}
}