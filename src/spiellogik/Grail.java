package spiellogik;

public class Grail {
	private int xPos;
	private int yPos;
	private Knight bearer;
	
	
	public Grail(int xPos, int yPos) {
		this.xPos = xPos;
		this.yPos = yPos;
	}
	
	public int getX() {
		return xPos;
	}
	public int getY() {
		return yPos;
	}
	public Knight getBearer() {
		return bearer;
	}
	
	
	public void setBearer(Knight b) {
		bearer = b;
	}
	public void setX(int x) {
		xPos = x;
	}
	public void setY(int y) {
		yPos = y;
	}
	
}
