package spiellogik;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import network.client.MapPicture;
import network.interfaces.ServerInterface;
import network.server.ServerClient;
import network.server.ServerGame;

public class Map {

	public final int MAPSIZEX = 128;
	public final int MAPSIZEY = 128;
	public int team1GoalX = 0;
	public int team1GoalY = 0;
	public int team2GoalX = 0;
	public int team2GoalY = 0;
	public int team1SpawnX = 0;
	public int team1SpawnY = 0;
	public int team2SpawnX = 0;
	public int team2SpawnY = 0;
	
	private final byte[][] mapArray = new byte[MAPSIZEX][MAPSIZEY];
	private final byte[][] trapArray = new byte[MAPSIZEX][MAPSIZEY];
	
	public static final byte UP = 1;
	public static final byte RIGHT = 2;
	public static final byte DOWN = 3;
	public static final byte LEFT = 4;
	
	private final byte COLLISIONTHRESHOLD = 64;
	public final byte VISIONTHRESHOLD = 68;
	private ServerGame serverGame;
	private ServerInterface serverInterface;
	private Game game;
	private Grail grail;
	
	/*
	 * --- When creating a map, use tile IDs >= 1 with IDs < COLLISIONTHRESHOLD being walkable. ---
	 * Also, ID needs to be < VISIONTHRESHOLD to allow vision on tiles beyond itself.
	 */
	
	public Map(ServerInterface serverInterface, ServerGame serverGame, Game game, Grail grail) {
		this.serverInterface = serverInterface;
		this.serverGame = serverGame;
		this.game = game;
		this.grail = grail;
	}
	
	/**
	 * Reads a map from a .htgmap file.
	 * @param serverInterface 
	 * @return true if successful
	 */
	public boolean setUp(int mapID) {
		InputStream resource = ClassLoader.getSystemClassLoader().getResourceAsStream("files/maps/" + mapID + ".htgmap");
		BufferedInputStream mapSteam = new BufferedInputStream (resource);
		byte[] data = new byte[10+MapPicture.MAPSIZEX*MapPicture.MAPSIZEY];
		try {
			for (int j = 0; j < data.length; j++) {
				data[j] = (byte) mapSteam.read();
			}
			mapSteam.close();
		} catch (IOException e2) {}
		
		for (int i = 0; i < data.length - 10; i++) {
			mapArray[i%MAPSIZEX][i/MAPSIZEX] = data[i+10];
		}
		placeGrail(data[0], data[1], grail);
		team1SpawnX = data[2];
		team1SpawnY = data[3];
		team2SpawnX = data[4];
		team2SpawnY = data[5];
		team1GoalX = data[6];
		team1GoalY = data[7];
		team2GoalX = data[8];
		team2GoalY = data[9];
		return true;
	}
	
	public void placeGrail(int xPos, int yPos, Grail grail) {
		grail.setX(xPos);
		grail.setY(yPos);
		grail.setBearer(null);
	}
	
	
	/**
	 * Returns the type of the tile at (x,y).
	 * @param x
	 * @param y
	 * @return type of tile or 0 if failed.
	 */

	public byte getType(int x, int y){
		if ((x < 0 || x >= MAPSIZEX) || (y < 0 || y >= MAPSIZEY)) { //check if out of bound;
			return 0;
		}
		return mapArray[x][y];
	}
	
	/**
	 * Returns the trap type of the tile at (x,y).
	 * @param x
	 * @param y
	 * @return trap type of tile of type TrapInfo.TEAMX + TrapInfo.EFFECT.
	 */
	public byte checkTrap(int x, int y){
		if ((x < 0 || x > MAPSIZEX) || (y < 0 || y > MAPSIZEY)) { //check if out of bound;
			return 0;
		}
		return trapArray[x][y];
	}
	
	/**
	 * Sets the tile's trap value to type.<br>
	 * Must be of type TrapInfo.TEAMX + TrapInfo.EFFECT
	 * @param x
	 * @param y
	 * @param type
	 */
	public void setTrap(int x, int y, byte type){
		trapArray[x][y] = type;
	}
	
	/**
	 * Tries to move a player in the desired direction.
	 * @param target the player to move.
	 * @param direction accepts Map.UP, DOWN, LEFT, RIGHT
	 * @return true if successful, false if failed.
	 */
	public boolean move(Knight target, byte direction) {
		int destX = target.getX();
		int destY = target.getY();
		switch (direction) {
		case UP:
			destY -= 1;
			break;
			
		case DOWN:
			destY += 1;
			break;
			
		case LEFT:
			destX -= 1;
			break;
			
		case RIGHT:
			destX += 1;
			break;

		default:
			return false;
		}
		int cost = target.hasEffect(StatusEffect.SLOWED) ? Knight.MOVE_COST*2 : Knight.MOVE_COST;
		if ((destX == team1GoalX && destY == team1GoalY)) {
			if (target.getTeam() == 1 && target.hasGrail()) {
				if (target.payAction(cost)) {
					serverInterface.endGame(serverGame, 1);
				}
			}
			else return false;
		}
		else if ((destX == team2GoalX && destY == team2GoalY)) {
			if (target.getTeam() == 2 && target.hasGrail()) {
				if (target.payAction(cost)) {
					serverInterface.endGame(serverGame, 2);
				}
			}
			else return false;
		}
		
		else if (isClear(destX, destY)) { // if the destination is accessible
			if (target.payAction(cost)) {
				target.setX(destX);
				target.setY(destY);
				if(grail.getX() == destX && grail.getY() == destY && grail.getBearer() == null) {
					target.setGrail(true);
					grail.setBearer(target);
				} //Important: Check grail before you apply traps.
				if (checkTrap(destX, destY) > 0) {
					boolean triggered = TrapInfo.trigger(target, checkTrap(destX,destY)); //sets off trap if necessary.
					if (triggered) {
						setTrap(destX, destY, (byte) 0);
					}
				}
				return true;
			}
		}
		else {
			Knight opponent = getPlayerCollision(destX, destY);
			if (opponent != null && (opponent.getTeam() != target.getTeam())) {
				target.attack(opponent);
			}
		}
		return false;
	}
	/**
	 * Checks if a given position may be stepped on.
	 */
	private boolean isClear(int x, int y) {
		return (isTerraClear(x,y) && isPlayerClear(x,y));
	}

	/**
	 * Checks if the position has terrain collision.
	 */
	private boolean isTerraClear(int x, int y) {
		int type = getType(x,y);
		if (type >= COLLISIONTHRESHOLD || type == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * ---NOT YET IMPLEMENTED; ALWAYS RETURNS TRUE---<br>
	 * Checks if the position has player collision.
	 */
	private boolean isPlayerClear(int x, int y) {
		if(getPlayerCollision(x, y) == null) {
			return true;
		}
		else return false;
	}
	
	private Knight getPlayerCollision(int x, int y) {
		return game.getPlayerAt(x, y);
	}
	
	/**
	 * Takes coordinates and team from Knight and installs trap at his position in trapArray if the player can afford it.
	 * @param trapper
	 * @param type accepts TrapInfo.POSION, REVEAL, BLIND, SLOW, STUN, DISPEL, PURGE, PRONE.
	 */
	public void installTrap(Knight trapper, int type){
		int x = trapper.getX();
		int y = trapper.getY();
		int teamval = trapper.getTeam() == 1 ? TrapInfo.TEAM1 : TrapInfo.TEAM2;
		switch (type) {
		case TrapInfo.BLIND:
			if (trapper.payAction(TrapInfo.BLIND_COST)){
				setTrap(x, y, (byte) (teamval + type));
				serverInterface.sendEvent(trapper.getSummoner(), HTGEvent.TRAP_PLACE, trapper.getX(), trapper.getY());
			}
			break;
			
		case TrapInfo.DISPEL:
			if (trapper.payAction(TrapInfo.DISPEL_COST)){
				setTrap(x, y, (byte) (teamval + type));
				serverInterface.sendEvent(trapper.getSummoner(), HTGEvent.TRAP_PLACE, trapper.getX(), trapper.getY());
			}
			break;
			
		case TrapInfo.POISON:
			if (trapper.payAction(TrapInfo.POISON_COST)){
				setTrap(x, y, (byte) (teamval + type));
				serverInterface.sendEvent(trapper.getSummoner(), HTGEvent.TRAP_PLACE, trapper.getX(), trapper.getY());
			}
			break;
			
		case TrapInfo.PURGE:
			if (trapper.payAction(TrapInfo.PURGE_COST)){
				setTrap(x, y, (byte) (teamval + type));
				serverInterface.sendEvent(trapper.getSummoner(), HTGEvent.TRAP_PLACE, trapper.getX(), trapper.getY());
			}
			break;
			
		case TrapInfo.REVEAL:
			if (trapper.payAction(TrapInfo.REVEAL_COST)){
				setTrap(x, y, (byte) (teamval + type));
				serverInterface.sendEvent(trapper.getSummoner(), HTGEvent.TRAP_PLACE, trapper.getX(), trapper.getY());
			}
			break;
			
		case TrapInfo.SLOW:
			if (trapper.payAction(TrapInfo.SLOW_COST)){
				setTrap(x, y, (byte) (teamval + type));
				serverInterface.sendEvent(trapper.getSummoner(), HTGEvent.TRAP_PLACE, trapper.getX(), trapper.getY());
			}
			break;
			
		case TrapInfo.STUN:
			if (trapper.payAction(TrapInfo.STUN_COST)){
				setTrap(x, y, (byte) (teamval + type));
				serverInterface.sendEvent(trapper.getSummoner(), HTGEvent.TRAP_PLACE, trapper.getX(), trapper.getY());
			}
			break;
			
		case TrapInfo.PRONE:
			if (trapper.payAction(TrapInfo.PRONE_COST)){
				setTrap(x, y, (byte) (teamval + type));
				serverInterface.sendEvent(trapper.getSummoner(), HTGEvent.TRAP_PLACE, trapper.getX(), trapper.getY());
			}
			break;

		default:
			break;
		}
	}
	
	/**
	 * sends an event notification (animation, sound) to all players near the source
	 * @param event What type of event to notify about.
	 * @param source The Knight that caused the event.
	 * @param knightX The source position X when the event happens
	 * @param knightY The source position Y when the event happens
	 * @param radius How far away it can be heard/seen
	 * @param escapeMe When true does not send to source itself.
	 */
	public void sendToNearbyPlayers(String event, Knight source, int knightX, int knightY, double radius, boolean escapeMe){
		ArrayList<ServerClient> clients = game.getServerGame().getAllClients();
		int x1, x2, y1, y2;
		double dist;
		x2 = knightX;
		y2 = knightY;
		for (ServerClient serverClient : clients) {
			Knight avatar = serverClient.getAvatar();
			if (!escapeMe || avatar != source) {
				x1 = avatar.getX();
				y1 = avatar.getY();
				dist = Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
				if (dist <= radius) {
					serverInterface.sendEvent(serverClient, event, x2, y2);
				}
			}
		}
	}
	
	/**
	 * sends an event notification (animation, sound) to all players near the source
	 * @param event What type of event to notify about.
	 * @param source The Knight that caused the event.
	 * @param knightX The source position X when the event happens
	 * @param knightY The source position Y when the event happens
	 * @param radius How far away it can be heard/seen
	 */
	public void sendToNearbyPlayers(String event, Knight source, int knightX, int knightY, double radius){
		sendToNearbyPlayers(event, source, knightX, knightY, radius, false);
	}
	
	/**
	 * sends an event notification (animation, sound) to one specific player
	 * @param event What type of event to notify about.
	 * @param source The Knight that caused the event.
	 * @param knightX The source position X when the event happens
	 * @param knightY The source position Y when the event happens
	 */
	public void sendToSpecificPlayer(String event, Knight source, int knightX, int knightY){
		serverInterface.sendEvent(source.getSummoner(), event, knightX, knightY);
	}
	
	
}


