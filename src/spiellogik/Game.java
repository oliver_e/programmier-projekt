package spiellogik;

import java.util.Vector;

import network.interfaces.ServerInterface;
import network.server.ServerGame;

public class Game {
	
	private Vector<Knight> MEMBERS = new Vector<Knight>();
//	private ServerInterface serverInterface;
	private ServerGame serverGame;
	private Map map;
	private Grail grail;
	private VisionInfo visionInfo;
	private boolean ended;
	
	public Game(ServerInterface serverInterface, ServerGame serverGame) {
		grail = new Grail(-2, -2);
		setMap(new Map(serverInterface, serverGame, this, grail));
		getMap().setUp(serverGame.getMapID());
		serverGame.setMap(getMap());
//		this.serverInterface = serverInterface;
		this.setServerGame(serverGame);
		visionInfo = new VisionInfo(getMap(), this);
	}

	/**
	 * adds Knight to Vector
	 * @param k
	 */
	public void addKnight(Knight k){
		MEMBERS.add(k);
	}
	
	/**
	 * removes Knight from Vector
	 * @param k
	 */
	public void removeKnight(Knight k){
		MEMBERS.remove(k);
	}
	
	/**If a Knight stands on specified tile, returns the Knight, else returns null.
	 * @return Knight or null
	 */
	
	
	public Knight getPlayerAt(int x, int y) {
		for (Knight i : MEMBERS) {
			if (i.getX()== x && i.getY() == y) {
				return i;
			}
		}
		return null;
	}
	
	/**returns all Knights that stand on a tile with is set to true in the given boolean array.
	 * @param area a 2-dimensional MAPSIZEX*MAPSIZEY boolean array.
	 * @return Vector(Knight) (may be empty)
	 */
	public Vector<Knight> getPlayersAtArea(boolean[][] area) {
		Vector<Knight> result = new Vector<Knight>();
		for (Knight i : MEMBERS) {
			if (!((i.getX() < 0 || i.getX() > getMap().MAPSIZEX) || (i.getY() < 0 || i.getY() > getMap().MAPSIZEY))){
				if (area[i.getX()][i.getY()]) {
					result.add(i);
					
				}
			}
		}
		return result;
	}
	
	/**returns all Knights that are revealed, including the Grail bearer.
	 * @return Vector(Knight) (may be empty)
	 */
	public Vector<Knight> getRevealedPlayers() {
		Vector<Knight> result = new Vector<Knight>();
		for (Knight i : MEMBERS) {
			if (i.hasEffect(StatusEffect.REVEALED) || i.hasGrail()) {
				result.add(i);
			}
		}
		return result;
	}
	
	/**
	 * creates Vector containing all team members 
	 * @param team
	 * @return Vector containing all Knights of the same team;
	 */
	public Vector<Knight> getTeamMembers(byte team){
		
		Vector<Knight> teamVector = new Vector<Knight>();
		for(Knight i: MEMBERS){
			if(i.getTeam()==team){
				teamVector.add(i);
			}
		}
		return teamVector;
	}

	public void updateTeam1() {
		visionInfo.updateTeam1();
		
	}

	public void updateTeam2() {
		visionInfo.updateTeam2();
		
	}
	
	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	public ServerGame getServerGame() {
		return serverGame;
	}

	public void setServerGame(ServerGame serverGame) {
		this.serverGame = serverGame;
	}

	public void move(Knight avatar, byte direction) {
		map.move(avatar, direction);
	}

	public void installTrap(Knight avatar, int trapNumber) {
		map.installTrap(avatar, trapNumber);
		
	}

	public boolean[][] specifyVision(Knight knight) {
		return visionInfo.specifyVision(knight);
	}

	public Vector<Knight> getSeenPlayers(Knight knight) {
		return visionInfo.getSeenPlayers(knight);
	}

	public boolean isEnded() {
		return ended;
	}

	public void setEnded(boolean ended) {
		this.ended = ended;
	}

	public Grail getGrail() {
		return grail;
	}

	
	

}
	


