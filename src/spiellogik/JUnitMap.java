package spiellogik;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import network.Server;
import network.interfaces.ServerInterface;
import network.server.ServerClient;
import network.server.ServerGame;

import org.junit.Assert;
import org.junit.Test;

/**
 * This JUnit tests the core class which implements the whole game-logic.
 */
public class JUnitMap {

	private Map map;
	private static boolean firstRun = true;
	private static ServerInterface serverInterface;
	private Knight knight1;
	private Knight knight2;
	private Socket socket1;
	private Socket socket2;
	private ServerClient serverClient1;
	private ServerClient serverClient2;
	private Game game;
	
	

	/** initializes a server and 2 clients in order to run the tests
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public JUnitMap() throws UnknownHostException, IOException {
		if (firstRun) {
			firstRun = false;
			serverInterface = null;
			try {
				serverInterface = new ServerInterface(12345, new Server());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		ServerGame serverGame = new ServerGame("testgame", 11);
		game = new Game(serverInterface, serverGame);
		map = new Map(serverInterface, serverGame, game, new Grail(0, 0));
		map.setUp(11);
		socket1 = new Socket("", 12345);
		socket2 = new Socket("", 12345);
		serverClient1 = new ServerClient(socket1, 0, new BufferedReader(new InputStreamReader(socket1.getInputStream())), new BufferedWriter(new OutputStreamWriter(socket1.getOutputStream())), serverInterface.getServer());
		serverClient2 = new ServerClient(socket1, 0, new BufferedReader(new InputStreamReader(socket2.getInputStream())), new BufferedWriter(new OutputStreamWriter(socket2.getOutputStream())), serverInterface.getServer());
		knight1 = new Knight(serverClient1, (byte) 1, 2, 2, new Grail(0, 0), map);
		knight2 = new Knight(serverClient2, (byte) 2, 1, 1, new Grail(0, 0), map);
		game.addKnight(knight1);
		game.addKnight(knight2);
	}
	
	/**
	 * Tests whether action points are decreased
	 */
	@Test
	public void decreasingActionPoints() {
		setKnightsTo(0, 0, 0, 1);
		map.move(knight2, Map.DOWN);
		map.move(knight2, Map.UP);
		
		// 2 moves should cost 2 ap
		Assert.assertFalse("ran into wall", knight2.getAction() < 8);
		// attacking knight1 should cost 2 each hit
		knight2.updateFirst();
		map.move(knight2, Map.UP);
		map.move(knight2, Map.UP);
		Assert.assertFalse("ran into water", knight2.getAction() < 4);
	}
	
	/**
	 * Tests whether knights can run into water / walls
	 */
	@Test
	public void runningIntoWaterOrWall() {
		setKnightsTo(5, 1, 4, 2);
		//walltest
		Assert.assertFalse("ran into wall", map.move(knight1, Map.DOWN));
		//watertest
		Assert.assertFalse("ran into water", map.move(knight1, Map.DOWN));
	}
	


	/**
	 * Tests killing knights with 5 hits
	 */
	@Test
	public void killingEnemies() {
		setKnightsTo(0, 0, 0, 1);
		for (int i = 0; i < 5; i++) {
			map.move(knight2, Map.UP);
		}
		Assert.assertFalse("couldnt kill knight", knight1.getLife() != 0);
	}
	
	/**
	 * Tests whether knights can pick up the grail
	 */
	@Test
	public void pickingUpTheGrail() {
		setKnightsTo(0, 0, 0, 1);
		for (int i = 0; i < 5; i++) {
			map.move(knight1, Map.RIGHT);
		}
		Assert.assertFalse("couldnt pick up grail", !knight1.hasGrail());
	}
	
	/**
	 * Tests whether you can end the game by placing the grail to your goal
	 */
	@Test
	public void endGame() {
		setKnightsTo(0, 0, 0, 1);
		for (int i = 0; i < 5; i++) {
			map.move(knight1, Map.RIGHT);
		}
		map.move(knight1, Map.LEFT);
		map.move(knight1, Map.LEFT);
		map.move(knight1, Map.DOWN);
		Assert.assertFalse("couldnt pick up grail", !game.getServerGame().isEnded());
	}
	
	

	/** Sets knight positions
	 * @param i x knight1
	 * @param j y knight1
	 * @param k x knight2
	 * @param l y knight2
	 */
	private void setKnightsTo(int i, int j, int k, int l) {
		knight1.setX(i);knight1.setY(j);
		knight2.setX(k);knight2.setY(l);
	}
}
