package spiellogik;

public class HTGEvent {
	public String fileName;
	public static final String TRAP_PLACE = "trap_place";
	public static final String TRAP_TRIGGER = "trap_trigger";
	public static final String DAMAGE_HIT = "damage_hit";
	public static final String DAMAGE_POISON = "damage_poison";
	public static final String DAMAGE_DISPEL = "damage_dispel";
	public static final String DAMAGE_PURGE = "damage_purge";
	public static final String DAMAGE_PURGE_SELF = "damage_purge_s";

	public HTGEvent() {
	}
}