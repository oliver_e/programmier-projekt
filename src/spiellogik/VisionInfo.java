package spiellogik;

import java.util.Vector;


public class VisionInfo {
	
	
	public static final int SIGHTRANGE = 10;
	private boolean[][] Team1Vision;
	private boolean[][] Team2Vision;
	private Vector<Knight> Team1Players;
	private Vector<Knight> Team2Players;
	private Map map;
	private Game game;
	
	/**
	 * Redraws the vision for Team 1, effectively merging the vision of each individual Knight.
	 * Access data via Team1Vision and Team1Player. 
	 */
	public VisionInfo(Map map, Game game) {
		 this.map = map;
		 this.game = game;
		 Team1Vision = new boolean[map.MAPSIZEX][map.MAPSIZEY];
		 Team2Vision = new boolean[map.MAPSIZEX][map.MAPSIZEY];
	}
	
	public void updateTeam1(){
		darkenTeam1();
		Vector<Knight> knightPool = game.getTeamMembers((byte) 1);
		for (Knight knight : knightPool) {
			int sightrange = (knight.hasEffect(StatusEffect.BLINDED)) ? 1 : SIGHTRANGE;
			int kX = knight.getX();
			int kY = knight.getY();
			int dX,dY;
			for (int i = 0; i < sightrange * 2 + 1; i++) {
				for (int j = 0; j < sightrange * 2 + 1; j++) {
					dX = kX + (i - (sightrange));
					dY = kY + (j - (sightrange));
					if (getDist(kX, kY, dX, dY) <= (sightrange+0.25)) {
						if (legolas(kX, kY, dX, dY)){
							try {
								Team1Vision[dX][dY] = true;
							} catch (ArrayIndexOutOfBoundsException e) {}
						}
					}
				}
			}
		}
		//Merge Players on same team, in vision area and with revealed status.
		Team1Players = game.getTeamMembers((byte) 1);
		Vector<Knight> temp = game.getPlayersAtArea(Team1Vision);
		for (Knight i : temp) {
			if(!Team1Players.contains(i)){
				Team1Players.add(i);
			}
		}
		temp = game.getRevealedPlayers();
		for (Knight i : temp) {
			if(!Team1Players.contains(i)){
				Team1Players.add(i);
			}
		}
		
	}
	
	/**
	 * Redraws the vision for Team 2, effectively merging the vision of each individual Knight.
	 * Access data via Team2Vision and Team2Player. 
	 */
	public void updateTeam2(){
		darkenTeam2();
		Vector<Knight> knightPool = game.getTeamMembers((byte) 2);
		for (Knight knight : knightPool) {
			int sightrange = (knight.hasEffect(StatusEffect.BLINDED)) ? 1 : SIGHTRANGE;
			int kX = knight.getX();
			int kY = knight.getY();
			int dX,dY;
			for (int i = 0; i < sightrange * 2 + 1; i++) {
				for (int j = 0; j < sightrange * 2 + 1; j++) {
					dX = kX + (i - (sightrange));
					dY = kY + (j - (sightrange));
					if (getDist(kX, kY, dX, dY) <= (sightrange+0.25)) {
						if (legolas(kX, kY, dX, dY)){
							try {
								Team2Vision[dX][dY] = true;
							} catch (ArrayIndexOutOfBoundsException e) {}
						}
					}
				}
			}
		}
		//Merge Players on same team, in vision area and with revealed status.
		Team2Players = game.getTeamMembers((byte) 2);
		Vector<Knight> temp = game.getPlayersAtArea(Team2Vision);
		for (Knight i : temp) {
			if(!Team2Players.contains(i)){
				Team2Players.add(i);
			}
		}
		temp = game.getRevealedPlayers();
		for (Knight i : temp) {
			if(!Team2Players.contains(i)){
				Team2Players.add(i);
			}
		}
		
	}
	
	/**
	 * resets the vision for team 1, to be called by updateTeam1().
	 */
	
	private void darkenTeam1(){
		for (int i = 0; i < Team1Vision[0].length; i++) {
			for (int j = 0; j < Team1Vision.length; j++) {
				Team1Vision[i][j] = false;
			}
		}
	}
	
	/**
	 * resets the vision for team 2, to be called by updateTeam2().
	 */
	
	private void darkenTeam2(){
		for (int i = 0; i < Team2Vision[0].length; i++) {
			for (int j = 0; j < Team2Vision.length; j++) {
				Team2Vision[i][j] = false;
			}
		}
	}
	
	/**
	 * Cuts out a part of the merged Vision specifically for the given Knight.
	 * @param target the Knight whose Vision you want.
	 * @return a 2-dimensional (2*SIGHTRANGE+1)*(2*SIGHTRANGE+1) boolean array.
	 */
	public boolean[][] specifyVision(Knight target){
		boolean[][] result = new boolean[2*SIGHTRANGE+1][2*SIGHTRANGE+1];
		int xOffset = target.getX() - SIGHTRANGE;
		int yOffset = target.getY() - SIGHTRANGE;
		for (int y = 0; y < result[0].length; y++) {
			for (int x = 0; x < result.length; x++) {
				if (x+xOffset >= 0 && x+xOffset < map.MAPSIZEX && y+yOffset >= 0 && y+yOffset < map.MAPSIZEY)
					if (target.getTeam() == 1) {
						result[x][y] = Team1Vision[x+xOffset][y+yOffset];
					} else {
						result[x][y] = Team2Vision[x+xOffset][y+yOffset];
					}
			}
		}
		return result;
	}
	
	/**
	 * Uses the Bresenheim Algorithm for straight lines to check whether the destination tile can be seen from the starting tile.<br><br>
	 * "What do your elf eyes see?"
	 * @param xPos X coord of starting point
	 * @param yPos Y coord of starting point
	 * @param targetX X coord of destination
	 * @param targetY Y coord of destination
	 * @return true if the destination tile can be seen from the starting tile.
	 */
	
	private boolean legolas(int xPos, int yPos, int targetX, int targetY) {
		if (xPos==targetX && yPos==targetY) return true; //trivial case
		
		int dx =  Math.abs(targetX-xPos), sx = xPos<targetX ? 1 : -1;
		int dy = -Math.abs(targetY-yPos), sy = yPos<targetY ? 1 : -1;
		int err = dx+dy, e2; /* error value e_xy */
		 
		for(int failsafe = 0; failsafe < 1000; failsafe++){  // loops until destination reached or opaque block encountered.
			e2 = 2*err;
			if (e2 > dy) { err += dy; xPos += sx; } /* e_xy+e_x > 0 */
			if (e2 < dx) { err += dx; yPos += sy; } /* e_xy+e_y < 0 */
			if (xPos==targetX && yPos==targetY) return true; // breaks with true if destination reached.
			if ((map.getType(xPos, yPos) >= map.VISIONTHRESHOLD)) return false; // breaks with false if obstructed and not destination.
		}
		return false; //should never be reached

	}
	
	/**
	 * Combines then legolas and gandalf algorithm.
	 * @param xPos X coord of starting point
	 * @param yPos Y coord of starting point
	 * @param targetX X coord of destination
	 * @param targetY Y coord of destination
	 * @param team the team for which it will write.  
	 */
	/*
	private void fellowship(int xPos, int yPos, int targetX, int targetY, byte team) {
		
		int dx =  Math.abs(targetX-xPos), sx = xPos<targetX ? 1 : -1;
		int dy = -Math.abs(targetY-yPos), sy = yPos<targetY ? 1 : -1;
		int err = dx+dy, e2; // error value e_xy
		 
		for(int failsafe = 0; failsafe < 1000; failsafe++){  // loops until destination reached or opaque block encountered.
			if (team == (byte) 1) {
				Team1Vision[xPos][yPos] = true;
			} else {
				Team2Vision[xPos][yPos] = true;
			}
			if (xPos==targetX && yPos==targetY) return; // breaks if destination reached.
			if ((map.getType(xPos, yPos) >= map.VISIONTHRESHOLD)) return; // breaks if obstructed.
			e2 = 2*err;
			if (e2 > dy) { err += dy; xPos += sx; } // e_xy+e_x > 0
			if (e2 < dx) { err += dx; yPos += sy; } // e_xy+e_y < 0 
		}

	}
	*/

	/**
	 * Returns the distance between two tiles
	 */
	private double getDist(int x1, int y1, int x2, int y2){
		return Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
	}

	public Vector<Knight> getSeenPlayers(Knight knight) {
		if (knight.getTeam() == 1){
			return Team1Players;
		} else if (knight.getTeam() == 2) {
			return Team2Players;
		}
		return null;
	}
	
}
