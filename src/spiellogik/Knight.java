package spiellogik;

import java.util.Vector;
import network.server.ServerClient;

public class Knight {
	
	private ServerClient summoner;
	
	private int action;
	private int life;
	private byte team;
	private int xCoord;
	private int yCoord;
	private int direction = 0;
	private Vector<StatusEffect> statusEffects;
	private Vector<StatusEffect> flaggedForDeletion; //Temporarily holds effects to be removed at end of turn.
	private boolean hasGrail;
	private int spawnX;
	private int spawnY;
	private Grail grail;
	private static int ATTACK_COST = 2;
	private static int ATTACK_POWER = 25;
	public static int MOVE_COST = 1;
	private Map map;
	
	
	/**
	 * Represents a player's character.
	 * @param summoner
	 * affiliated ServerClient object.
	 * @param team
	 * Must be 1 or 2.
	 * @param spawnX
	 * @param spawnY
	 */
	public Knight(ServerClient summoner, byte team, int startX, int startY, Grail grail, Map map){
		this.summoner = summoner;
		action = 10;
		life = 100;
		this.team = team;
		this.grail = grail;
		this.map = map;
		xCoord = startX;
		yCoord = startY;
		spawnX = startX;
		spawnY = startY;
		statusEffects = new Vector<StatusEffect>();
		flaggedForDeletion = new Vector<StatusEffect>();
	}
	
	/**
	 * Always call on start of turn.
	 */
	public void updateFirst() {// To be called on start of turn.
		if (getLife()<=0) { //if the player is gone
			respawn();
		}
		resetAction();
		tickEffectsFirst();
	}
	
	/**
	 * ALways call on end of turn.
	 */
	public void updateLast() { // To be called on end of turn.
		tickEffectsLast();
	}
	
	/** If Knight has enough AP to attack, pays AP cost and attacks opponent.
	 * @param opponent
	 * @return true if attack was successful
	 */
	
	public boolean attack(Knight opponent) {
		if (payAction(ATTACK_COST)) {
			opponent.addLife(-ATTACK_POWER);
			getMap().sendToNearbyPlayers(HTGEvent.DAMAGE_HIT, opponent, opponent.getX(), opponent.getY(), 10.25);
			return true;
		}
		return false;
	}
	
	public boolean payAction(int cost) {
		if (action >= cost) {
			action -= cost;
			return true;
		}
		return false;
		
	}

	/**
	 * @return Knight's current pool of Action Points.
	 */
	public int getAction(){
		return action;
	}
	
	/**
	 * @return Knight's current pool of Life Points.
	 */
	public int getLife(){
		return life;
	}
	/**
	 * The Knight's team alignment.
	 * @return
	 * 1 for Team 1<br> 2 for Team 2
	 */
	public byte getTeam() {
		return team;
	}
	
	public int getX(){
		return xCoord;
	}
	
	public int getY(){
		return yCoord;
	}
	
	public ServerClient getSummoner(){
		return summoner;
	}

	/**
	 * @return true if the Knight is the Grail bearer.
	 */
	public boolean hasGrail() {
		return hasGrail;
	}
	
	public void setX(int x) {
		this.xCoord = x;
	}
	
	public void setY(int y) {
		this.yCoord = y;
	}
	
	public void setGrail(boolean grail) {
		hasGrail = grail;
	}
	
	/**
	 * Adds an amount to the Knight's Action Pool. Sets to 0 if result<0.
	 * @param value
	 * the amount to be added. Use negative for subtraction.
	 */
	public void addAction(int value){
		if (action + value >= 0) {
			action += value;
		} else {
			action = 0;
		}
	}
	/**
	 * Reset's the Knight's Action Pool to 10 or 7 if he has the grail.
	 */
	public void resetAction(){
		if (hasGrail()) {
			action = 7;
		} else {
			action = 10;
		}
		
	}
	
	/**
	 * Adds an amount to the Knight's Life Points.
	 * @param value
	 * the amount to be added. Use negative for subtraction.
	 */
	public void addLife(int value){
		addLife(value, false);
	}
	
	/**
	 * Adds an amount to the Knight's Life Points.
	 * @param value the amount to be added. Use negative for subtraction.
	 * @param ignoreArmor whether the damage does not take the prone effect into account.
	 */
	public void addLife(int value, boolean ignoreArmor){
		int moddamage = (hasEffect(StatusEffect.PRONE) && !ignoreArmor) ? 2*value : value;
		if (life + moddamage > 0) {
				life += moddamage;
		} else {
			life = 0;
			perish();
		}
	}
	
	/**
	 * Reset's the Knight's Life Points to 100.
	 */
	public void resetLife(){
		life = 100;
	}
	
	
	/**
	 * checks if the Knight has an active status effect of a certain type.
	 * @param type
	 * accepts StatusEffect.POISONED, REVEALED, BLINDED, SLOWED, STUNNED.
	 */
	public boolean hasEffect(int type) {
		for (StatusEffect eff : statusEffects) {
			if (eff.getType() == type) return true;
		} //no match found
		return false;
	}
	
	
	/**
	 * Adds a status effect to the Knight.
	 * @param eff
	 * the StatusEffect to add.
	 */
	public void addEffect(StatusEffect eff) {
		statusEffects.add(eff);
		eff.tickFirst();
	}
	
	/**
	 * removes a specific status effect when applyEffectRemoval is called. To be called by the effect itself.
	 */
	public void removeEffect(StatusEffect eff) {
		flaggedForDeletion.add(eff);
	}
	
	private void applyEffectRemoval() {
		if (flaggedForDeletion.size() > 0) {
			for (StatusEffect eff : flaggedForDeletion) {
				statusEffects.remove(eff);
			}
			flaggedForDeletion.removeAllElements();
		}
		
	}
	/**
	 * removes all status effects when applyEffectRemoval is called. To be called upon death.
	 */
	public void clenseEffects() {
		if (statusEffects.size() > 0) {
			for (StatusEffect eff : statusEffects) {
			flaggedForDeletion.add(eff);
			}
		}
	}
	
	/**
	 * Applies Effects. To be called at the start of a turn.
	 */
	private void tickEffectsFirst() {
		if (statusEffects.size() > 0) {
			for (StatusEffect eff : statusEffects) {
				eff.tickFirst();
			}
		}
	}
	
	/**
	 * Applies Effects and reduces timer. To be called at the end of a turn.
	 */
	private void tickEffectsLast() {
		if (statusEffects.size() > 0) {
			for (StatusEffect eff : statusEffects) {
				eff.tickLast();
			}
			applyEffectRemoval();
		}
	}
	
	/**
	 * Makes the Knight disappear from the game while leaving him technically intact (effects can finish ticking etc.).<br>
	 * Use respawn() to make him reenter the game.<br>
	 * To be called upon death.
	 */
	private void perish() {
		if(hasGrail()) {
			grail.setX(xCoord);
			grail.setY(yCoord);
			grail.setBearer(null);
			setGrail(false);
		}
		setX(-32);
		setY(-32);
	}
	
	/**
	 * Makes the Knight reenter the game with full health at his spawn point.<br>
	 * To be called on the start of the turn of a Knight, that has previously been defeated.
	 */
	private void respawn() {
		setX(spawnX);
		setY(spawnY);
		resetLife();
		clenseEffects();
		applyEffectRemoval();
	}

	public Vector<StatusEffect> getEffects() {
		return statusEffects;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public Map getMap() {
		return map;
		
	}
	


	
}

