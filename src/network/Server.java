package network;

import gui.server.ServerStartFrame;

import java.awt.Point;
import java.io.IOException;
import java.net.BindException;
import java.util.Vector;

import javax.swing.JOptionPane;

import network.interfaces.ServerInterface;
import network.server.ServerClient;
import network.server.ServerControler;
import network.server.ServerGame;
import spiellogik.Game;
import spiellogik.Knight;
import spiellogik.Map;
import spiellogik.TrapInfo;
import startup.Main;


/**
 * main class of server: starts serverframe and validates knight moves.
 *
 */
public class Server implements ServerControler {

	private ServerInterface serverInterface;
	private Vector<Game> games;
	private ServerStartFrame sframe;
	
	public Server() {
		games = new Vector<Game>();
	}
	
	public static void main(String[] args) {
		new Server().run(args);		
	}

	/** requests port from user and open serverframe
	 * @param args
	 */
	private void run(String[] args) {
		// Hier wird eben alles abgefragt, eingabe des Port und etc.
		int port = 0;
		while (true) {
			if (Main.port > 0) {
				port = Main.port;
				Main.port = 0;//invalidate port to avoid it blocking repeated input due to port being already taken
			} else {
				while (true) {
					try {
						String input = JOptionPane.showInputDialog(null,
								"", "Enter port", JOptionPane.QUESTION_MESSAGE);
						if (input == null) {
							//if canceled
							System.exit(0);
						}
						port = Integer.parseInt(input);
						break;
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null,
								"Please type in a number!");
					}
				}
			}
			
			try {
				serverInterface = new ServerInterface(port, this);
				break;
			} catch (BindException e) {
				JOptionPane.showMessageDialog(null,
						"Port is already in use... type in a new one");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		serverInterface.waitForClients();
		createServerFrame();
	}
	
	private void createServerFrame(){
		sframe = new ServerStartFrame(serverInterface);
		sframe.setVisible(true);
	}


	/**
	 * initializes the game
	 */
	@Override
	public void gameStarted(ServerGame serverGame) {
		
		Game game = new Game(serverInterface, serverGame);
		games.add(game);
		int counter = 0;
		for (ServerClient client : serverGame.getTeam1()) {
			//Knight knight = new Knight(client, (byte) 1, 12+counter++, 12); //Spawn = 12, 12 fix for now, change later.
			Knight knight = new Knight(client, (byte) 1, serverGame.getSpawnX(1)+counter++, serverGame.getSpawnY(1), game.getGrail(), game.getMap()); //Test Spawn Point
			game.addKnight(knight);
			client.assignAvatar(knight);
		}
		counter = 0;
		for (ServerClient client : serverGame.getTeam2()) {
			//Knight knight = new Knight(client, (byte) 2, 52-counter++, 52); //Spawn = 52, 52 fix for now, change later.
			Knight knight = new Knight(client, (byte) 2, serverGame.getSpawnX(2)+counter++, serverGame.getSpawnY(2), game.getGrail(), game.getMap()); //Test Spawn Point
			game.addKnight(knight);
			client.assignAvatar(knight);
		}
		game.updateTeam1();
		game.updateTeam2();
		for (Knight knight : game.getTeamMembers((byte) 1)) {
			serverInterface.update(knight.getSummoner(), knight.getLife(), knight.getAction(), new Point(knight.getX(), knight.getY()), game.specifyVision(knight), game.getSeenPlayers(knight), knight.getEffects(), game.getGrail().getBearer() == null? game.getGrail().getX() : game.getGrail().getBearer().getX(), game.getGrail().getBearer() == null? game.getGrail().getY() : game.getGrail().getBearer().getY(), game.getGrail().getBearer() != null, knight.getSummoner() == serverGame.getTeam1().get(0));
		}
		for (Knight knight : game.getTeamMembers((byte) 2)) {
			serverInterface.update(knight.getSummoner(), knight.getLife(), knight.getAction(), new Point(knight.getX(), knight.getY()), game.specifyVision(knight), game.getSeenPlayers(knight), knight.getEffects(), game.getGrail().getBearer() == null? game.getGrail().getX() : game.getGrail().getBearer().getX(), game.getGrail().getBearer() == null? game.getGrail().getY() : game.getGrail().getBearer().getY(), game.getGrail().getBearer() != null, knight.getSummoner() == serverGame.getTeam1().get(0));
		}
		
		serverGame.setCurrentPlayer(serverGame.getTeam1().get(0));
	}
	
	
	/**
	 * sends move to map and applies updates after the move
	 */
	@Override
	public void playerWantsToMakeMove(ServerClient serverClient, int move) {
		if (serverClient.getCurrentGame() != null) {
			if (serverClient.getCurrentGame().getCurrentPlayer() == serverClient) {
				ServerClient nextPlayer = serverClient;
				Game game = null;
				for (Game gameCount : games) {
					if (serverClient.getCurrentGame() == gameCount
							.getServerGame()) {
						game = gameCount;
					}
				}
				switch (move) {
				case Client.TURN_MOVEUP:
					game.move(serverClient.getAvatar(), Map.UP);
					serverClient.getAvatar().setDirection(0);
					break;

				case Client.TURN_MOVEDOWN:
					game.move(serverClient.getAvatar(), Map.DOWN);
					serverClient.getAvatar().setDirection(2);
					break;

				case Client.TURN_MOVELEFT:
					game.move(serverClient.getAvatar(), Map.LEFT);
					serverClient.getAvatar().setDirection(3);
					break;

				case Client.TURN_MOVERIGHT:
					game.move(serverClient.getAvatar(), Map.RIGHT);
					serverClient.getAvatar().setDirection(1);
					break;

				case Client.TURN_PLACETRAP_POISON:
					game.installTrap(serverClient.getAvatar(), TrapInfo.POISON);
					break;

				case Client.TURN_PLACETRAP_STUN:
					game.installTrap(serverClient.getAvatar(), TrapInfo.STUN);
					break;

				case Client.TURN_PLACETRAP_SLOW:
					game.installTrap(serverClient.getAvatar(), TrapInfo.SLOW);
					break;

				case Client.TURN_PLACETRAP_REVEAL:
					game.installTrap(serverClient.getAvatar(), TrapInfo.REVEAL);
					break;

				case Client.TURN_PLACETRAP_BLIND:
					game.installTrap(serverClient.getAvatar(), TrapInfo.BLIND);
					break;

				case Client.TURN_PLACETRAP_PURGE:
					game.installTrap(serverClient.getAvatar(), TrapInfo.PURGE);
					break;

				case Client.TURN_PLACETRAP_DISPEL:
					game.installTrap(serverClient.getAvatar(), TrapInfo.DISPEL);
					break;
					
				case Client.TURN_PLACETRAP_PRONE:
					game.installTrap(serverClient.getAvatar(), TrapInfo.PRONE);
					break;

				case Client.TURN_COMPLETETURN:
					// takes next player from the running game's list of all players.
					serverClient.getAvatar().updateLast();
					nextPlayer = serverClient
							.getCurrentGame()
							.getAllClients()
							.get(((serverClient.getCurrentGame()
									.getAllClients().indexOf(serverClient)) + 1)
									% (serverClient.getCurrentGame()
											.getAllClients().size()));
					serverClient.getCurrentGame().setCurrentPlayer(nextPlayer);
					
					nextPlayer.getAvatar().updateFirst();
					break;

				default:
					break;
				}
				game.updateTeam1();
				game.updateTeam2();
				if (!game.getServerGame().isEnded()) {
					for (ServerClient client : serverClient.getCurrentGame()
							.getAllClients()) {
						Knight knight = client.getAvatar();
						serverInterface.update(client, knight.getLife(), knight
								.getAction(),
								new Point(knight.getX(), knight.getY()), game
										.specifyVision(knight), game
										.getSeenPlayers(knight), knight
										.getEffects(), game.getGrail()
										.getBearer() == null ? game.getGrail()
										.getX() : game.getGrail().getBearer()
										.getX(),
								game.getGrail().getBearer() == null ? game
										.getGrail().getY() : game.getGrail()
										.getBearer().getY(), game.getGrail()
										.getBearer() != null, client == nextPlayer);
					}
				}
				serverInterface.commitEvents();
			}
		}
	}

	@Override
	public void updateServerFrame() {
		try {
			sframe.update();
		} catch (NullPointerException e) {
			//for JUNITtest
		}
	}

}
