package network.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;


/** utility so save and load highscores
 */
public class Highscore implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Entry> entries = new ArrayList<Entry>();
	
	public ArrayList<Entry> getEntries(){
		return entries;
	}
	
	@SuppressWarnings("resource")
	public static Highscore load() {
		try {
			String path = Highscore.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			String decodedPath = URLDecoder.decode(path, "UTF-8");
			decodedPath = decodedPath.substring(0, decodedPath.lastIndexOf(File.separator)+1) + "highscores.data";
			
			InputStream resource = new FileInputStream(decodedPath);
			ObjectInputStream obj_in = new ObjectInputStream(resource);
			return (Highscore) obj_in.readObject();
		} catch (IOException e) {
			return new Highscore();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("resource")
	public void save() {
		sort();
		try {
			String path = Highscore.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			String decodedPath = URLDecoder.decode(path, "UTF-8");
			decodedPath = decodedPath.substring(0, decodedPath.lastIndexOf(File.separator)+1) + "highscores.data";
			
			FileOutputStream f_out = new FileOutputStream(decodedPath);
			ObjectOutputStream obj_out = new ObjectOutputStream(f_out);
			obj_out.writeObject(this);
		} catch (Exception e) {
		}
		
	}
	
	public void sort() {
		Collections.sort(entries);
		Collections.reverse(entries);
	}
	
	public boolean addWin(String name) {
		for (Entry entry : entries) {
			if (entry.name.equals(name)){
				entry.wins += 1;
				return true;
			}
		}
		// no entry yet
		entries.add(new Entry(name, 1));
		return true;
	}
	
	/**
	 * Makes an entirely new Highscore. Should only ever be called once.
	 */
	public Highscore() {
		
	}
	
	public class Entry implements Comparable<Entry>, Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public String name;
		public int wins;
		public Entry(String name, int wins) {
			this.name = name;
			this.wins = wins;
		}
		@Override
		public int compareTo(Entry o) {
			return this.wins - o.wins;
		}
	}

	public int getValueOf(String name) {
		for (Entry entry : entries) {
			if (entry.name.equals(name)) {
				return entry.wins;
			}
		}
		return 0;
	}
}
