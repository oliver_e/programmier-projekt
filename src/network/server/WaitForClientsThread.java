package network.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/** this thread waits for incoming socket connections and puts them into the client arraylist
 */
public class WaitForClientsThread extends Thread {

	private NetworkServer server;
	private ArrayList<ServerClient> alClients;
	private int uniqueId = 0;
	private ServerSocket serverSocker;


	public WaitForClientsThread(NetworkServer server, ServerSocket serverSocket, ArrayList<ServerClient> alClients) {
		this.server = server;
		this.serverSocker = serverSocket;
		this.alClients = alClients;
	}
	
	
	/**
	 * accepts connection from sockets and creates writer and reader for connection
	 */
	@Override
	public void run() {
		while (true) {
			try {
//				System.out.println("Auf Clients warten...");
				// Hier wird auf die Clients gewartet
				Socket s = serverSocker.accept();
				
//				System.out.println("Verbindung von " + s.getRemoteSocketAddress().toString() + " entgegengenommen..");
				
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream(), "ISO-8859-1"));
				
				BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));

				// Hier und im folgenden wird f�r jeden Client ein Thread
				// erstellt und in die Liste aufgenommen
				ServerClient client = new ServerClient(s, uniqueId++, br, bw, server);
				Thread t = new Thread(client);
				// Hier wird der client in die Liste eingef�gt zum sp�teren
				// broadcasten und etc.
				alClients.add(client);
				t.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
