package network.server;

/** these events are triggered from network-server to notify the server
 */
public interface ServerControler {

	void gameStarted(ServerGame game);

	void playerWantsToMakeMove(ServerClient serverClient, int move);

	void updateServerFrame();

}
