package network.server;

import java.util.ArrayList;

import spiellogik.Map;

/** holds all information which the server needs to communicate to the clients
 */
public class ServerGame implements Runnable {

	private String gameName;
	

	private ArrayList<ServerClient> team1;
	private ArrayList<ServerClient> team2;
	private boolean started;
	private boolean ended = false;
	
	private int mapID;
	private Map map;


	private ServerClient currentPlayer;
	public ServerGame(String gameName, int mapID) {
		super();
		this.gameName = gameName;
		this.team1 = new ArrayList<ServerClient>();
		this.team2 = new ArrayList<ServerClient>();
		this.mapID = mapID;
		started = false;
	}
	
	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	@Override
	public void run() {
		started = true;
	}
	
	
	public ArrayList<ServerClient> getTeam1() {
		return team1;
	}

	public ArrayList<ServerClient> getTeam2() {
		return team2;
	}

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}

	public ArrayList<ServerClient> getAllClients() {
		ArrayList<ServerClient> clients = new ArrayList<ServerClient>();
		for (ServerClient client : team1) {
			clients.add(client);
		}
		for (ServerClient client : team2) {
			clients.add(client);
		}
		return clients;
	}

	public boolean isEnded() {
		return ended;
	}
	
	public void setEnded(boolean ended) {
		this.ended = ended;
	}

	public void setCurrentPlayer(ServerClient serverClient) {
		this.currentPlayer = serverClient;
	}

	public ServerClient getCurrentPlayer() {
		return currentPlayer;
	}
	
	public int getMapID(){
		return mapID;
	}

	public void setMap(Map map) {
		this.map = map;
		
	}

	public int getSpawnX(int team) {
		if (team == 1) {
			return map.team1SpawnX;
		} else {
			return map.team2SpawnX;
		}
	}
	
	public int getSpawnY(int team) {
		if (team == 1) {
			return map.team1SpawnY;
		} else {
			return map.team2SpawnY;
		}
	}

	public void remove(ServerClient serverClient) {
		team1.remove(serverClient);
		team2.remove(serverClient);
	}
}