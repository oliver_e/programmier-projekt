package network.server;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import network.Protocol;
import spiellogik.Knight;
import spiellogik.StatusEffect;
import spiellogik.VisionInfo;


/** has important methods for server client communication, creates / manages games and passes chat messages etc.
 */
public class NetworkServer {

	// Liste f�r die Clients, welche sp�ter f�rs Broadcasten oder zum closen der
	// Sockets verwendet wird
	ArrayList<ServerClient> alClients;
	ArrayList<ServerGame> alGames;
	ServerSocket serverSocket;
	WaitForClientsThread waitForClientsThread;
	// Die einmaligen IDs die jedem client zugewiesen werden, wird pro Client um
	// 1 erh�ht
	int uniqueId;
	private ServerControler serverControler;

	int port;

	public NetworkServer(int port, ServerControler serverControler) throws IOException {
		this.serverControler = serverControler;
		this.port = port;
		alClients = new ArrayList<ServerClient>();
		alGames = new ArrayList<ServerGame>();
		
		serverSocket = new ServerSocket(port);
		
	}

	public void waitForClients() {
		waitForClientsThread = new WaitForClientsThread(this, serverSocket, alClients);
		waitForClientsThread.start();
	}

	public synchronized ArrayList<ServerClient> getWho() {
		return alClients;
	}
	
	public void sendServerMessageToAllClients(String msg) {
		for (ServerClient client : alClients) {
			client.sendToClient(Protocol.SVMS, "[SERVER] " + msg);
		}
		
	}

	/** sends message to all clients who should receive it
	 * @param msg
	 * @param sendingClient
	 * @throws Exception
	 */
	public synchronized void sendMessage(String msg, ServerClient sendingClient) throws Exception {
		//whisper
		if (msg.startsWith("/w ")) {
			boolean userFound = false;
			String receivingPerson = null; 
			try {
				receivingPerson = msg.split(" ")[1];
				msg = msg.substring(receivingPerson.length()+4, msg.length());
			} catch (StringIndexOutOfBoundsException | ArrayIndexOutOfBoundsException e) {
				sendingClient.sendToClient(Protocol.MESS, "Correct Usage: /w player message");
			}
			for (ServerClient client : alClients) {
				if (client.username.equals(receivingPerson)) {
					client.sendToClient(Protocol.MESS, "[Private] " + sendingClient.username + ": " + msg);
					userFound = true;
				}
			}
			if (!userFound) {
				sendingClient.sendToClient(Protocol.MESS, "Couldnt find user.");
			}
		}
		//game
		else if (msg.startsWith("/g ")) {
			ServerGame game = sendingClient.currentGame;
			if (game != null) {
				ArrayList<ServerClient> team1 = game.getTeam1();
				ArrayList<ServerClient> team2 = game.getTeam2();
				String message = msg.substring(3, msg.length());
				for (ServerClient serverClient : team1) {
					if (serverClient != sendingClient) {
						serverClient.sendToClient(Protocol.MESS, "[Game] " + sendingClient.username + ": " + message);
					}
				}
				for (ServerClient serverClient : team2) {
					if (serverClient != sendingClient) {
						serverClient.sendToClient(Protocol.MESS, "[Game] " + sendingClient.username + ": " + message);
					}
				}
			} else {
				sendingClient.sendToClient(Protocol.MESS, "You are currently not in a game!");
			}
			
		}
		//team
		else if (msg.startsWith("/t ")) {
			String message = msg.substring(3, msg.length());
			ArrayList<ServerClient> team = null;
			if (sendingClient.currentTeam == 1) {
				team = sendingClient.currentGame.getTeam1();
			} else if (sendingClient.currentTeam == 2) {
				team = sendingClient.currentGame.getTeam2();
			} else {
				sendingClient.sendToClient(Protocol.MESS, "You are currently not in a team!");
				return;
			}
			for (ServerClient client : team) {
				if (client != sendingClient) {
					client.sendToClient(Protocol.MESS, "[Team] " + sendingClient.username + ": " + message);
				}
			}
			
		}
		//global
		else {
			msg = "[Global] " + sendingClient.username + ": " + msg;
			for (int i = 0; i < alClients.size(); i++) {
				if (alClients.get(i) != sendingClient) {
					alClients.get(i).sendToClient(Protocol.MESS, msg);
				}	
			}
		}
		
	}

	public void removeClient(ServerClient serverClient) {
		alClients.remove(serverClient);
	}

	public void sendServerMessageToAllClientsWithoutMe(String msg, ServerClient him) {
		for (ServerClient client : alClients) {
			if (client != him) {
				client.sendToClient(Protocol.MESS, "[SERVER] " + msg);
			}
		}
	}

	public boolean isNameAlreadyInUse(String username) {
		for (int i = 0; i < alClients.size(); i++) {
			if (alClients.get(i).username != null && alClients.get(i).username.equals(username)) {
				return true;
			}
		}
		return false;
	}

	public void gameStarted(ServerGame game) {
		ArrayList<ServerClient> clients = game.getAllClients();
		for (ServerClient serverClient : clients) {
			serverClient.sendToClient(Protocol.STRT, "DONE", "" + game.getMapID());
		}
		sendOpenGamesToAll();
		serverControler.gameStarted(game);
	}

	void sendOpenGamesToAll() {
		for (ServerClient serverClient : alClients) {
			sendOpenGames(serverClient);
		}
	}

	public void update(ServerClient receivingClient, int healthpoints,
			int actionpoints, Point position, boolean[][] fieldOfVision,
			Vector<Knight> seenKights, Vector<StatusEffect> statuseffects, int grailX, int grailY, boolean carried, boolean yourTurn) {
		
		if (!carried){ //checks whether the grail's position needs to be concealed.
			if ((grailX-position.x+VisionInfo.SIGHTRANGE)<0 
					|| (grailX-position.x+VisionInfo.SIGHTRANGE)>=fieldOfVision.length 
					|| (grailY-position.y+VisionInfo.SIGHTRANGE)<0 
					|| (grailY-position.y+VisionInfo.SIGHTRANGE)>=fieldOfVision[0].length) {
				grailX = -1;
				grailY = -1;
			} else {
				if (!fieldOfVision[(grailX-position.x+VisionInfo.SIGHTRANGE)][(grailY-position.y+VisionInfo.SIGHTRANGE)]){
					grailX = -1;
					grailY = -1;
				}
			}
		}
		
		String argument1 = String.valueOf(healthpoints);
		String argument2 = String.valueOf(actionpoints);
		String argument3 = (int)position.getX() + "," + (int)position.getY();
		String argument4 = "";
		for (int x = 0; x < fieldOfVision.length; x++) {
			argument4 += "(";
			for (int y = 0; y < fieldOfVision[x].length; y++) {
				if (fieldOfVision[x][y]) {
					argument4 += "1";
				} else {
					argument4 += "0";
				}
				if (y < fieldOfVision[x].length-1) {
					argument4 += "-";
				}
			}
			argument4 += ")";
			if (x < fieldOfVision.length-1) {
				argument4 += ",";
			}
		}
		String argument5 = "";
		for (int i = 0; i < seenKights.size(); i++) {
			argument5 += "(";
			argument5 += seenKights.get(i).getX();
			argument5 += "#";
			argument5 += seenKights.get(i).getY();
			argument5 += "#";
			argument5 += (receivingClient.currentTeam == seenKights.get(i).getTeam()) ? "t" : "f";
			argument5 += "#";
			argument5 += seenKights.get(i).getDirection();
			argument5 += ")";
			if (i < seenKights.size()-1) {
				argument5 += ",";
			}
		}
		
		String argument7 = "";
		for (int i = 0; i < statuseffects.size(); i++) {
			argument7 += "(";
			argument7 += statuseffects.get(i).getType();
			argument7 += "#";
			argument7 += statuseffects.get(i).getDuration();
			argument7 += ")";
			if (i < statuseffects.size()-1) {
				argument7 += ",";
			}
		}
		String argument6 = "";
		argument6 += grailX;
		argument6 += ",";
		argument6 += grailY;
		argument6 += ",";
		argument6 += carried ? "t" : "f";
		
		String argument8 = yourTurn ? "t" : "f";
				
		receivingClient.sendToClient(Protocol.UPDT, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8);
	}

	public void makeTurn(ServerClient serverClient, int move) {
		serverControler.playerWantsToMakeMove(serverClient, move);
	}

	public String[] getAllUsernames() {
		String[] allUsernames = new String[alClients.size()];
		for (int i = 0; i < alClients.size(); i++) {
			allUsernames[i] = alClients.get(i).username;
		}
		return allUsernames;
	}

	public ArrayList<ServerGame> getAllGames() {
		return alGames;
	}

	public void endGame(ServerGame game, int teamNumberOfTheTeamWhichWon) {
		game.setEnded(true);
		ArrayList<ServerClient> team1 = game.getTeam1();
		ArrayList<ServerClient> team2 = game.getTeam2();
		for (ServerClient serverClient : team1) {
			serverClient.sendToClient(Protocol.GMOV, String.valueOf(teamNumberOfTheTeamWhichWon));
		}
		for (ServerClient serverClient : team2) {
			serverClient.sendToClient(Protocol.GMOV, String.valueOf(teamNumberOfTheTeamWhichWon));
		}
		updateServerFrame();
		updateHighscores(game, teamNumberOfTheTeamWhichWon);
		addOldGame(game, teamNumberOfTheTeamWhichWon);
	}

	private void updateHighscores(ServerGame game, int teamNumber) {
		Highscore highscore = Highscore.load();
		if (teamNumber == 1){
			for (ServerClient client : game.getTeam1()) {
				highscore.addWin(client.getUsername());
				
			}
		} else {
			for (ServerClient client : game.getTeam2()) {
				highscore.addWin(client.getUsername());
				
			}
		}
		highscore.save();
	}

	private void addOldGame(ServerGame game, int teamNumberOfTheTeamWhichWon) {
		String path = Highscore.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		String decodedPath = path;
		try {decodedPath = URLDecoder.decode(path, "UTF-8");} catch (UnsupportedEncodingException e1) {}
		decodedPath = decodedPath.substring(0, decodedPath.lastIndexOf(File.separator)+1) + "oldGames.txt";
		
		try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(decodedPath, true)))) {
			String newLine = "";
			//first gameName
			newLine += game.getGameName() + ";";
			for (int i = 0; i < game.getTeam1().size(); i++) {
				newLine += game.getTeam1().get(i).getUsername();
				if (i < game.getTeam1().size()-1) {
					newLine += ",";
				}
			}
			newLine += ";";
			for (int i = 0; i < game.getTeam2().size(); i++) {
				newLine += game.getTeam2().get(i).getUsername();
				if (i < game.getTeam2().size()-1) {
					newLine += ",";
				}
			}
			newLine += ";" + teamNumberOfTheTeamWhichWon;
			out.println(newLine);
		}catch (IOException e) {
		    e.printStackTrace();
		}
	}
	
	/*
	public void readAndSysoutOldGames() {
		try {
			String path = Highscore.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			String decodedPath = URLDecoder.decode(path, "UTF-8");
			decodedPath = decodedPath.substring(0, decodedPath.lastIndexOf(File.separator)+1) + "oldGames.txt";
			
			String[] lines = readLines(decodedPath);
			for (String line : lines) {
				System.out.println("Gamename: " + line.split(";")[0] + " Team1Spieler: " + line.split(";")[1] + " Team2Spieler: " + line.split(";")[2] + " Team which won:" + line.split(";")[3]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	*/
	
	public String[] readLines(String filename) throws IOException, FileNotFoundException {
        FileReader fileReader = new FileReader(filename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        List<String> lines = new ArrayList<String>();
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            lines.add(line);
        }
        bufferedReader.close();
        return lines.toArray(new String[lines.size()]);
    }

	public void sendOpenGames(ServerClient serverClient) {
		String allGameNames = "";
		for (int i = 0; i < alGames.size(); i++) {
			if (!alGames.get(i).isStarted()) {
				allGameNames += alGames.get(i).getGameName() + " " + "("
						+ alGames.get(i).getTeam1().size() + "|"
						+ alGames.get(i).getTeam2().size() + ")";
				if (i != alGames.size() - 1) {
					allGameNames += ",";
				}
			}
		}
		serverClient.sendToClient(Protocol.SHOW, allGameNames);
	}

	public void sendAllWhoIsOn(ServerClient serverClient) {
		for (ServerClient sClient : alClients) {
			if (sClient != serverClient) {
				sendWhoIsOn(sClient);
			}
		}
	}

	public void sendWhoIsOn(ServerClient serverClient) {
		ArrayList<ServerClient> al = alClients;
		String allUsers ="";
		for (int i = 0; i < al.size(); i++) {
			allUsers += al.get(i).username;
			//Seperation von mehreren Usern user1,user2...
			if (i != al.size()-1) {
				allUsers += ",";
			}
		}
		
		serverClient.sendToClient(Protocol.WHON, allUsers);
	}

	public void deleteClient(ServerClient serverClient) {
		alClients.remove(serverClient);
	}

	public void createAGame(String gameName, int mapID, ServerClient serverClient) {
		boolean nameAlreadyInUse = false; 
		for (int i = 0; i < alGames.size(); i++) {
			if (alGames.get(i).getGameName().equals(gameName)) {
				nameAlreadyInUse = true;
			}
		}
		
		if (!nameAlreadyInUse) {
			ServerGame game = new ServerGame(gameName, mapID);
			game.getTeam1().add(serverClient);
			alGames.add(game);
			serverClient.currentGame = game;
			serverClient.currentTeam = 1;
			serverClient.sendToClient(Protocol.BEGN, "DONE");
			sendAllOpenGamesToAllClientsExceptMe(serverClient);
			sendServerMessageToAllClientsWithoutMe(serverClient.username + " hat das Spiel " + gameName + " erstellt.", serverClient);
			updateServerFrame();
		} else {
			serverClient.sendToClient(Protocol.BEGN, "FAILED");
		}
	}

	private void sendAllOpenGamesToAllClientsExceptMe(ServerClient serverClient) {
		for (ServerClient sClient : alClients) {
			if (sClient != serverClient) {
				sendOpenGames(sClient);
			}
		}
	}

	public void requestPlayersInMyGame(ServerClient serverClient) {
		ServerGame game = serverClient.currentGame;
		String argument0 = "";
		for (int i = 0; i < game.getTeam1().size(); i++) {
			argument0 += game.getTeam1().get(i).username;
			if (i < game.getTeam1().size() - 1) {
				argument0 += ",";
			}
		}
		String argument1 = "";
		for (int i = 0; i < game.getTeam2().size(); i++) {
			argument1 += game.getTeam2().get(i).username;
			if (i < game.getTeam2().size() - 1) {
				argument1 += ",";
			}
		}
		serverClient.sendToClient(Protocol.RQPL, argument0, argument1);
	}

	public void sendPlayersToAllOtherClientsInMyGame(ServerClient serverClient) {
		ServerGame game = serverClient.currentGame;
		for (ServerClient sClient : game.getTeam1()) {
			if (sClient != serverClient) {
				requestPlayersInMyGame(sClient);
			}
		}
		for (ServerClient sClient : game.getTeam2()) {
			if (sClient != serverClient) {
				requestPlayersInMyGame(sClient);
			}
		}
	}

	public void getMeOutOfTheGame(ServerClient serverClient) {
		if (serverClient.currentGame != null) {
			if (serverClient.currentTeam == 1) {
				serverClient.currentGame.getTeam1().remove(serverClient);
			} else {
				serverClient.currentGame.getTeam2().remove(serverClient);
			}
		} else {
			serverClient.sendToClient(Protocol.LEAV, "FAILED");
			return;
		}
		//close game if empty
		if (serverClient.currentGame.getTeam1().size() == 0 && serverClient.currentGame.getTeam2().size() == 0) {
			alGames.remove(serverClient.currentGame);
			sendOpenGamesToAll();
			updateServerFrame();
		}
		
		//Send info of the leave to all other clients in the game
		sendPlayersToAllOtherClientsInMyGame(serverClient);
		sendOpenGamesToAll();
		
		serverClient.currentGame = null;
		serverClient.currentTeam = 0;
		serverClient.sendToClient(Protocol.LEAV, "DONE");
	}
	
	public String[] readOldGames() {
		try {
			String path = Highscore.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			String decodedPath = URLDecoder.decode(path, "UTF-8");
			decodedPath = decodedPath.substring(0, decodedPath.lastIndexOf(File.separator)+1) + "oldGames.txt";
			
			return readLines(decodedPath);
		} catch (IOException e) {
			//e.printStackTrace();
			String[] failReturn = new String[1];
			failReturn[0]="no old Games available";
			return failReturn;
		}
	}

	public String[] getAllGameNames() {
		String[] gameNames = new String[alGames.size()];
		for (int i = 0; i < gameNames.length; i++) {
			gameNames[i] = alGames.get(i).getGameName();
		}
		return gameNames;
	}

	public void updateServerFrame() {
		serverControler.updateServerFrame();
	}

	public void leaveIngame(ServerClient serverClient) {
		if (serverClient.currentGame != null) {
			endGame(serverClient.currentGame, serverClient.currentTeam == 1 ?  2 : 1);
		}
		getMeOutOfTheGame(serverClient);
	}

	public void kickPlayer(String name) {
		for (ServerClient sClient : alClients) {
			if (sClient.username.equals(name)) {
				sClient.sendToClient(Protocol.KICK);
			}
		}
	}

	public void pingfailed(ServerClient serverClient) {
		if (serverClient.currentGame != null) {
			ServerGame serverGame = serverClient.getCurrentGame();
			for (ServerClient sClient : serverGame.getAllClients()) {
				if (sClient != serverClient) {
					sendServerMessage(sClient, serverClient.getUsername() + " has connectionproblems!");
				}
			}
		}
	}

	private void sendServerMessage(ServerClient sClient, String msg) {
		sClient.sendToClient(Protocol.SVMS, msg);
	}

	public void playerReconnectTookTooLong(ServerClient serverClient) {
		endGame(serverClient.getCurrentGame(), serverClient.getTeam() == 1 ? 2 : 1);
	}

	public void pingWorksAgain(ServerClient serverClient) {
		if (serverClient.currentGame != null) {
			ServerGame serverGame = serverClient.getCurrentGame();
			for (ServerClient sClient : serverGame.getAllClients()) {
				if (sClient != serverClient) {
					sendServerMessage(sClient, serverClient.getUsername() + " has reconnected!");
				}
			}
		}
	}

	public void sendEvent(ServerClient serverClient, String event, int x, int y) {
		serverClient.sendToClient(Protocol.EVNT, event, ""+x, ""+y);
	}

	public void sendAllPlayersInGameToAllInGame(ServerClient serverClient) {
		ServerGame game = serverClient.currentGame;
		for (ServerClient sClient : game.getTeam1()) {
			requestPlayersInMyGame(sClient);
		}
		for (ServerClient sClient : game.getTeam2()) {
			requestPlayersInMyGame(sClient);
		}
	}

	public void receivedHighscoreRequest(ServerClient serverClient, String name) {
		Highscore highscore = Highscore.load();
		int value = highscore.getValueOf(name);
		serverClient.sendToClient(Protocol.HIGH, name, ""+value);
	}
}
