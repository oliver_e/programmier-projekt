package network.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OptionalDataException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.SocketException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Timer;
import java.util.TimerTask;

import spiellogik.Knight;
import network.Protocol;
import network.pinger.OurPinger;
import network.pinger.Pinger;


/** holds all information about one client and offers methods to communicate. furthermore it receives all messages sent from client
 */
public class ServerClient implements Runnable, Pinger {
		Socket socket;
//		private int id;
		String username;
		

		ServerGame currentGame;
		int currentTeam;
		
		BufferedReader br;
		BufferedWriter bw;
		private OurPinger pinger;
		private NetworkServer server;
		
		private Knight avatar;
		private Timer reconnectTimer;
		private String msg;

		public ServerClient(Socket socket, int id, BufferedReader br, BufferedWriter bw, NetworkServer server) {
			this.br = br;
			this.socket = socket;
//			this.id = id;
			this.bw = bw;
			this.pinger = new OurPinger(5000, this);
			this.server = server;
			this.reconnectTimer = new Timer();
		}

		/**
		 * receives message from client and passes it to the assign method
		 */
		@Override
		public void run() {
			while (true) {
				try {
					if (!socket.isClosed()) {
						msg = ((String) br.readLine());
						if (msg.length() >= 4) {
							// Hier wird ein Message Received zum Client geschickt dass die
							// Daten erhalten haben.
							sendToClient(Protocol.MSRC, "");
							// Zuweisung des Protokolls
							//System.out.println("Server received:" + check);
							assign(msg);
						} // end of if
					} else {
//						System.out.println("Verbindung zum Benutzer "
//								+ username + " geschlossen");
						socket.close();
						break;
					}
					

				} catch (OptionalDataException e) {
					System.out.println("end of file:" + e.eof);
				} 
				catch (SocketException e) {
					//e.printStackTrace();
					if (e.getMessage().equals("Connection reset")) {
						System.err.println("Die Verbindung zu " + username + " wurde unterbrochen (kein logout).");
						logout();
						server.removeClient(this);
						break;
					}
				}
				catch (Exception e) {
					e.printStackTrace();
					//System.out.println("Die Verbindung zu " + username + " wurde unterbrochen (kein logout).");
					//alClients.remove(this);
				}
			}
		}
		
		@Override
		public boolean equals(Object serverClient) {
			return serverClient == this;
		}

		/** implements the protocol to check what information the client has sent
		 * @param msg
		 * @throws Exception
		 */
		public void assign(String msg) throws Exception {
			
			try {
				String command = msg.substring(0, 4);
				String[] arguments = null;
				if (msg.contains(";")) {
					arguments = msg.substring(5, msg.length()).split(";",-1);
					//decode arguments
					for (int i = 0; i < arguments.length; i++) {
						arguments[i] = URLDecoder.decode(arguments[i], "UTF-8");
					}
				}
				
				Protocol protocol = Protocol.valueOf(command);
				switch (protocol) {
				case LGIN:
					loginClient(arguments[0]);
					break;
				case MESS:
					server.sendMessage(arguments[0], this);
					break;
				case LOGO:
					logout();
					break;
				case WHON:
					whoIs();
					break;
				case PING:
					sendToClient(Protocol.PIOK);
					break;
				case PIOK:
					pinger.okReceived();
					break;
				case BEGN:
					createAGame(arguments[0], Integer.parseInt(arguments[1]));
					break;
				case SHOW:
					server.sendOpenGames(this);
					break;
				case STAT:

					break;
				case JOIN:
					joinGame(arguments[0], arguments[1]);
					break;
				case NMCH:
					changeUsername(arguments[0]);
					break;
				case LEAV:
					leaveGame();
					break;
				case STRT:
					startGame();
					break;
				case TURN:
					server.makeTurn(this, Integer.parseInt(arguments[0]));
					break;
				case RQPL:
					server.requestPlayersInMyGame(this);
					break;
				case LVIG:
					server.leaveIngame(this);
					break;
				case HIGH:
					server.receivedHighscoreRequest(this, arguments[0]);
					break;
				default:
					break;
				}
			} catch (StringIndexOutOfBoundsException e){
				// illegal message received from server
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}

		private void startGame() {
			// not in game or both teams doesnt have the same amount of players -> gamestart fails
			if (currentGame == null || currentGame.getTeam1().size() != currentGame.getTeam2().size()) {
				sendToClient(Protocol.STRT, "FAILED");
			} else {
				currentGame.setStarted(true);
				server.gameStarted(currentGame);
			}
		}

		private void leaveGame() {
			server.getMeOutOfTheGame(this);
		}

		private void loginClient(String username) {
			boolean nameAlreadyInUse = server.isNameAlreadyInUse(username);
			int addcount = 1;
			String newname = username;
			while (nameAlreadyInUse) {
				newname = username + "_" + ++addcount;
				nameAlreadyInUse = server.isNameAlreadyInUse(newname);
			}
			username = newname;
			
			this.username = username;
//			System.out.println("Benutzer: " + username + " hat sich angemeldet");
			sendToClient(Protocol.LGIN, username);
			//tell other clients that someone logged in
			server.sendAllWhoIsOn(this);
			server.updateServerFrame();
			
		}

		private void changeUsername(String newName) {
			newName = newName.replaceAll(" ", "_");
			boolean nameAlreadyInUse = server.isNameAlreadyInUse(newName);
			if (!nameAlreadyInUse) {
				username = newName;
				sendToClient(Protocol.NMCH, "DONE", newName);
			} else {
				sendToClient(Protocol.NMCH, "FAILED", newName);
			}
			server.sendAllWhoIsOn(this);
			server.sendWhoIsOn(this);
			if (currentGame != null) {
				server.sendAllPlayersInGameToAllInGame(this);
			}
		}

		private void joinGame(String gameName, String team) {
			//check if client is already in a game
			if (currentGame != null) {
				sendToClient(Protocol.JOIN, "Failed");
				return;
			}
			
			boolean gameFound = false;
			for (ServerGame gme : server.alGames) {
				if (gme.getGameName().equals(gameName)) {
					if (team.equals("1")) {
						gme.getTeam1().add(this);
						currentTeam = 1;
					} else {
						gme.getTeam2().add(this);
						currentTeam = 2;
					}
					sendToClient(Protocol.JOIN, "DONE");
					currentGame = gme;
					gameFound = true;
					
					//tell other players
					server.sendPlayersToAllOtherClientsInMyGame(this);
					server.sendOpenGamesToAll();
					break;
				}
			}
			if (!gameFound) {
				sendToClient(Protocol.JOIN, "FAILED");
			}
		}

		private void createAGame(String gameName, int mapID) throws IOException {
			server.createAGame(gameName, mapID, this);
		}

		 public void sendToClient(Protocol protocol, String... arguments) {
			String linkedmsg = protocol.toString();
			for (int i = 0; i < arguments.length; i++) {
				linkedmsg += ";";
				try {
					linkedmsg += URLEncoder.encode(arguments[i], "UTF-8");
				} catch (UnsupportedEncodingException e) {
					System.err.println("An unsupported encoding was used");
				}
			}
			//System.out.println("Server sends:" + linkedmsg);
			try {
				bw.write(linkedmsg);
				bw.newLine();
				bw.flush();
			} catch (IOException e) {
				if (e instanceof SocketException) {
					//
					return;
				}
				e.printStackTrace();
			}
		}
		
		public void sendToClient(Protocol protocol) {
			String msg = protocol.toString();
			
			//System.out.println("Server sends:" + msg);
			try {
				bw.write(msg);
				bw.newLine();
				bw.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		

		// Die Who is on Liste
		void whoIs() {
			server.sendWhoIsOn(this);
		}

		void logout() {
			pinger.stop();
			leaveGame();
			server.alClients.remove(this);
			server.updateServerFrame();
			server.sendAllWhoIsOn(this);
			try {
				br.close();
				bw.close();
				socket.close();
			} catch (Exception e) {
				//already closed connection
			}// end of try

		}

		@Override
		public void pingFailed() {
			server.pingfailed(this);
			this.reconnectTimer.schedule(new TimerTask() {
				private ServerClient serverClient;

				@Override
				public void run() {
					server.playerReconnectTookTooLong(serverClient);
				}

				public TimerTask init(ServerClient serverClient) {
					this.serverClient = serverClient;
					return this;
				}
			}.init(this), 30000);
			System.out.println("Ping to " + username + " took too long");
		}

		@Override
		public void sendPing() {
			sendToClient(Protocol.PING);
		}

		@Override
		public void pingWorksAgain() {
			reconnectTimer.cancel();
			server.pingWorksAgain(this);
			System.out.println("Ping reappeared");
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}

		/**
		 * links the Client to the Knight (in addition to linking the Knight to the Client).
		 */
		public void assignAvatar(Knight knight) {
			avatar = knight;
		}
		
		/**
		 * links the Client to the Knight (in addition to linking the Knight to the Client).
		 */
		public Knight getAvatar() {
			return avatar;
		}
		
		public ServerGame getCurrentGame() {
			return currentGame;
		}

		public int getTeam() {
			return currentTeam;
		}
}
