package network.interfaces;

import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import network.server.NetworkServer;
import network.server.ServerClient;
import network.server.ServerControler;
import network.server.ServerGame;
import spiellogik.Knight;
import spiellogik.StatusEffect;

/** abstracts the server client communication
 */
public class ServerInterface {

	NetworkServer server;
	private ArrayList<OurEvent> eventList;
	
	public class OurEvent {
		private int y;
		private int x;
		private String event;
		private ServerClient serverClient;

		public ServerClient getServerClient() {
			return serverClient;
		}

		public OurEvent(ServerClient serverClient, String event, int x, int y) {
			this.event = event;
			this.x = x;
			this.y = y;
			this.serverClient = serverClient;
		}

		public int getY() {
			return y;
		}

		public int getX() {
			return x;
		}

		public String getEvent() {
			return event;
		}
	}

	/** creates a new networkserver
	 * @param port
	 * @param serverControler
	 * @throws IOException
	 */
	public ServerInterface(int port, ServerControler serverControler) throws IOException {
		server = new NetworkServer(port, serverControler);
		this.eventList = new ArrayList<OurEvent>();
	}
	
	public NetworkServer getServer(){
		return server;
	}
	
	/** adds a new event to the queue
	 * @param serverClient
	 * @param event
	 * @param x
	 * @param y
	 */
	public void sendEvent(ServerClient serverClient, String event, int x, int y) {
		eventList.add(new OurEvent(serverClient, event, x, y));
	}

	public void waitForClients() {
		server.waitForClients();
	}

	public void update(ServerClient receivingClient, int healthpoints, int actionpoints, Point position,
			boolean[][] fieldOfVision, Vector<Knight> seenKights, Vector<StatusEffect> statuseffects, int grailX, int grailY, boolean carried, boolean yourTurn) {
		server.update(receivingClient, healthpoints, actionpoints, position, fieldOfVision, seenKights, statuseffects, grailX, grailY, carried, yourTurn);
	}

	public void sendServerMessage(String servermessage) {
		server.sendServerMessageToAllClients(servermessage);
	}

	public String[] getAllUsernames() {
		return server.getAllUsernames();
	}

	public ArrayList<ServerGame> getAllGames() {
		return server.getAllGames();
	}
	
	public void endGame(ServerGame game, int teamNumberOfTheTeamWhichWon) {
		server.endGame(game, teamNumberOfTheTeamWhichWon);
	}

	public String[] readOldGames() {
		return server.readOldGames();
	}

	public String[] getAllGameNames() {
		return server.getAllGameNames();
	}

	public void kickPlayer(String name) {
		server.kickPlayer(name);
	}

	/** commits all events in the queue
	 * 
	 */
	public void commitEvents() {
		for (OurEvent event : eventList) {
			server.sendEvent(event.getServerClient(), event.getEvent(), event.getX(), event.getY());
		}
		eventList = new ArrayList<OurEvent>();
	}
}
