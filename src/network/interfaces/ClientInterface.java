package network.interfaces;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;

import network.client.NetworkClient;
import network.client.ClientControler;

/** abstracts the client - server communication and contains some simple methods
 */
public class ClientInterface {
	
	NetworkClient client = null;
	
	/** sets up the connection to the server
	 * @param port
	 * @param host
	 * @param username
	 * @param clientControler
	 * @throws UnknownHostException
	 * @throws ConnectException
	 */
	public ClientInterface(int port, String host, String username, ClientControler clientControler) throws UnknownHostException, ConnectException {
		try {
			Socket s = new Socket(host, port);
			
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream(), "ISO-8859-1"));
			
			BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			
			client = new NetworkClient(username, bw, br, s, clientControler);
		} catch (ConnectException e) {
			throw e;
		} catch (UnknownHostException e) {
			throw e;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void whoIsOn() {
		client.whoIsOn();
	}

	public void logout() {
		client.logout();
	}

	public void beginGame(String gameName, int mapID) {
		client.beginGame(gameName, mapID);
	}

	public void showMeAllGame() {
		client.showMeAllGame();
	}

	public void getStatusOfGame() {
		client.getStatusOfGame();
	}

	public void joinAGame(String gameName, String team) {
		client.joinAGame(gameName, team);
	}

	public void changeName(String newUserName) {
		client.changeName(newUserName);
	}

	public void sendMessage(String input) {
		client.sendMessage(input);
	}

	public void leaveGame() {
		client.leaveGame();
	}

	public void startGame() {
		client.startGame();
	}

	public void makeTurn(int turn) {
		client.makeTurn(turn);
	}

	public String getUsername() {
		return client.getUsername();
	}

	public void requestPlayersInMyGame() {
		client.requestPlayersInMyGame();
	}

	public int getTeam() {
		return client.getTeam();
	}

	public void setTeam(int teamNumber) {
		client.setTeam(teamNumber);
		
	}

	public void leaveIngame() {
		client.leaveIngame();
	}

	public void setUsername(String newName) {
		client.setUsername(newName);
	}

	public void requestHighscore(String name) {
		client.requestHighscore(name);
	}
}
