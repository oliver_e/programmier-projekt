package network;

import gui.client.ClientStartFrame;
import gui.game.GameGUI;
import gui.game.SoundPlayer;
import gui.lobby.LobbyGui;

import java.awt.Point;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.Vector;

import javax.swing.JOptionPane;

import network.client.ClientControler;
import network.client.KnightPicture;
import network.client.MapPicture;
import network.client.StatusEffectPicture;
import network.interfaces.ClientInterface;
import spiellogik.HTGEvent;
import startup.Main;

/**
 * This is the main class of the client and controls pretty much everything
 * which is required.
 * 
 */
public class Client implements ClientControler {

	public static void main(String[] args) {
		new Client();
	}

	public ClientInterface clientInterface = null;
	boolean gameIsRunning = false;
	public final static int TURN_MOVEUP = 1;
	public final static int TURN_MOVEDOWN = 2;
	public final static int TURN_MOVELEFT = 3;
	public final static int TURN_MOVERIGHT = 4;
	public final static int TURN_COMPLETETURN = 5;
	public final static int TURN_PLACETRAP_POISON = 6;
	public final static int TURN_PLACETRAP_STUN = 7;
	public final static int TURN_PLACETRAP_SLOW = 8;
	public final static int TURN_PLACETRAP_REVEAL = 9;
	public final static int TURN_PLACETRAP_BLIND = 10;
	public final static int TURN_PLACETRAP_PURGE = 11;
	public final static int TURN_PLACETRAP_DISPEL = 12;
	public final static int TURN_PLACETRAP_PRONE = 13;

	public GameGUI gameGUI;
	private ClientStartFrame clientStartFrame;
	private boolean clientLoggedInSuccessfully;
	private LobbyGui lobbyGui;

	/**
	 * Logs in client and creates clientframe
	 */
	public Client() {
		clientStartFrame = new ClientStartFrame(this);

		if (Main.port > 0 && Main.ip != null && Main.ip.length() > 7) {
			loginButtonPressed(Main.port, Main.ip,
					System.getProperty("user.name").replaceAll(" ", "_"));
		} else {
			clientStartFrame.setVisible(true);
		}

		while (!clientLoggedInSuccessfully) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		clientStartFrame.setVisible(false);
		clientStartFrame.dispose();

		this.lobbyGui = new LobbyGui(800, 400, this);

		// to fill gui list
		clientInterface.whoIsOn();
		clientInterface.showMeAllGame();
	}


	/**
	 * shows server-message in frames
	 */
	@Override
	public void receivedServerMessage(String serverMessage) {
		if (gameGUI != null)
			gameGUI.addChatMessage(serverMessage);
		if (lobbyGui != null)
			lobbyGui.addChatMessage(serverMessage);
	}

	/**
	 * shows all clients in frame
	 */
	@Override
	public void receivedWhoIsOn(String[] whoIsOn) {
		try {
			lobbyGui.deleteAllPlayers();
			for (String player : whoIsOn) {
				lobbyGui.addPlayer(player);
			}
		} catch (NullPointerException e) {
			// if server sends whoison before lobbygui is created
		}
	}

	/** shows message of client in frame
	 */
	@Override
	public void receivedMessage(String message) {
		if (gameGUI != null)
			gameGUI.addChatMessage(message);
		if (lobbyGui != null)
			lobbyGui.addChatMessage(message);
	}

	/** shows all open game in frame
	 */
	@Override
	public void receivedOpenGames(String[] openGameNames) {
		lobbyGui.updateOpenGames(openGameNames);
	}

	/** change clientframe that is shows 2 teams with players
	 */
	@Override
	public void joinedGame(boolean success) {
		if (success) {
			lobbyGui.changeToJoinedGameMode(null, null);
			clientInterface.requestPlayersInMyGame();
		} else {
			// System.out.println("You couldnt join the game, maybe you misstyped the gamename or you are already in a game");

		}
	}

	/** change clientframe to joinedgamemode
	 */
	@Override
	public void gameCreated(boolean success) {
		if (success) {
			String[] team1 = { clientInterface.getUsername() };
			lobbyGui.changeToJoinedGameMode(team1, null);
			// System.out.println("You have created a game successfully");
		} else {
			lobbyGui.askForNewGameName();
			// System.out.println("You couldnt create a game, maybe the gameName is already given");
		}
	}

	/** changes username locally
	 */
	@Override
	public void changedUsername(boolean success, String newName) {
		if (success) {
			// System.out.println("You changed your username successfully");
			clientInterface.setUsername(newName);
		} else {
			// System.out.println("Couldnt change your username, maybe it is already given");
			lobbyGui.showUsernameWasAlreadyGivenDialogAndAskForNewOne();
		}
	}

	/** set boolean to open the clientframe
	 */
	@Override
	public void loggedIn(String username) {
		// System.out.println("You logged in successfully as "+username);
		clientInterface.setUsername(username);
		clientLoggedInSuccessfully = true;
	}
	
	/** changes username locally
	 */
	@Override
	public void leftGame(boolean success) {
		if (success) {
			lobbyGui.changeToNotInGameMode();
			clientInterface.showMeAllGame();
			clientInterface.whoIsOn();
			// System.out.println("Left game successfully");
		} else {
			// System.out.println("Couldnt leave game (maybe you werent in a game?!)");
		}
	}
	
	/** starts gamegui
	 */
	@Override
	public void gameStarted(int mapID) {
		lobbyGui.setVisible(false);
		MapPicture.setUp(mapID);
		gameGUI = new GameGUI(clientInterface, this);
		// System.out.println("Your game started!");
		gameIsRunning = true;
	}

	/**
	 * @param seenKnights
	 * @param sourceX
	 * @param sourceY
	 * @param grailX
	 * @param grailY
	 * @return true if grail bearer is at (sourceX, sourceY)
	 */
	public boolean isGrailBearerAt(Vector<KnightPicture> seenKnights,
			int sourceX, int sourceY, int grailX, int grailY) {
		for (KnightPicture knight : seenKnights) {
			if (knight.getxCoord() == sourceX && knight.getyCoord() == sourceY
					&& sourceX == grailX && sourceY == grailY) {
				return true;
			}
		}
		return false;
	}

	/** 
	 * @param seenKnights
	 * @param sourceX
	 * @param sourceY
	 * @return true - if seen knight is allied
	 */
	public boolean giveAllegianceAt(Vector<KnightPicture> seenKnights,
			int sourceX, int sourceY) {
		for (KnightPicture knight : seenKnights) {
			if (knight.getxCoord() == sourceX && knight.getyCoord() == sourceY) {
				if (knight.getAllied()) {
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * @param seenKnights
	 * @param sourceX
	 * @param sourceY
	 * @return true if a seen knight is at (sourceX, sourceY)
	 */
	public boolean isPlayerAt(Vector<KnightPicture> seenKnights, int sourceX,
			int sourceY) {
		for (KnightPicture knight : seenKnights) {
			if (knight.getxCoord() == sourceX && knight.getyCoord() == sourceY) {
				return true;
			}
		}
		return false;
	}

	/** show game over screen
	 */
	@Override
	public void gameEnded(int teamNumberOfTheTeamWhichWon) {
		gameGUI.gameEnded(clientInterface.getTeam() == teamNumberOfTheTeamWhichWon);
		gameIsRunning = false;
	}
	

	/** connects to client to server
	 * @param serverPort
	 * @param ip
	 * @param username
	 */
	public void loginButtonPressed(int serverPort, String ip, String username) {
		try {
			clientInterface = new ClientInterface(serverPort, ip, username,
					this);
		} catch (UnknownHostException e) {
			clientStartFrame.requestNewServerIP();
			// System.out.println("Host wurde nicht gefunden bitte geben sie einen existierenden Host an!");
		} catch (ConnectException e) {
			// probably mistyped port
			clientStartFrame.requestNewServerPort();
			// System.out.println("Sie haben vermutlich den Port falsch angegeben!");
		}
	}

	/** requests creating game on server
	 * @param gameName
	 * @param mapID
	 */
	public void createGame(String gameName, int mapID) {
		clientInterface.setTeam(1);
		clientInterface.beginGame(gameName, mapID);
	}

	/** 
	 * @param gameName
	 * @param teamNumber
	 */
	public void joinGame(String gameName, int teamNumber) {
		clientInterface.joinAGame(gameName, "" + teamNumber);
		clientInterface.setTeam(teamNumber);
	}
	
	/**
	 * shows players in lobby
	 */
	@Override
	public void receivedPlayersInGame(String[] team1, String[] team2) {
		lobbyGui.updatePlayersInGame(team1, team2);
	}
	
	/**
	 * requests starting the game
	 */
	public void startButtonPressed() {
		clientInterface.startGame();
	}

	/**
	 * request leaving game
	 */
	public void leaveButtonPressed() {
		clientInterface.leaveGame();
	}
	
	/**
	 * shows user that game couldnt start
	 */
	@Override
	public void gameCouldntStart() {
		lobbyGui.showNoBalancedTeamsMessage();
	}

	/**
	 * sends message to server
	 * @param msg
	 */
	public void chatMessageEntered(String msg) {
		clientInterface.sendMessage(msg);
		lobbyGui.addChatMessage(msg);
	}

	/**
	 * logs out at server
	 */
	public void clientClosedLobby() {
		clientInterface.logout();
		// System.out.println("logged out");
	}

	/**
	 * brings client back to lobby
	 */
	public void leaveGame() {
		if (gameIsRunning) {
			clientInterface.leaveIngame();
		} else {
			clientInterface.leaveGame();
		}

		clientInterface.showMeAllGame();
		clientInterface.whoIsOn();
		lobbyGui.changeToNotInGameMode();
		lobbyGui.setVisible(true);

	}

	/**
	 * @param value
	 * @return true if value equals to the clients username 
	 */
	public boolean isMyName(String value) {
		return clientInterface.getUsername().equals(value);
	}

	/** passes update to the gamegui
	 */
	@Override
	public void update(int healthPoints, int actionPoints, Point position,
			boolean[][] visionField, Vector<KnightPicture> seenKnights,
			Vector<StatusEffectPicture> statuseffects, int grailX, int grailY,
			boolean carried, boolean yourTurn) {
		if (yourTurn) {
		} else {
		}
		gameGUI.update(healthPoints, actionPoints, position.x, position.y,
				visionField, seenKnights, statuseffects, grailX, grailY,
				yourTurn); // add more parameters if needed
	}

	/** requests newname from server
	 * @param newName
	 */
	public void changeName(String newName) {
		clientInterface.changeName(newName);
	}

	/**
	 * closes all frames and exits
	 */
	@Override
	public void gotKicked() {
		if (gameGUI != null) {
			gameGUI.close();
		}
		lobbyGui.setVisible(false);
		clientInterface.logout();
		JOptionPane.showMessageDialog(null, "You got kicked!");
		System.exit(0);
	}

	/**
	 * passes event to soundplayer and gamegui
	 */
	@Override
	public void receivedEvent(String event, int x, int y) {
		new SoundPlayer(event);
		if (event.equals(HTGEvent.DAMAGE_DISPEL) || event.equals(HTGEvent.DAMAGE_HIT) || event.equals(HTGEvent.DAMAGE_POISON) || event.equals(HTGEvent.DAMAGE_PURGE) || event.equals(HTGEvent.DAMAGE_PURGE_SELF)) {
			gameGUI.getGameMap().update(event, x, y);
		}
	}


	public void highscoreRequested(String name) {
		clientInterface.requestHighscore(name);
	}


	@Override
	public void receivedHighscore(String name, int value) {
		lobbyGui.showHighscore(name, value);
	}

}
