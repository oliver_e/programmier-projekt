package network.client;

public class StatusEffectPicture {

	private int type;
	private int duration;
	
	public static final int POISONED = 1;
	public static final int REVEALED = 2;
	public static final int BLINDED = 3;
	public static final int SLOWED = 4;
	public static final int STUNNED = 5;
	public static final int PRONE = 6;
	
	
	public StatusEffectPicture(int type, int duration) {
		this.type = type;
		this.duration = duration;
	}
	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
}
