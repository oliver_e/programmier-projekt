package network.client;

import java.awt.Point;
import java.util.Vector;

/**
 * these events are called from the network-client to notify the client
 */
public interface ClientControler {

	void receivedServerMessage(String serverMessage);

	void receivedWhoIsOn(String[] players);

	void receivedMessage(String message);

	void receivedOpenGames(String[] openGameNames);

	void joinedGame(boolean success);

	void gameCreated(boolean success);

	void changedUsername(boolean success, String newName);

	void loggedIn(String username);

	void leftGame(boolean success);

	void gameStarted(int mapID);

	void update(int healthPoints, int actionPoints, Point position,
			boolean[][] visionField, Vector<KnightPicture> seenKnights,
			Vector<StatusEffectPicture> statuseffects, int grailX, int grailY, boolean carried, boolean b);

	void gameEnded(int teamNumberOfTheTeamWhichWon);
	
	void receivedPlayersInGame(String[] team1, String[] team2);

	void gameCouldntStart();

	void gotKicked();

	void receivedEvent(String event, int x, int y);

	void receivedHighscore(String name, int value);

}
