package network.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.URLEncoder;

import network.Protocol;
import network.pinger.OurPinger;
import network.pinger.Pinger;

/**
 * the network-client is responsible for the communication with the server
 */
public class NetworkClient implements Pinger{

//	private int port = 12345;
//	private String host = "localhost";
	private Socket socket;
	private BufferedWriter bw;
	private BufferedReader br;
	private String username;
	private ServerListener serverListener;
	private OurPinger pinger;
	private int team;

	/** logs in client at first and then starts a serverlistener
	 * @param username
	 * @param bw
	 * @param br
	 * @param socket
	 * @param clientControler
	 */
	public NetworkClient(String username, BufferedWriter bw, BufferedReader br, Socket socket, ClientControler clientControler) {
		
		this.socket = socket;
		this.bw = bw;
		this.username = username;
		this.br = br;
		
		sendToServer(Protocol.LGIN, username);

		pinger = new OurPinger(5000, this);
		
		serverListener = new ServerListener(br, pinger, this, clientControler);
		serverListener.start();
	}

	/** sends all arguments to server by concatenating them
	 * @param protocol
	 * @param arguments
	 */
	private void sendToServer(Protocol protocol, String... arguments) {
		String linkedMsg = protocol.toString();
		for (int i = 0; i < arguments.length; i++) {
			linkedMsg += ";";
			try {
				linkedMsg += URLEncoder.encode(arguments[i], "UTF-8");
			} catch (UnsupportedEncodingException e) {
				System.err.println("An unsupported encoding was used");
			}
		}
		//System.out.println("Client sends:" + linkedMsg);
		try {
			bw.write(linkedMsg);
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** sends protocol to server without any arguments
	 * @param protocol
	 */
	private void sendToServer(Protocol protocol) {
		String msg = protocol.toString();
		//System.out.println("Client sends:" + linkedMsg);
		try {
			bw.write(msg);
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			if (e.getMessage().equals("Stream closed")) {
				//client logged out prolly
			} else e.printStackTrace();
		}
	}

	

	public void logout() {
		sendToServer(Protocol.LOGO);
		pinger.stop();
		try {
			socket.close();
			br.close();
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void beginGame(String gameName, int mapID) {
		Protocol protocol = Protocol.BEGN;
		sendToServer(protocol, gameName, "" + mapID);
	}

	public void changeName(String newUserName) {
		sendToServer(Protocol.NMCH, newUserName);
	}

	public void joinAGame(String gameName, String team) {
		sendToServer(Protocol.JOIN, gameName, team);
	}

	@Override
	public void pingFailed() {
		System.out.println("Ping held off");
	}

	@Override
	public void sendPing() {
		sendToServer(Protocol.PING);
	}

	public void receivedPing() {
		sendToServer(Protocol.PIOK);
	}

	@Override
	public void pingWorksAgain() {
		System.out.println("Ping reappeared");
	}

	public void showMeAllGame() {
		sendToServer(Protocol.SHOW);
	}

	public void whoIsOn() {
		sendToServer(Protocol.WHON);
	}

	public void getStatusOfGame() {
		sendToServer(Protocol.STAT);
	}

	public void sendMessage(String input) {
		sendToServer(Protocol.MESS, input);
	}

	public void leaveGame() {
		sendToServer(Protocol.LEAV);
	}

	public void startGame() {
		sendToServer(Protocol.STRT);
	}

	public void makeTurn(int turn) {
		sendToServer(Protocol.TURN, String.valueOf(turn));
	}

	public String getUsername() {
		return username;
	}

	public void requestPlayersInMyGame() {
		sendToServer(Protocol.RQPL);
	}
	
	public void setTeam(int team){
		this.team = team;
	}

	public int getTeam() {
		return team;
	}

	public void leaveIngame() {
		sendToServer(Protocol.LVIG);
	}

	public void setUsername(String newName) {
		this.username = newName;
	}

	public void requestHighscore(String name) {
		sendToServer(Protocol.HIGH, name);
	}

	
}
