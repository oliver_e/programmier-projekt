package network.client;

/**
 * simple class to organize seen knights on client-side
 */
public class KnightPicture {
	private int xCoord;
	private int yCoord;
	private int direction;
	private boolean allied;
	/**
	 * 
	 * @param allied true if same team.
	 * @param xCoord
	 * @param yCoord
	 */
	public KnightPicture(boolean allied, int xCoord, int yCoord, int direction) {
		this.allied = allied;
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.setDirection(direction);
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the xCoord
	 */
	public int getxCoord() {
		return xCoord;
	}
	public boolean getAllied() {
		return allied;
	}
	/**
	 * @param xCoord the xCoord to set
	 */
	public void setxCoord(int xCoord) {
		this.xCoord = xCoord;
	}
	/**
	 * @return the yCoord
	 */
	public int getyCoord() {
		return yCoord;
	}
	/**
	 * @param yCoord the yCoord to set
	 */
	public void setyCoord(int yCoord) {
		this.yCoord = yCoord;
	}
	/**
	 * @return the team
	 */
	public boolean isAllied() {
		return allied;
	}
	public int getDirection() {
		return direction;
	}
	public void setDirection(int direction) {
		this.direction = direction;
	}
}
