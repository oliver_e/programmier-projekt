package network.client;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OptionalDataException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URLDecoder;
import java.util.Vector;

import network.Protocol;
import network.pinger.OurPinger;

/** this thread is listening for new messages from server and passes them to client
 *
 */
class ServerListener extends Thread {
	private static final String DONE = "DONE";
	private BufferedReader br;
	private OurPinger ourPinger;
	private NetworkClient client;
	private ClientControler clientControler;
	private String msg;

	public ServerListener(BufferedReader br, OurPinger ourPinger, NetworkClient client, ClientControler clientControler) {
		this.clientControler = clientControler;
		this.br = br;
		this.ourPinger = ourPinger;
		this.client = client;
	}

	/** if the client received a message from the server it will be assigned and passed
	 */
	public void run() {
		
		while (true) {
			try {
				msg = br.readLine();
				Protocol protocol = Protocol.valueOf(msg.substring(0, 4));
				String[] arguments = null;
				if (msg.contains(";")) {
					arguments = msg.substring(5, msg.length()).split(";",-1);
					//decode arguments
					for (int i = 0; i < arguments.length; i++) {
						arguments[i] = URLDecoder.decode(arguments[i], "UTF-8");
					}
				}
				
				
				
				//System.out.println("Client received:" + msg);
				
				switch (protocol) {
				case LGIN:
					clientControler.loggedIn(arguments[0]);
					break;
				case PING:
					client.receivedPing();
					break;
				case PIOK:
					ourPinger.okReceived();
					break;
				case MSRC:
					// Server received MSG
					break;
				case SVMS:
					clientControler.receivedServerMessage(arguments[0]);
					break;
				case WHON:
					clientControler.receivedWhoIsOn(arguments[0].split(","));
					break;
				case MESS:
					clientControler.receivedMessage(arguments[0]);
					break;
				case BEGN:
					if (arguments[0].equals(DONE)) {
						clientControler.gameCreated(true);
					} else {
						clientControler.gameCreated(false);
					}
					break;
				case SHOW:
					clientControler.receivedOpenGames(arguments[0].split(","));
					break;
				case JOIN:
					if (arguments[0].equals(DONE)) {
						clientControler.joinedGame(true);
					} else {
						clientControler.joinedGame(false);
					}
					
					break;
				case NMCH:
					if (arguments[0].equals(DONE)) {
						clientControler.changedUsername(true, arguments[1]);
					} else {
						clientControler.changedUsername(false, arguments[1]);
					}
					break;
				case LEAV:
					if (arguments[0].equals(DONE)) {
						clientControler.leftGame(true);
					} else {
						clientControler.leftGame(false);
					}
					break;
				case STRT:
					if (arguments[0].equals(DONE)) {
						clientControler.gameStarted(Integer.parseInt(arguments[1]));
					} else {
						clientControler.gameCouldntStart();
					}
					break;
				case UPDT:
					pieceTogetherUpdate(arguments);
					break;
				case GMOV:
					endGame(arguments[0]);
					break;
				case RQPL:
					receivedPlayersInGame(arguments[0], arguments[1]);
					break;
				case KICK:
					gotKicked();
					break;
				case EVNT:
					clientControler.receivedEvent(arguments[0], Integer.parseInt(arguments[1]), Integer.parseInt(arguments[2]));
					break;
				case HIGH:
					clientControler.receivedHighscore(arguments[0], Integer.parseInt(arguments[1]));
					break;
				default:
					break;
				}
			} catch (OptionalDataException e) {
				System.err.println("end of file:" + e.eof);
			} 
			catch (SocketTimeoutException e) {
				//probably disconnected from server
				//wait some time for reconnect
				try {
					sleep(500);
				} catch (InterruptedException e1) {
					System.err.println("Socket timeout was user interrupted");
				}
			}
			catch (SocketException e) {
				if (e.getMessage().equals("Connection reset")) {
					System.out.println("Server shut down. Terminating client...");
					System.exit(0);
				} else if (e.getMessage().equals("socket closed")) {
					//client logged off
				} else e.printStackTrace();
			}
			catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			catch (IOException e) {
				if (e.getMessage().equals("Stream closed")) {
					// client logged out
					return;
				}
				e.printStackTrace();
			} catch (StringIndexOutOfBoundsException e) {
				//illegal message received from server
			}
		} // end of while
	}

	private void gotKicked() {
		clientControler.gotKicked();
	}

	private void receivedPlayersInGame(String playerTeam1, String playerTeam2) {
		String[] team1 = playerTeam1.split(",");
		String[] team2 = playerTeam2.split(",");
		clientControler.receivedPlayersInGame(team1, team2);
	}

	private void endGame(String teamNumber) {
		clientControler.gameEnded(Integer.parseInt(teamNumber));
	}

	/** the whole game update is packed into strings with delimiter and must be decoded
	 * @param arguments
	 */
	private void pieceTogetherUpdate(String[] arguments) {
		int healthPoints = Integer.parseInt(arguments[0]);
		int actionPoints = Integer.parseInt(arguments[1]);
		Point position = new Point(Integer.parseInt(arguments[2].split(",")[0]), Integer.parseInt(arguments[2].split(",")[1]));
		
		String fieldOfVision = arguments[3];
		String[] columns = fieldOfVision.split(",");
		boolean[][] visionField = new boolean[columns.length][];
		for (int i = 0; i < columns.length; i++) {
			//remove braces
			String booleansWithoutBraces = columns[i].substring(1, columns[i].length()-1);
			String[] booleans = booleansWithoutBraces.split("-");
			visionField[i] = new boolean[booleans.length];
			for (int j = 0; j < booleans.length; j++) {
				visionField[i][j] = booleans[j].equals("1");
			}
		}
		
		Vector<KnightPicture> seenKnights = new Vector<KnightPicture>();
		if (arguments[4].length() != 0) {
			String[] knights = arguments[4].split(",");
			for (String knight : knights) {
				//remove braces
				knight = knight.substring(1, knight.length()-1);
				seenKnights.add(new KnightPicture((knight.split("#")[2]).equals("t"), Integer.parseInt(knight.split("#")[0]), Integer.parseInt(knight.split("#")[1]), Integer.parseInt(knight.split("#")[3])));
			}
		}
		
		
		Vector<StatusEffectPicture> statuseffects = new Vector<StatusEffectPicture>();
		if (arguments[6].length() != 0) {
			String[] effects = arguments[6].split(",");
			for (String effect : effects) {
				effect = effect.substring(1, effect.length()-1);
				statuseffects.add(new StatusEffectPicture(Integer.parseInt(effect.split("#")[0]), Integer.parseInt(effect.split("#")[1])));
			}
		}
		String[] grailStats = arguments[5].split(",");
		int grailX = Integer.parseInt(grailStats[0]);
		int grailY = Integer.parseInt(grailStats[1]);
		boolean carried = grailStats[2].equals("t");
		
		String yourTurn = arguments[7];
		
		clientControler.update(healthPoints, actionPoints, position, visionField, seenKnights, statuseffects, grailX, grailY, carried, yourTurn.equals("t") ? true : false);
	}

}