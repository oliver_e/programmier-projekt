package network.client;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MapPicture {
	
	public static final int DISPLAYDIMENSIONS = 21; //How big of an area will be displayed on screen. Should match up with 2*SIGHTRANGE+1 on serversided VisionInfo;

	public static final int MAPSIZEX = 128;
	public static final int MAPSIZEY = 128;
	
	private static final byte[][] mapArray = new byte[MAPSIZEX][MAPSIZEY];
	

	public static final byte UP = 1;
	public static final byte RIGHT = 2;
	public static final byte DOWN = 3;
	public static final byte LEFT = 4;
	
	public static final byte COLLISIONTHRESHOLD = 64;
	public static final byte VISIONTHRESHOLD = 68;
	
	
	/**
	 * Reads a map from a .htgmap file.
	 * @return true if successful
	 */
	public static boolean setUp(int mapID) {
		InputStream resource = ClassLoader.getSystemClassLoader().getResourceAsStream("files/maps/" + mapID + ".htgmap");
		BufferedInputStream mapSteam = new BufferedInputStream (resource);
		byte[] data = new byte[10+MapPicture.MAPSIZEX*MapPicture.MAPSIZEY];
		try {
			for (int j = 0; j < data.length; j++) {
				data[j] = (byte) mapSteam.read();
			}
			mapSteam.close();
		} catch (IOException e2) {}
		
		
		for (int i = 0; i < data.length - 10; i++) {
			mapArray[i%MAPSIZEX][i/MAPSIZEX] = data[i + 10];
		}
		
		
		return false;
	}
	
	public static byte[][] getMapArray() {
		return mapArray;
	}
	
	/**
	 * Returns the type of the tile at (x,y).
	 * @param x
	 * @param y
	 * @return type of tile or 0 if failed.
	 */

	public static byte getType(int x, int y){
		if ((x < 0 || x >= MAPSIZEX) || (y < 0 || y >= MAPSIZEY)) { //check if out of bound;
			//System.out.println("terrain out of bound");
			return 0;
		}
		//System.out.println("terrain for x="+x+", y="+y+" is "+mapArray[x][y]);
		return mapArray[x][y];
	}
	
	/**
	 * returns a part of the terrain map around a central tile for display purposes.
	 * @param centerX
	 * @param centerY
	 * @return
	 */
	public static byte[][] specifyAt(int centerX, int centerY){
		byte[][] result = new byte[DISPLAYDIMENSIONS][DISPLAYDIMENSIONS];
		int sourceX, sourceY;
		for (int y = 0; y < DISPLAYDIMENSIONS; y++) {
			for (int x = 0; x < DISPLAYDIMENSIONS; x++) {
				sourceX = centerX-(DISPLAYDIMENSIONS/2)+x;
				sourceY = centerY-(DISPLAYDIMENSIONS/2)+y;
				if ((sourceX < 0 || sourceX > MAPSIZEX) || (sourceY < 0 || sourceY > MAPSIZEY)){
					result[x][y] = (byte) 0;
				} else {
					result[x][y] = mapArray[sourceX][sourceY];
				}
			}
		}
		return result;
	}
	
	/**
	 * Checks if the position has terrain collision.
	 */
	public static boolean isTerraClear(int x, int y) {
		int type = getType(x,y);
		if (type >= COLLISIONTHRESHOLD || type == 0) {
			return false;
		} else {
			return true;
		}
	}
}
