package network;

/**
 * This enumeration defines our protocol. </br>
 * Every request has the following structure: </br>
 * 4-char-command;arg0;arg1;...;argn </br>
 * You can find specific arguments for every command below.
 */
public enum Protocol {
	/** MESSAGE
	 * arguments:</br>
	 * Client -> Server: message (/w tim hallo) </br>
	 * Server -> Client: message (tim fl�stert hallo)
	 */
	MESS,
	/** LOGOUT
	 * Client -> Server:</br>
	 * Server -> Client:
	 */
	LOGO,
	/** WHO IS ON
	 * arguments:</br>
	 * Client -> Server:</br>
	 * Server -> Client: list-of-Users (player1, player2, player3...)
	 */
	WHON,
	/** BEGIN GAME
	 * arguments:</br>
	 * Client -> Server: gameName</br>
	 * Server -> Client: DONE/FAILED
	 */
	BEGN,
	/** SHOW GAMES
	 * arguments:</br>
	 * Client -> Server:</br>
	 * Server -> Client: list-of-games
	 */
	SHOW,
	/** STATUS
	 * not implemented yet!
	 */
	STAT,
	/** JOIN A GAME
	 * arguments:</br>
	 * Client -> Server: gameName, teamNumber</br>
	 * Server -> Client: DONE/FAILED
	 */
	JOIN,
	/** NAMECHANGE
	 * arguments:</br>
	 * Client -> Server: newName</br>
	 * Server -> Client: DONE/FAILED
	 */
	NMCH,
	/** MESSAGE RECEIVED
	 * no arguments
	 */
	MSRC,
	/** PING
	 * no arguments
	 */
	PING,
	/**	PING ERHALTEN
	 * no arguments
	 */
	PIOK,
	/** SERVERMESSAGE
	 * arguments:</br>
	 * Server -> Client: message
	 */
	SVMS,
	/** LOGIN
	 * arguments:</br>
	 * Client -> Server: username (proposed)</br>
	 * Server -> Client: username (effective)
	 */
	LGIN,
	/** LEAVE GAME </br>
	 * Server -> Client: DONE/FAILED
	 */
	LEAV,
	/** START GAME
	 * Server -> Client: DONE/FAILED
	 */
	STRT, 
	/** UPDATE
	 * Server -> Client: healthpoints, actionpoints, (x,y), fieldOfVision, seenKnights, statuseffects
	 */
	UPDT, 
	/** REQUEST TURN
	 * Server -> Client:
	 * Client -> Server: moveCode
	 */
	RQTN, 
	/** MAKE A TURN
	 * Client -> Server: type-of-turn e.q. ClientTest.TURN_MOVEUP
	 */
	TURN,
	/** GAME OVER
	 * Server -> Client: team that won (1/2)
	 */
	GMOV,
	/**
	 * Request Players in Game
	 */
	RQPL, 
	/**
	 * Leave ingame
	 */
	LVIG, 
	/**
	 * Kick player
	 * only from Server -> Client
	 */
	KICK, 
	/**
	 * Event happened
	 * Server -> Client: event
	 */
	
	EVNT, 
	/**
	 * Request / send highscore
	 * client -> server: name
	 * server -> client: name, highscore
	 */
	HIGH
}
