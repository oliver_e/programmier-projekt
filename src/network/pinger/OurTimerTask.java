package network.pinger;

import java.util.TimerTask;

/** notifies pinger (client/server) that ping fails / works again
 */
public class OurTimerTask extends TimerTask {
	private boolean okReceived;
	private Pinger pinger;
	private boolean pingFailed;

	public OurTimerTask(Pinger pinger) {
		this.pinger = pinger;
		// first run
		okReceived = true;
		pingFailed = false;
	}

	@Override
	public void run() {
		if (okReceived) {
			if (pingFailed) {
				pinger.pingWorksAgain();
				pingFailed = false;
			}
			okReceived = false;
			pinger.sendPing();
		} else {
			if (!pingFailed) {
				pingFailed = true;
				pinger.pingFailed();
			}
		}
	}

	public void okReceived() {
		okReceived = true;
	}

}
