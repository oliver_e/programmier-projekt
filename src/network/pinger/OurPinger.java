package network.pinger;

import java.util.Timer;

/** is pinging client/server in a constant time and manages received ping-oks
 */
public class OurPinger extends Timer {
	private OurTimerTask tt;
	public OurPinger(int time, Pinger pinger) {
		super();
		tt = new OurTimerTask(pinger);
		super.schedule(tt, 2000, time);
	}

	public void okReceived() {
		tt.okReceived();
	}

	public void stop() {
		tt.cancel();
	}
}
