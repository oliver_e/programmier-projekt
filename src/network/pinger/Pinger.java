package network.pinger;

public interface Pinger {
	public void pingFailed();
	public void sendPing();
	public void pingWorksAgain();
}
