package gui.game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Vector;

import javax.swing.JPanel;

import network.client.KnightPicture;
import network.client.MapPicture;
import network.client.StatusEffectPicture;
import network.interfaces.ClientInterface;
import spiellogik.VisionInfo;

@SuppressWarnings("serial")
public class Minimap extends JPanel{
	
	private BufferedImage minimap;
	private BufferedImage fog;
	private Graphics2D fographics;
	private int rgb;
	private Vector<KnightPicture> seenKnights;
	private int grailX;
	private int grailY;
	private int knightX;
	private int knightY;
	private boolean blinded;
	private ClientInterface client;
	
	/**Returns a JPanel with each field of the mapArray draws as a 2x2-pixel rectangle in a colour corresponding to the tile type.
	 * 
	 * @param mapArray mapArray from which you want a minimap.
	 */
	public Minimap(byte[][] mapArray, ClientInterface client) {
		this.client = client;
		setPreferredSize(new Dimension(2*MapPicture.MAPSIZEX, 2*MapPicture.MAPSIZEY));
		minimap = new BufferedImage(2*MapPicture.MAPSIZEX, 2*MapPicture.MAPSIZEY, BufferedImage.TYPE_INT_RGB);
		fog = new BufferedImage(2*MapPicture.MAPSIZEX, 2*MapPicture.MAPSIZEY, BufferedImage.TYPE_INT_ARGB);
		fographics = fog.createGraphics();
		fographics.setColor(new Color(0, 0, 0, 100));
		fographics.setBackground(new Color(0, 0, 0, 0));
		for (int i = 0; i < MapPicture.MAPSIZEY; i++) {
			for (int j = 0; j < MapPicture.MAPSIZEX; j++) {	
				switch (MapPanel.terrainTypeToTileID(mapArray[i][j])) {
				case MapPanel.GRASS:
					rgb = Integer.parseInt("007F0E", 16);
					break;
				case MapPanel.WATER:
					rgb = Integer.parseInt("00137F", 16);
					break;
				case MapPanel.PAVEMENT:
					rgb = Integer.parseInt("808080", 16);
					break;
				case MapPanel.STONEFLOOR:
					rgb = Integer.parseInt("000000", 16);
					break;
				case MapPanel.BRIDGE:
					rgb = Integer.parseInt("7F3300", 16);
					break;
				case MapPanel.MOUNTAIN:
					rgb = Integer.parseInt("000000", 16);
					break;
				case MapPanel.WALL:
					rgb = Integer.parseInt("404040", 16);
					break;
				case MapPanel.GOAL1:
					rgb = Integer.parseInt("0094FF", 16);
					break;
				case MapPanel.GOAL2:
					rgb = Integer.parseInt("FF6A00", 16);
					break;
				case MapPanel.SPAWN1:
					rgb = Integer.parseInt("0026FF", 16);
					break;
				case MapPanel.SPAWN2:
					rgb = Integer.parseInt("AA0000", 16);
					break;
				case MapPanel.PEDESTAL:
					rgb = Integer.parseInt("808080", 16);
					break;
				case MapPanel.MUD:
					rgb = Integer.parseInt("7F1C00", 16);
					break;
				case MapPanel.SAND:
					rgb = Integer.parseInt("7F6A00", 16);
					break;
				default:
					rgb = Integer.parseInt("FFFFFF", 16);
					break;
				}
				minimap.setRGB(2*i, 2*j, rgb);
				minimap.setRGB(2*i, 2*j+1, rgb);
				minimap.setRGB(2*i+1, 2*j, rgb);
				minimap.setRGB(2*i+1, 2*j+1, rgb);
			}
		}
	}
	
	/**
	 * updates the minimap's seenKnights and grail position.
	 * @param seenKnights the new knights that need to be displayed
	 * @param grailX x-Position of the grail
	 * @param grailY y-Position of the grail
	 */
	public void update(Vector<KnightPicture> seenKnights, int grailX, int grailY, int x, int y, Vector<StatusEffectPicture> statEffects) {
		this.seenKnights = seenKnights;
		this.grailX = grailX;
		this.grailY = grailY;
		knightX = x;
		knightY = y;
		blinded = false;
		for (StatusEffectPicture eff : statEffects) {
			if (eff.getType() == StatusEffectPicture.BLINDED) {
				blinded = true;
				break;
			}
		}
		updateVision();
	}
	
	private void updateVision() {
		fographics.clearRect(0, 0, 2*MapPicture.MAPSIZEX, 2*MapPicture.MAPSIZEY);
		fographics.fillRect(0, 0, 2*MapPicture.MAPSIZEX, 2*MapPicture.MAPSIZEY);
		for (KnightPicture knight : seenKnights) {
			if (knight.isAllied()) {
				int sightrange = blinded ? 1 : VisionInfo.SIGHTRANGE;
				int knightX = knight.getxCoord();
				int knightY = knight.getyCoord();
				for (int i = 0; i < 2 * sightrange + 1; i++) {
					for (int j = 0; j < 2 * sightrange + 1; j++) {
						if (getDist(knightX, knightY, (knightX - sightrange) + i, (knightY - sightrange) + j) <= (sightrange+0.25)) {
							if (legolas(knightX, knightY, (knightX - sightrange) + i, (knightY - sightrange) + j)) {
								fographics.clearRect(2*((knightX - sightrange) + i), 2*((knightY - sightrange) + j), 2, 2);
							}
						}
					}
				}
			}
		}
	}
	
	private double getDist(int x1, int y1, int x2, int y2){
		return Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
	}
	
	/**
	 * draws the minimap-image created in the constructor and draws allied knights, enemy knights and the grail position(if seen)
	 */
	public void paint(Graphics g) {
		g.drawImage(minimap, 0, 0, null);
		g.drawImage(fog, 0, 0, null);
		if (seenKnights != null) {
			for (KnightPicture knight : seenKnights) {
				if (client.getTeam() == 1) {
					if (knight.isAllied()) {
						g.setColor(Color.CYAN);
						g.fillRect(2*knight.getxCoord(), 2*knight.getyCoord(), 2, 2);
					} else {
						g.setColor(Color.RED);
						g.fillRect(2*knight.getxCoord(), 2*knight.getyCoord(), 2, 2);
					}
				}
				else {
					if (knight.isAllied()) {
						g.setColor(Color.RED);
						g.fillRect(2*knight.getxCoord(), 2*knight.getyCoord(), 2, 2);
					} else {
						g.setColor(Color.CYAN);
						g.fillRect(2*knight.getxCoord(), 2*knight.getyCoord(), 2, 2);
					}
				}
			}
		}
		g.setColor(Color.YELLOW);
		g.fillRect(2*grailX, 2*grailY, 2, 2);
		g.setColor(Color.WHITE);
		g.fillRect(2*knightX, 2*knightY, 2, 2);
	}
	
	private boolean legolas(int xPos, int yPos, int targetX, int targetY) {
		if (xPos==targetX && yPos==targetY) return true; //trivial case
		
		int dx =  Math.abs(targetX-xPos), sx = xPos<targetX ? 1 : -1;
		int dy = -Math.abs(targetY-yPos), sy = yPos<targetY ? 1 : -1;
		int err = dx+dy, e2; /* error value e_xy */
		 
		for(int failsafe = 0; failsafe < 1000; failsafe++){  // loops until destination reached or opaque block encountered.
			e2 = 2*err;
			if (e2 > dy) { err += dy; xPos += sx; } /* e_xy+e_x > 0 */
			if (e2 < dx) { err += dx; yPos += sy; } /* e_xy+e_y < 0 */
			if (xPos==targetX && yPos==targetY) return true; // breaks with true if destination reached.
			if ((MapPicture.getType(xPos, yPos) >= MapPicture.VISIONTHRESHOLD)) return false; // breaks with false if obstructed and not destination.
		}
		return false; //should never be reached
	}
}
