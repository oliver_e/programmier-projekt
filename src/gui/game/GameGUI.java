package gui.game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLayer;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

import network.Client;
import network.client.KnightPicture;
import network.client.MapPicture;
import network.client.StatusEffectPicture;
import network.interfaces.ClientInterface;

public class GameGUI implements WindowListener {
	
	public static final int RESOLUTION = 32;

	private static final String TURN = "It's your turn";
	private static final String TURNNOT = "It's not your turn";
	
	private ClientInterface client;
	private Client clientTest;
	private JFrame mainFrame;
	private MapPanel gameMap;
	JLayer<JComponent> mapLayer;
	private StatLabel stats;
	private Minimap minimap;
	private StatEffects effects;
	private TrapPlacer trapPlacer;
	private MoveButtons moveButtons;
	private JTextArea chatDisplay;
	private JTextArea chatInput;
	private JButton endTurn;
	private boolean running = true;
	
	private int oldaction;
	private int oldhealth;

	private JTextArea turnDisplay;

	private boolean setOnFront;

	
	public GameGUI(ClientInterface client, Client clientTest) {
		this.client = client;
		this.clientTest = clientTest;
		
		mainFrame = new JFrame();
		mainFrame.setLayout(new FlowLayout());
//		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mainFrame.setPreferredSize(new Dimension(1120, 730));
		mainFrame.addWindowListener(this);
		gameMap = new MapPanel(client);
		gameMap.setPreferredSize(new Dimension(672, 672));
		mapLayer = new JLayer<JComponent>(gameMap, gameMap.getLayerUI());
		mapLayer.setDoubleBuffered(true);
		stats = new StatLabel(100, 10);
		minimap = new Minimap(MapPicture.getMapArray(), client);
		effects = new StatEffects();
		trapPlacer = new TrapPlacer(client);
		moveButtons = new MoveButtons(client);
		endTurn = new JButton("END TURN");
		endTurn.setPreferredSize(new Dimension(150, 70));
		
		chatDisplay = new JTextArea();
		((DefaultCaret)chatDisplay.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		chatDisplay.setEditable(false);
		JScrollPane chatDisplayS = new JScrollPane(chatDisplay);
		chatDisplayS.setPreferredSize(new Dimension(minimap.getPreferredSize().width, 300));
		chatInput = new JTextArea();
		JScrollPane chatInputS = new JScrollPane(chatInput);
		chatInputS.setPreferredSize(new Dimension(minimap.getPreferredSize().width, 30));
		
		chatInput.addKeyListener(new MyChatKeyListener());
		endTurn.addActionListener(new EndListener());
		
		turnDisplay = new JTextArea(1,1);
		turnDisplay.setEditable(false);
		setTurnDisplay(false);
		
//		chatDisplayS.setFocusable(false);
//		chatInputS.setFocusable(false);
//		chatDisplay.setFocusable(false);
//		chatInput.setFocusable(false);
//		gameMap.setFocusable(true);
		

		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		leftPanel.add(mapLayer);
		mainFrame.add(leftPanel);
		
		JPanel midPanel = new JPanel();
		midPanel.setLayout(new BoxLayout(midPanel, BoxLayout.Y_AXIS));
		midPanel.add(stats);
		midPanel.add(turnDisplay);
		midPanel.add(chatDisplayS);
		midPanel.add(chatInputS);
		midPanel.add(minimap);
		mainFrame.add(midPanel);

		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		rightPanel.add(effects);
		rightPanel.add(trapPlacer);
		rightPanel.add(moveButtons);
		JPanel endTurnButtonPanel = new JPanel();
		endTurnButtonPanel.setPreferredSize(new Dimension(150, 75));
		endTurnButtonPanel.add(endTurn);
		rightPanel.add(endTurnButtonPanel);
		mainFrame.add(rightPanel);
		
		mainFrame.pack();
		mainFrame.setVisible(true);
		
		mainFrame.addKeyListener(new MyKeyListener());
		mainFrame.addMouseListener(new MyMouseListener());
		
	}
	
	/**
	 *  Updates all the valid information.
	 * @param health
	 * @param action
	 * @param x The current x-coordinate of the Knight the vision is centered on.
	 * @param y The current y-coordinate of the Knight the vision is centered on.
	 * @param visionField
	 * @param seenKnights
	 * @param statuseffects
	 * @param grailX
	 * @param grailY
	 * @param yourTurn 
	 */
	public void update(int health, int action, int x, int y, boolean[][] visionField, Vector<KnightPicture> seenKnights, Vector<StatusEffectPicture> statuseffects, int grailX, int grailY, boolean yourTurn){
		if (yourTurn) {
			if (!setOnFront) {
				setOnFront = true;
				setTurnDisplay(true);
				mainFrame.toFront();
				mainFrame.requestFocus();
				mainFrame.setExtendedState(JFrame.ICONIFIED);
				mainFrame.setExtendedState(JFrame.NORMAL);
			}
			
		} else {
			setOnFront = false;
			setTurnDisplay(false);
		}
		
		
		gameMap.update(x, y, visionField, seenKnights, grailX, grailY);
		stats.update(health, action);
		minimap.update(seenKnights, grailX, grailY, x, y, statuseffects);
		effects.update(statuseffects);
		mainFrame.repaint();
		
		//automated turn ending when AP falls to 0
		if (yourTurn) {
			if ((oldaction != 0 && action == 0) || (oldhealth != 0 && health == 0)) {
				client.makeTurn(Client.TURN_COMPLETETURN);
			}
		}
		oldaction = action;
		oldhealth = health;
	}
	
	public void addChatMessage(String message){
		chatDisplay.append(message + System.lineSeparator());
	}
	
	private void addOwnMessage(String msg) {
		// whisper
		if (msg.startsWith("/w ") && msg.length() > 5) {
			String receivingPerson = null;
			try {
				receivingPerson = msg.split(" ")[1];
				msg = msg.substring(receivingPerson.length() + 4, msg.length());
			} catch (StringIndexOutOfBoundsException e) {
				//server sends usage automatically
				return;
			}
			addChatMessage("[Private] (to " + receivingPerson + ") " + msg);
		}
		// game
		else if (msg.startsWith("/g ")) {
			addChatMessage("[Game] " + msg.substring(3));
		}
		// team
		else if (msg.startsWith("/t ")) {
			addChatMessage("[Team] " + msg.substring(3));
			
		} else {
			addChatMessage("[Global] " + msg);
		}
	}
	
	public MapPanel getGameMap() {
		return gameMap;
	}
	
	public static void main(String[] args) { //Just testing
		MapPicture.setUp(1);
		GameGUI myGUI = new GameGUI(null, null);
		myGUI.addChatMessage("DragonSlayer sagt: Here there be test messages...");
		myGUI.addChatMessage("n00bkillor98 sagt: blablabla!!!1!11");
		myGUI.addChatMessage("Das_Krümelmonster sagt: blablalba bla blabla");
		myGUI.addChatMessage("Nyuu sagt: blabla... -_-");
	}
	
	private class MyKeyListener implements KeyListener {

		@Override
		public void keyPressed(KeyEvent e) {}

		@Override
		public void keyReleased(KeyEvent e) {
			if (running) {
				int keyCode = e.getKeyCode();
			    switch( keyCode ) { 
			        case KeyEvent.VK_UP:
			            client.makeTurn(Client.TURN_MOVEUP);
			            break;
			        case KeyEvent.VK_DOWN:
			        	client.makeTurn(Client.TURN_MOVEDOWN);
			            break;
			        case KeyEvent.VK_LEFT:
			        	client.makeTurn(Client.TURN_MOVELEFT);
			            break;
			        case KeyEvent.VK_RIGHT :
			        	client.makeTurn(Client.TURN_MOVERIGHT);
			            break;
			        case KeyEvent.VK_ESCAPE :
			        	client.makeTurn(Client.TURN_COMPLETETURN);
			            break;
			        case KeyEvent.VK_1 :
			        	client.makeTurn(Client.TURN_PLACETRAP_REVEAL);
			            break;
			        case KeyEvent.VK_2 :
			        	client.makeTurn(Client.TURN_PLACETRAP_BLIND);
			            break;
			        case KeyEvent.VK_3 :
			        	client.makeTurn(Client.TURN_PLACETRAP_SLOW);
			            break;
			        case KeyEvent.VK_4 :
			        	client.makeTurn(Client.TURN_PLACETRAP_STUN);
			            break;
			        case KeyEvent.VK_5 :
			        	client.makeTurn(Client.TURN_PLACETRAP_POISON);
			            break;
			        case KeyEvent.VK_6 :
			        	client.makeTurn(Client.TURN_PLACETRAP_PRONE);
			            break;
			        case KeyEvent.VK_7 :
			        	client.makeTurn(Client.TURN_PLACETRAP_DISPEL);
			            break;
			        case KeyEvent.VK_8 :
			        	client.makeTurn(Client.TURN_PLACETRAP_PURGE);
			            break;
			     }
			}
		}

		@Override
		public void keyTyped(KeyEvent e) {}
		
	}
	
	private class EndListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			client.makeTurn(Client.TURN_COMPLETETURN);
		}
	}
	
	private class MyMouseListener implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			mainFrame.requestFocusInWindow();
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {

			
		}
		
	}
	
	private class MyChatKeyListener implements KeyListener {

		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER){
				e.consume();
				if (chatInput.getText().length() > 0) {
					String text = chatInput.getText();
					if (!(text.startsWith("/t ") || text.startsWith("/g ") || text.startsWith("/w "))){
						text = "/g " + text;
					}
					client.sendMessage(text);
					addOwnMessage(text);
					chatInput.setText("");
					mainFrame.requestFocusInWindow();
				}
			}
		}

		@Override
		public void keyReleased(KeyEvent e) {}

		@Override
		public void keyTyped(KeyEvent e) {}
		
	}

	public void gameEnded(boolean won) {
		setTurnDisplay(false);
		running = false;
		if (won){
			gameMap.setState(MapPanel.GAMEWON);
		} else {
			gameMap.setState(MapPanel.GAMELOST);
		}
//		JPanel bigPanel = new JPanel();
//		mainFrame.removeAll();
//		bigPanel.setLayout(new BorderLayout());
//		bigPanel.add(gameMap, BorderLayout.CENTER);
//		JPanel chatPanel = new JPanel();
//		chatPanel.add(chatDisplay, BorderLayout.CENTER);
//		chatPanel.add(chatInput, BorderLayout.PAGE_END);
//		bigPanel.add(chatPanel, BorderLayout.LINE_END);
//		mainFrame.add(bigPanel);
//		mainFrame.pack();
		
		
		
	}

	private void setTurnDisplay(boolean yourTurn) {
		if (yourTurn) {
			turnDisplay.setText(TURN);
			turnDisplay.setBackground(Color.GREEN);
		} else {
			turnDisplay.setText(TURNNOT);
			turnDisplay.setBackground(Color.RED);
		}
	}

	@Override
	public void windowActivated(WindowEvent e) {
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		clientTest.leaveGame();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {}

	@Override
	public void windowDeiconified(WindowEvent e) {}

	@Override
	public void windowIconified(WindowEvent e) {}

	@Override
	public void windowOpened(WindowEvent e) {}

	public void close() {
		clientTest.leaveGame();
		mainFrame.dispose();
	}
}
