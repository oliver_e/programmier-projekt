package gui.game;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import network.Client;
import network.interfaces.ClientInterface;

@SuppressWarnings("serial")
public class MoveButtons extends JPanel{
	private BufferedImage image;
	private JButton moveUp;
	private JButton moveDown;
	private JButton moveLeft;
	private JButton moveRight;
	private ClientInterface client;
	
	/**Returns JPanel with buttons to move up, down, left and right.
	 * 
	 * @param client the client on whose avatar the move commands are to be issued
	 */
	public MoveButtons(ClientInterface client) {
		setPreferredSize(new Dimension(150, 150));
		this.client = client;
		setLayout(new GridLayout(3, 3));
		try {
		    image = ImageIO.read(ClassLoader.getSystemResource("files/MoveButtons.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		ImageIcon icon = new ImageIcon(image.getSubimage(0, 0, 40, 40));
		moveUp = new JButton(icon);
		icon = new ImageIcon(image.getSubimage(40, 0, 40, 40));
		moveDown = new JButton(icon);
		icon = new ImageIcon(image.getSubimage(80, 0, 40, 40));
		moveLeft = new JButton(icon);
		icon = new ImageIcon(image.getSubimage(120, 0, 40, 40));
		moveRight = new JButton(icon);
		add(new JLabel(""));
		add(moveUp);
		add(new JLabel(""));
		add(moveLeft);
		add(new JLabel(""));
		add(moveRight);
		add(new JLabel(""));
		add(moveDown);
		add(new JLabel(""));
		moveUp.addActionListener(new MoveListener());
		moveDown.addActionListener(new MoveListener());
		moveLeft.addActionListener(new MoveListener());
		moveRight.addActionListener(new MoveListener());
	}
	private class MoveListener implements ActionListener {
		/**Tells client to move in certain direction if corresponding key is pressed.
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == moveUp) {
				client.makeTurn(Client.TURN_MOVEUP);
			}
			else if (e.getSource() == moveDown) {
				client.makeTurn(Client.TURN_MOVEDOWN);
			}
			else if (e.getSource() == moveLeft) {
				client.makeTurn(Client.TURN_MOVELEFT);
			}
			else if (e.getSource() == moveRight) {
				client.makeTurn(Client.TURN_MOVERIGHT);
			}
		}
	}
}
