package gui.game;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.plaf.LayerUI;

import network.client.KnightPicture;
import network.client.MapPicture;
import network.interfaces.ClientInterface;
import spiellogik.HTGEvent;

@SuppressWarnings("serial")
public class MapPanel extends JPanel {
	
	private static BufferedImage tileset;
	private static BufferedImage[] tiles;
	private static BufferedImage redFront;
	private static BufferedImage redRight;
	private static BufferedImage redLeft;
	private static BufferedImage redBack;
	private static BufferedImage blueFront;
	private static BufferedImage blueRight;
	private static BufferedImage blueLeft;
	private static BufferedImage blueBack;
	private static BufferedImage grail;
	private static BufferedImage fog;
	private static BufferedImage dispelImg;
	private static BufferedImage hitImg;
	private static BufferedImage purgeImg;
	private static BufferedImage poisonImg;
	private static BufferedImage nukeImg;
	
	private BufferedImage terrainImg;
	private BufferedImage fogImg;
	private BufferedImage playerImg;
	
	private BufferedImage entireMap;
	
	private Vector<KnightPicture> seenKnights;
	
	private int offsetX; //Where the top left corner is located on the whole 128*128 map; may be negative
	private int offsetY;
	private boolean[][] visionField;
	private int grailX;
	private int grailY;
	private ClientInterface client;
	private Vector<Animation> animations = new Vector<Animation>();
	private BufferedImage[] attackArray = new BufferedImage[15];
	private BufferedImage[] poisonArray = new BufferedImage[15];
	private BufferedImage[] dispelArray = new BufferedImage[12];
	private BufferedImage[] purgeArray = new BufferedImage[15];
	private BufferedImage[] nukeArray = new BufferedImage[15];
	
	private LayerUI<JComponent> layerUI;
	
	private static int res = GameGUI.RESOLUTION;
	
	public static final int MOUNTAIN = 0;
	public static final int GRASS = 1;
	public static final int SAND = 2;
	public static final int MUD = 3;
	public static final int WATER = 4;
	public static final int STONEFLOOR = 5;
	public static final int BRIDGE = 6;
	public static final int WALL = 7;
	public static final int PAVEMENT = 8;
	public static final int PEDESTAL = 9;
	public static final int SPAWN1 = 10;
	public static final int SPAWN2 = 11;
	public static final int GOAL1 = 12;
	public static final int GOAL2 = 13;
	
	private int gameState = 0;
	private int xPos;
	private int yPos;
	public static final int GAMERUNNING = 0;
	public static final int GAMEWON = 1;
	public static final int GAMELOST = 2;
	public static final int GAMEPLAYERDEAD = 3;

	public MapPanel(ClientInterface client) {
		
		setDoubleBuffered(true);
		
		setPreferredSize(new Dimension(MapPicture.DISPLAYDIMENSIONS * res, MapPicture.DISPLAYDIMENSIONS * res));
		this.client = client;
		layerUI = new AnimationLayerUI();
		fogImg = new BufferedImage(res*MapPicture.DISPLAYDIMENSIONS, res*MapPicture.DISPLAYDIMENSIONS, BufferedImage.TYPE_INT_ARGB);
		terrainImg = new BufferedImage(res*MapPicture.DISPLAYDIMENSIONS, res*MapPicture.DISPLAYDIMENSIONS, BufferedImage.TYPE_INT_ARGB);
		playerImg = new BufferedImage(res*MapPicture.DISPLAYDIMENSIONS, res*MapPicture.DISPLAYDIMENSIONS, BufferedImage.TYPE_INT_ARGB);
		entireMap = new BufferedImage(res*MapPicture.DISPLAYDIMENSIONS, res*MapPicture.DISPLAYDIMENSIONS, BufferedImage.TYPE_INT_ARGB);
		
		
		try {
		    tileset = ImageIO.read(ClassLoader.getSystemClassLoader().getResourceAsStream("files/TileSet.png"));

		    redFront = ImageIO.read(ClassLoader.getSystemResource("files/knights/KnightFrontRed.png"));
		    redRight = ImageIO.read(ClassLoader.getSystemResource("files/knights/KnightRightRed.png"));
			redLeft = ImageIO.read(ClassLoader.getSystemResource("files/knights/KnightLeftRed.png"));
			redBack = ImageIO.read(ClassLoader.getSystemResource("files/knights/KnightBackRed.png"));
			blueFront = ImageIO.read(ClassLoader.getSystemResource("files/knights/KnightFrontBlue.png"));
			blueRight = ImageIO.read(ClassLoader.getSystemResource("files/knights/KnightRightBlue.png"));
			blueLeft = ImageIO.read(ClassLoader.getSystemResource("files/knights/KnightLeftBlue.png"));
			blueBack = ImageIO.read(ClassLoader.getSystemResource("files/knights/KnightBackBlue.png"));
			
			dispelImg = ImageIO.read(ClassLoader.getSystemResource("files/explosion_dispel.png"));
			purgeImg = ImageIO.read(ClassLoader.getSystemResource("files/explosion_purge_2.png"));
			nukeImg = ImageIO.read(ClassLoader.getSystemResource("files/explosion_nuke.png"));
			poisonImg = ImageIO.read(ClassLoader.getSystemResource("files/poison_effect.png"));
			hitImg = ImageIO.read(ClassLoader.getSystemResource("files/slash_effect.png"));
			for (int i = 0; i < attackArray.length; i++) {
				attackArray[i] = hitImg.getSubimage(i*60, 0, 60, 60);//just testing
			}
			for (int i = 0; i < poisonArray.length; i++) {
				poisonArray[i] = poisonImg.getSubimage(i*100, 0, 100, 100);
			}
			for (int i = 0; i < dispelArray.length; i++) {
				dispelArray[i] = dispelImg.getSubimage(i*50, 0, 50, 50);
			}
			for (int i = 0; i < purgeArray.length; i++) {
				purgeArray[i] = purgeImg.getSubimage(i*96, 0, 96, 96);
			}
			for (int i = 0; i < purgeArray.length; i++) {
				nukeArray[i] = nukeImg.getSubimage(i*300, 0, 300, 300);
			}
		    grail = ImageIO.read(ClassLoader.getSystemResource("files/Grail.png"));
		    fog = ImageIO.read(ClassLoader.getSystemResource("files/Fog.png"));
		} catch (IOException e) {
			System.err.println("An Image file was not found");
			if (tileset == null) tileset = new BufferedImage(res, res, 0);
			if (redFront == null) redFront = new BufferedImage(res, res, 0);
			if (redRight == null) redRight = new BufferedImage(res, res, 0);
			if (redLeft == null) redLeft = new BufferedImage(res, res, 0);
			if (redBack == null) redBack = new BufferedImage(res, res, 0);
			if (blueFront == null) blueFront = new BufferedImage(res, res, 0);
			if (blueRight == null) blueRight = new BufferedImage(res, res, 0);
			if (blueLeft == null) blueLeft = new BufferedImage(res, res, 0);
			if (blueBack == null) blueBack = new BufferedImage(res, res, 0);
			if (grail == null) grail = new BufferedImage(res, res, 0);
			if (fog == null) fog = new BufferedImage(res, res, 0);
			
		}
		
		int diffTilesX = tileset.getWidth() / res;
		int diffTilesY = tileset.getHeight() / res;
		
		tiles = new BufferedImage[diffTilesX*diffTilesY];
		for (int i = 0; i < diffTilesX*diffTilesY; i++) { 
			tiles[i] = tileset.getSubimage((i%diffTilesX) * res, (i/diffTilesX) * res, res, res);
		}
		
		seenKnights = new Vector<KnightPicture>();
		visionField = new boolean[MapPicture.MAPSIZEX][MapPicture.MAPSIZEY];
	}
	
	private class AnimationThread extends Thread {
		private Graphics g;
		private MapPanel panel;
		private int iterations;
		public AnimationThread(Graphics g, MapPanel panel, int iterations) {
			this.panel = panel;
			this.g = g;
			this.iterations = iterations;
		}
		public void run() {
			for (int i = 0; i < iterations; i++) {
				layerUI.paint(g, panel);
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {}
			}
		}
	}
	
	public void paintAnimations(Graphics g, int iterations) {
		AnimationThread animThread = new AnimationThread(g, this, iterations);
		animThread.start();
	}
	
	@Override
	public void paint(Graphics g) {
		if (gameState == GAMERUNNING){
			g.drawImage(entireMap, 0, 0, null);
			
//			g.drawImage(terrainImg, 0, 0, null);
//			g.drawImage(fogImg, 0, 0, null);
//			drawTerrain(g);
//			drawFog(g);
//			createPlayers(g);
//			drawGrail(g);
		} else  if (gameState == GAMEWON){
			try {
				BufferedImage screen = ImageIO.read(ClassLoader.getSystemResource("files/Win.png"));
				g.drawImage(screen, 0, 0, null);
			} catch (IOException e) {e.printStackTrace();}
		} else  if (gameState == GAMELOST){
			try {
				BufferedImage screen = ImageIO.read(ClassLoader.getSystemResource("files/Lose.png"));
				g.drawImage(screen, 0, 0, null);
			} catch (IOException e) {e.printStackTrace();}
		} else  if (gameState == GAMEPLAYERDEAD){
			try {
				BufferedImage screen = ImageIO.read(ClassLoader.getSystemResource("files/Dead.png"));
				g.drawImage(screen, 0, 0, null);
			} catch (IOException e) {e.printStackTrace();}
		}
		
		
	}
	
	
	private void createTerrain() {
		Graphics2D g2 = terrainImg.createGraphics();
		g2.setBackground(new Color(0, 0, 0, 0));
		g2.clearRect(0, 0, terrainImg.getWidth(), terrainImg.getHeight());
		for (int localY = 0; localY < MapPicture.DISPLAYDIMENSIONS; localY++) {
			for (int localX = 0; localX < MapPicture.DISPLAYDIMENSIONS; localX++) {
				int globalX = localX+offsetX;
				int globalY = localY+offsetY;
				
				//localX: x-position on canvas; globalX: x-position on entire map.
				
				if ( ( (globalX < 0) || (globalX >= MapPicture.MAPSIZEX) )
						|| ( (globalY < 0) || (globalY >= MapPicture.MAPSIZEY) ) ) {
					//out of bounds; not a problem but a trivial case. Just draw mountains.
					drawTile(localX, localY, MOUNTAIN, g2);
				} else {
					drawTile(localX, localY, terrainTypeToTileID(MapPicture.getType(globalX, globalY)), g2);
				}
			}
		}
		g2.dispose();
	}

	private void createPlayers() {
		Graphics2D g2 = playerImg.createGraphics();
		g2.setBackground(new Color(0, 0, 0, 0));
		g2.clearRect(0, 0, playerImg.getWidth(), playerImg.getHeight());
		for (KnightPicture knight : seenKnights) {
			if (knight.getxCoord() > -1 && knight.getyCoord() > -1 && knight.getxCoord()-offsetX >= 0 && knight.getxCoord()-offsetX < MapPicture.DISPLAYDIMENSIONS 
					&& knight.getyCoord()-offsetY >= 0 && knight.getyCoord()-offsetY < MapPicture.DISPLAYDIMENSIONS) {
				//if the knight is in displayable area
				if (client.getTeam() == 1) {
					if (knight.isAllied()) {
						switch (knight.getDirection()) {
						case 0:
							g2.drawImage(blueBack, (knight.getxCoord() - offsetX) * res,
									(knight.getyCoord() - offsetY) * res, null);
							break;
						case 1:
							g2.drawImage(blueRight, (knight.getxCoord() - offsetX) * res,
									(knight.getyCoord() - offsetY) * res, null);
							break;
						case 2:
							g2.drawImage(blueFront, (knight.getxCoord() - offsetX) * res,
									(knight.getyCoord() - offsetY) * res, null);
							break;
						case 3:
							g2.drawImage(blueLeft, (knight.getxCoord() - offsetX) * res,
									(knight.getyCoord() - offsetY) * res, null);
							break;
						}
					} else {
						switch (knight.getDirection()) {
						case 0:
							g2.drawImage(redBack, (knight.getxCoord() - offsetX) * res,
									(knight.getyCoord() - offsetY) * res, null);
							break;
						case 1:
							g2.drawImage(redRight, (knight.getxCoord() - offsetX) * res,
									(knight.getyCoord() - offsetY) * res, null);
							break;
						case 2:
							g2.drawImage(redFront, (knight.getxCoord() - offsetX) * res,
									(knight.getyCoord() - offsetY) * res, null);
							break;
						case 3:
							g2.drawImage(redLeft, (knight.getxCoord() - offsetX) * res,
									(knight.getyCoord() - offsetY) * res, null);
							break;
						}
					}
				}
				else {
					if (!knight.isAllied()) {
						switch (knight.getDirection()) {
						case 0:
							g2.drawImage(blueBack, (knight.getxCoord() - offsetX) * res,
									(knight.getyCoord() - offsetY) * res, null);
							break;
						case 1:
							g2.drawImage(blueRight, (knight.getxCoord() - offsetX) * res,
									(knight.getyCoord() - offsetY) * res, null);
							break;
						case 2:
							g2.drawImage(blueFront, (knight.getxCoord() - offsetX) * res,
									(knight.getyCoord() - offsetY) * res, null);
							break;
						case 3:
							g2.drawImage(blueLeft, (knight.getxCoord() - offsetX) * res,
									(knight.getyCoord() - offsetY) * res, null);
							break;
						}
					} else {
						switch (knight.getDirection()) {
						case 0:
							g2.drawImage(redBack, (knight.getxCoord() - offsetX) * res,
									(knight.getyCoord() - offsetY) * res, null);
							break;
						case 1:
							g2.drawImage(redRight, (knight.getxCoord() - offsetX) * res,
									(knight.getyCoord() - offsetY) * res, null);
							break;
						case 2:
							g2.drawImage(redFront, (knight.getxCoord() - offsetX) * res,
									(knight.getyCoord() - offsetY) * res, null);
							break;
						case 3:
							g2.drawImage(redLeft, (knight.getxCoord() - offsetX) * res,
									(knight.getyCoord() - offsetY) * res, null);
							break;
						}
					}
				}
			}
		}
		g2.dispose();
	}

	private void createFog() {
		Graphics2D g2 = fogImg.createGraphics();
		g2.setBackground(new Color(0, 0, 0, 0));
		g2.clearRect(0, 0, fogImg.getWidth(), fogImg.getHeight());
		for (int y = 0; y < MapPicture.DISPLAYDIMENSIONS; y++) {
			for (int x = 0; x < MapPicture.DISPLAYDIMENSIONS; x++) {
				if (!visionField[x][y]) {
					g2.drawImage(fog, x*res, y*res, null);
				}
			}
		}
		g2.dispose();
		
	}

	private void drawGrail(Graphics g) {
		if (grailX > -1 && grailY > -1 && grailX-offsetX >= 0 && grailX-offsetX < MapPicture.DISPLAYDIMENSIONS 
				&& grailY-offsetY >= 0 && grailY-offsetY < MapPicture.DISPLAYDIMENSIONS) {
			//if the grail is in displayable area
			g.drawImage(grail, (grailX-offsetX)*res, (grailY-offsetY)*res, null);	
		}
	}

	private boolean drawTile(int x, int y, int tileID, Graphics g){
		if ( ( (x < 0 || x >= MapPicture.DISPLAYDIMENSIONS) || ( (y < 0 || y >= MapPicture.DISPLAYDIMENSIONS) ) ) 
				|| ( (tileID < 0) || (tileID >= tiles.length) ) ) {
			// out of bounds
			g.drawRect(x*res, y*res, res, res);
			return false;
		} else {
			g.drawImage(tiles[tileID], x*res, y*res, null);
		}
		return true;
		
	}
	
	
	/**
	 * Updates all valid information.
	 * @param grailY 
	 * @param grailX 
	 * @param visionField 
	 */
	public void update(int posX, int posY, boolean[][] visionField, Vector<KnightPicture> seenKnights, int grailX, int grailY){
		offsetX = posX - (MapPicture.DISPLAYDIMENSIONS/2);
		offsetY = posY - (MapPicture.DISPLAYDIMENSIONS/2);
		this.seenKnights = seenKnights;
		this.visionField = visionField;
		this.grailX = grailX;
		this.grailY = grailY;
		xPos = posX;
		yPos = posY;
		
		if (posX < 0 || posY < 0) {
			gameState = GAMEPLAYERDEAD;
		} else {
			if (gameState == GAMEPLAYERDEAD && posX >= 0 && posY >= 0) {
				gameState = GAMERUNNING;
			}
		}
		createTerrain();
		createFog();
		createPlayers();
		Graphics2D g2 = entireMap.createGraphics();
		g2.setBackground(new Color(0, 0, 0, 0));
		g2.clearRect(0, 0, entireMap.getWidth(), entireMap.getHeight());
		g2.drawImage(terrainImg, 0, 0, null);
		g2.drawImage(fogImg, 0, 0, null);
		g2.drawImage(playerImg, 0, 0, null);
		drawGrail(g2);
		
		repaint();
		g2.dispose();
	}
	
	public void update(String event, int x, int y) {
//		System.out.println(event + " at " + x + ", " + y);
		int localX = x - offsetX;
		int localY = y - offsetY;
//		System.out.println("local X = " + localX + ", local Y = " + localY);
//		if (localX < 0 || localX >= MapPicture.DISPLAYDIMENSIONS || localY < 0 || localY >= MapPicture.DISPLAYDIMENSIONS) {
//			return;
//		}
		switch (event) {
		case HTGEvent.DAMAGE_HIT:
			Animation hitAnim = new Animation(attackArray, localX*res -14, localY*res -14);
			animations.add(hitAnim);
			paintAnimations(this.getGraphics(), hitAnim.getMaxFrames());
			break;
			
		case HTGEvent.DAMAGE_DISPEL:
			Animation dispAnim = new Animation(dispelArray, localX*res -9, localY*res -9);
			animations.add(dispAnim);
			paintAnimations(this.getGraphics(), dispAnim.getMaxFrames());
			break;
			
		case HTGEvent.DAMAGE_POISON:
			Animation poisonAnim = new Animation(poisonArray, localX*res -32, localY*res -32);
			animations.add(poisonAnim);
			paintAnimations(this.getGraphics(), poisonAnim.getMaxFrames());
			break;
			
		case HTGEvent.DAMAGE_PURGE:
			Animation purgeAnim = new Animation(purgeArray, localX* res - 32, localY * res - 52);
			animations.add(purgeAnim);
			paintAnimations(this.getGraphics(), purgeAnim.getMaxFrames());
			break;
			
		case HTGEvent.DAMAGE_PURGE_SELF:
			Animation nukeAnim = new Animation(nukeArray, (MapPicture.DISPLAYDIMENSIONS/2)* res - 132, (MapPicture.DISPLAYDIMENSIONS/2) * res - 220);
			animations.add(nukeAnim);
			paintAnimations(this.getGraphics(), nukeAnim.getMaxFrames());
			break;
			
		}
	}
	
	public static int terrainTypeToTileID(int type) { //converts a terrain type used by Map to a tileID used to display;
		//HARD CODED FOR NOW, WILL CHANGE LATER
		switch (type) {
		case 1:
			return GRASS;
			
		case 3:
			return STONEFLOOR;
			
		case 5:
			return PAVEMENT;
			
		case 6:
			return BRIDGE;
			
		case 8:
			return SAND;
			
		case 9:
			return MUD;
			
		case 69:
			return MOUNTAIN;
			
		case 72:
			return WALL;
			
		case 65:
			return WATER;
			
		case 59:
			return PEDESTAL;
			
		case 60:
			return SPAWN1;
			
		case 61:
			return SPAWN2;
			
		case 62:
			return GOAL1;
			
		case 63:
			return GOAL2;

		}
		return 0;
		
	}
	
	public void setState(int newState){
		gameState = newState;
		repaint();
	}
	
	public LayerUI<JComponent> getLayerUI() {
		return layerUI;
	}
	
	private class AnimationLayerUI extends LayerUI<JComponent> {
		public AnimationLayerUI() {
			setDoubleBuffered(true);
		}
		public void paint(Graphics g, JComponent c) {
			super.paint(g, c);

		    Graphics2D g2 = (Graphics2D) g.create();

		    g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
		    
		    Object[] animArray = animations.toArray();
		    for (int i = 0; i < animArray.length; i++) {
		    	Animation tempAnim = (Animation) animArray[i];
		    	if (tempAnim.getCounter() >= tempAnim.getMaxFrames()) {
		    		animations.remove(tempAnim);
		    	}
		    	else {
		    		g2.drawImage(tempAnim.getNextFrame(), tempAnim.getxpos(), tempAnim.getypos(), null);
		    	}
		    }
		    g2.dispose();
		}
	}
	
	private class Animation {
		private BufferedImage[] images;
		private int counter;
		private int maxFrames;
		private int xPos;
		private int yPos;
		
		public int getMaxFrames() {
			return maxFrames;
		}

		public Animation(BufferedImage[] images, int x, int y) {
			this.images = images;
			counter = 0;
			maxFrames = images.length;
			xPos = x;
			yPos = y;
		}
		
		public int getCounter() {
			return counter;
		}
		
		public int getxpos() {
			return xPos;
		}
		
		public int getypos() {
			return yPos;
		}
		
		public BufferedImage getNextFrame() {
			if (counter == maxFrames -1) {
				return images[counter];
			}
			else {
				return images[counter++];
			}
			
		}
	}
	
}
