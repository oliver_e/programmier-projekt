package gui.game;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import network.Client;
import network.interfaces.ClientInterface;
import spiellogik.TrapInfo;

@SuppressWarnings("serial")
public class TrapPlacer extends JPanel{
	
	private BufferedImage image;
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private JButton button6;
	private JButton button7;
	private JButton button8;
	private ClientInterface client;
	private static int IMGSIZE = 61;
	
	/**Returns JPanel with seven buttons with tooltips that allow the user to place traps at their position. 
	 * 
	 * @param client The client associated with the calling GameGUI, used to issue commands to the player's avatar.
	 */
	
	public TrapPlacer(ClientInterface client) {
		this.client = client;
		setPreferredSize(new Dimension(150, 300));
		setLayout(new GridLayout(4, 2));
		try {
		    image = ImageIO.read(ClassLoader.getSystemResource("files/TrapPlacer.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		ImageIcon icon = new ImageIcon(image.getSubimage(0, 0, IMGSIZE, IMGSIZE));
		button1 = new JButton(icon);
		button1.setToolTipText("<html>" + "Reveals affected player for 5 turns" + "<br>" + "Cost: " + TrapInfo.REVEAL_COST + " stamina" + "</html>");
		button1.setFocusable(false);
		
		icon = new ImageIcon(image.getSubimage(IMGSIZE, 0, IMGSIZE, IMGSIZE));
		button2 = new JButton(icon);
		button2.setToolTipText("<html>Blinds affected player for 3 turns<br> Cost: " + TrapInfo.BLIND_COST + " stamina </html>");
		button2.setFocusable(false);
		
		icon = new ImageIcon(image.getSubimage(2*IMGSIZE, 0, IMGSIZE, IMGSIZE));
		button3 = new JButton(icon);
		button3.setToolTipText("<html>Slows affected player for 3 turns<br> Cost: " + TrapInfo.SLOW_COST + " stamina </html>");
		button3.setFocusable(false);
		
		icon = new ImageIcon(image.getSubimage(3*IMGSIZE, 0, IMGSIZE, IMGSIZE));
		button4 = new JButton(icon);
		button4.setToolTipText("<html>Stuns affected player for 1 turn<br> Cost: " + TrapInfo.STUN_COST + " stamina </html>");
		button4.setFocusable(false);
		
		icon = new ImageIcon(image.getSubimage(4*IMGSIZE, 0, IMGSIZE, IMGSIZE));
		button5 = new JButton(icon);
		button5.setToolTipText("<html>Deals 10 damage to affected player every turn for 5 turns<br> Cost: " + TrapInfo.POISON_COST + " stamina </html>");
		button5.setFocusable(false);
		
		icon = new ImageIcon(image.getSubimage(7*IMGSIZE, 0, IMGSIZE, IMGSIZE));
		button6 = new JButton(icon);
		button6.setToolTipText("<html>Makes the target receive doubled damage for 3 turns<br> Cost: " + TrapInfo.PRONE_COST + " stamina </html>");
		button6.setFocusable(false);
		
		icon = new ImageIcon(image.getSubimage(5*IMGSIZE, 0, IMGSIZE, IMGSIZE));
		button7 = new JButton(icon);
		button7.setToolTipText("<html>Deals 40 damage to affected player<br> Cost: " + TrapInfo.DISPEL_COST + " stamina </html>");
		button7.setFocusable(false);
		
		icon = new ImageIcon(image.getSubimage(6*IMGSIZE, 0, IMGSIZE, IMGSIZE));
		button8 = new JButton(icon);
		button8.setToolTipText("<html>Instantly kills affected player<br> Cost: " + TrapInfo.PURGE_COST + " stamina </html>");
		button8.setFocusable(false);
		
		add(button1);
		add(button2);
		add(button3);
		add(button4);
		add(button5);
		add(button6);
		add(button7);
		add(button8);
		button1.addActionListener(new TrapListener());
		button2.addActionListener(new TrapListener());
		button3.addActionListener(new TrapListener());
		button4.addActionListener(new TrapListener());
		button5.addActionListener(new TrapListener());
		button6.addActionListener(new TrapListener());
		button7.addActionListener(new TrapListener());
		button8.addActionListener(new TrapListener());
	}
	
	
	private class TrapListener implements ActionListener {
		/**ActionListener for buttons of TrapPlacer, issues place-commands to client
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == button1) {
				client.makeTurn(Client.TURN_PLACETRAP_REVEAL);
			}
			else if(e.getSource() == button2) {
				client.makeTurn(Client.TURN_PLACETRAP_BLIND);
			}
			else if(e.getSource() == button3) {
				client.makeTurn(Client.TURN_PLACETRAP_SLOW);
			}
			else if(e.getSource() == button4) {
				client.makeTurn(Client.TURN_PLACETRAP_STUN);
			}
			else if(e.getSource() == button5) {
				client.makeTurn(Client.TURN_PLACETRAP_POISON);
			}
			else if(e.getSource() == button6) {
				client.makeTurn(Client.TURN_PLACETRAP_PRONE);
			}
			else if(e.getSource() == button7) {
				client.makeTurn(Client.TURN_PLACETRAP_DISPEL);
			}
			else if(e.getSource() == button8) {
				client.makeTurn(Client.TURN_PLACETRAP_PURGE);
			}
		}
	}
}
