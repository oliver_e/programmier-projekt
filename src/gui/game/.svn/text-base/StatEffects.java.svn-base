package gui.game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import network.client.StatusEffectPicture;

@SuppressWarnings("serial")
public class StatEffects extends JPanel {
	
	private static int EFFECTSIZE = 50;
	private BufferedImage image;
	private boolean poisoned = false;
	private boolean revealed = false;
	private boolean blinded = false;
	private boolean slowed = false;
	private boolean stunned = false;
	private boolean prone = false;
	private StringBuilder bob;
	
	/**Returns a JPanel containing a picture for each status effect a knight can be suffering from. Applied effects are brighter
	 * than others. Tooltips additionally display the type and duration of each of these effects.
	 * 
	 */
	public StatEffects() {
		setPreferredSize(new Dimension(3*EFFECTSIZE, 2*EFFECTSIZE));
		try {
		    image = ImageIO.read(ClassLoader.getSystemResource("files/StatEffects.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		new Vector<StatusEffectPicture>();
	}
	
	/** 
	 * Removes all status effects from this particular GUI element. Does NOT remove effects from the knight, just doesn't display
	 * them anymore.
	 */
	private void clearEffects() {
		poisoned = false;
		revealed = false;
		blinded = false;
		slowed = false;
		stunned = false;
		prone = false;
	}
	/**Sets status effects of this GUI Element true of parameter contains at least one element of the corresponding type.
	 * Also creates tooltip containing type and duration of each effect.
	 * @param statEffects the status effect from which the knight associated with this GUI element is suffering.
	 */
	
	public void update(Vector<StatusEffectPicture> statEffects) {
		bob = new StringBuilder();
		bob.append("<html>");
		clearEffects();
		for (StatusEffectPicture effect : statEffects) {
			switch (effect.getType()) {
			case StatusEffectPicture.POISONED:
				poisoned = true;
				bob.append("Poisoned for " + effect.getDuration() + " Turns" + "<br>");
				break;
				
			case StatusEffectPicture.REVEALED:
				revealed = true;
				bob.append("Revealed for " + effect.getDuration() + " Turns" + "<br>");
				break;
				
			case StatusEffectPicture.BLINDED:
				blinded = true;
				bob.append("Blinded for " + effect.getDuration() + " Turns" + "<br>");
				break;
				
			case StatusEffectPicture.SLOWED:
				slowed = true;
				bob.append("Slowed for " + effect.getDuration() + " Turns" + "<br>");
				break;
				
			case StatusEffectPicture.STUNNED:
				stunned = true;
				bob.append("Stunned for " + effect.getDuration() + " Turns" + "<br>");
				break;
				
			case StatusEffectPicture.PRONE:
				prone = true;
				bob.append("Prone for " + effect.getDuration() + " Turns" + "<br>");
				break;
			}
		}
		if (statEffects == null || statEffects.isEmpty()) {
			bob.append("You are currently not suffering from any debuffs");
		}
		bob.append("</html");
		setToolTipText(bob.toString());
	}
	
	/**
	 * draws all status effects and draws a half-transparent rectangle over each effect from which the knight is not suffering.
	 */
	public void paint(Graphics g) {
		g.drawImage(image, 0, 0, null);
		g.setColor(new Color(0, 0, 0, 176));
		if (!revealed) {
			g.fillRect(0, 0, EFFECTSIZE, EFFECTSIZE);
		}
		if (!blinded) {
			g.fillRect(0, EFFECTSIZE, EFFECTSIZE, EFFECTSIZE);
		}
		if (!slowed) {
			g.fillRect(EFFECTSIZE, 0, EFFECTSIZE, EFFECTSIZE);
		}
		if (!stunned) {
			g.fillRect(EFFECTSIZE, EFFECTSIZE, EFFECTSIZE, EFFECTSIZE);
		}
		if (!poisoned) {
			g.fillRect(2*EFFECTSIZE, 0, EFFECTSIZE, EFFECTSIZE);
		}
		if (!prone) {
			g.fillRect(2*EFFECTSIZE, EFFECTSIZE, EFFECTSIZE, EFFECTSIZE);
		}
	}
}
