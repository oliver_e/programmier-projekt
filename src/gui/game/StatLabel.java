package gui.game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class StatLabel extends JPanel{
	private JLabel healthText;
	private JLabel staminaText;
	private StatBar healthBar;
	private StatBar staminaBar;
	
	
	private class StatBar extends JPanel {
		private int width; 
		private int height;
		private Color color;
		/**
		 * Returns colored bar with specified width & height
		 * @param width width of the bar
		 * @param height height of the bar
		 * @param color color of the bar
		 */
		public StatBar(int width, int height, Color color) {
			setPreferredSize(new Dimension(100, 15));
			this.width = width;
			this.height = height;
			this.color = color;
		}
		/**
		 * paints the bar again with its specifications
		 */
		public void paint(Graphics g) {
			g.setColor(color);
			g.fillRect(0, 0, width, height);
		}
		/**
		 * used to change specifications of the bar
		 * @param width new width of the bar
		 * @param height new height of the bar
		 * @param color new color of the bar
		 */
		public void update(int width, int height, Color color) {
			this.width = width;
			this.height = height;
			this.color = color;
		}
	}
	
	/**Returns a JPanel with both a health and a stamina bar whose lengths depend on the specified parameters. Also contains
	 * a JLabel for each parameter that displays the value as a number.
	 * @param health initial health
	 * @param stamina initial stamina
	 */
	public StatLabel(int health, int stamina) {
		setLayout(new FlowLayout());
		setPreferredSize(new Dimension(200, 70));
		healthText = new JLabel();
		staminaText = new JLabel();
		healthText.setPreferredSize(new Dimension(75, 20));
		staminaText.setPreferredSize(new Dimension(75, 20));
		healthText.setText("Health: " + health);
		staminaText.setText("Stamina: " + stamina);
		healthBar = new StatBar(health, 20, Color.RED);
		staminaBar = new StatBar(stamina*10, 20, Color.GREEN);
		add(healthText);
		add(healthBar);
		add(staminaText);
		add(staminaBar);
	}
	/**Used to change the health and stamina values of the StatLabel, updates both StatBars and JLabels.
	 * 
	 * @param health new amount of health 
	 * @param stamina new amount of stamina
	 */
	public void update(int health, int stamina) {
		healthText.setText("Health: " + health);
		staminaText.setText("Stamina: " + stamina);
		healthBar.update(health, 20, Color.RED);
		staminaBar.update(stamina*10, 20, Color.GREEN);
	}
}
