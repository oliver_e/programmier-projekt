package gui.game;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.sound.sampled.LineEvent.Type;

import spiellogik.HTGEvent;

public class SoundPlayer extends Thread{

	public static final String TRAP_PLACE = "trap_place";
	public static final int TRAP_PLACE_VARI = 1;
	public static final String TRAP_TRIGGER = "trap_trigger";
	public static final int TRAP_TRIGGER_VARI = 1;
	public static final String DAMAGE_HIT = "hit";
	public static final int DAMAGE_HIT_VARI = 4;
	public static final String DAMAGE_POISON = "poison";
	public static final int DAMAGE_POISON_VARI = 1;
	public static final String DAMAGE_DISPEL = "dispel";
	public static final int DAMAGE_DISPEL_VARI = 1;
	public static final String DAMAGE_PURGE = "purge";
	public static final int DAMAGE_PURGE_VARI = 1;
	public static final String DAMAGE_PURGE_SELF = "purge";
	public static final int DAMAGE_PURGE_SELF_VARI = 1;
	
	private String fileName;
	
	/**
	 * Plays a sound when instantiated.<br>
	 * Give it one of it's constants as an argument<br>
	 * (e.g. new SoundPlayer(SoundPlayer.TRAP_PLACE))
	 * @param event
	 */
	@SuppressWarnings("unused")
	public SoundPlayer(String event) {
		switch (event) {
		case HTGEvent.TRAP_PLACE:
			if (TRAP_PLACE_VARI > 1) {
				int rand = new Random().nextInt(TRAP_PLACE_VARI)+1;
				fileName = TRAP_PLACE + "_" + rand;
			} else {
				fileName = TRAP_PLACE;
			}
			break;
			
		case HTGEvent.TRAP_TRIGGER:
			if (TRAP_TRIGGER_VARI > 1) {
				int rand = new Random().nextInt(TRAP_TRIGGER_VARI)+1;
				fileName = TRAP_TRIGGER + "_" + rand;
			} else {
				fileName = TRAP_TRIGGER;
			}
			break;
			
		case HTGEvent.DAMAGE_HIT:
			if (DAMAGE_HIT_VARI > 1) {
				int rand = new Random().nextInt(DAMAGE_HIT_VARI)+1;
				fileName = DAMAGE_HIT + "_" + rand;
			} else {
				fileName = DAMAGE_HIT;
			}
			break;
			
		case HTGEvent.DAMAGE_POISON:
			if (DAMAGE_POISON_VARI > 1) {
				int rand = new Random().nextInt(DAMAGE_POISON_VARI)+1;
				fileName = DAMAGE_POISON + "_" + rand;
			} else {
				fileName = DAMAGE_POISON;
			}
			break;
			
		case HTGEvent.DAMAGE_DISPEL:
			if (DAMAGE_DISPEL_VARI > 1) {
				int rand = new Random().nextInt(DAMAGE_DISPEL_VARI)+1;
				fileName = DAMAGE_DISPEL + "_" + rand;
			} else {
				fileName = DAMAGE_DISPEL;
			}
			break;
			
		case HTGEvent.DAMAGE_PURGE:
			if (DAMAGE_PURGE_VARI > 1) {
				int rand = new Random().nextInt(DAMAGE_PURGE_VARI)+1;
				fileName = DAMAGE_PURGE + "_" + rand;
			} else {
				fileName = DAMAGE_PURGE;
			}
			break;
			
		case HTGEvent.DAMAGE_PURGE_SELF:
			if (DAMAGE_PURGE_VARI > 1) {
				int rand = new Random().nextInt(DAMAGE_PURGE_SELF_VARI)+1;
				fileName = DAMAGE_PURGE_SELF + "_" + rand;
			} else {
				fileName = DAMAGE_PURGE_SELF;
			}
			break;

		default:
			return;
		}
		start();
	}
	
	@Override
	public void run() {
		playSound(fileName);
	}
	
	private void playSound(String filename){
		class AudioListener implements LineListener {
			private boolean done = false;

			@Override
			public synchronized void update(LineEvent event) {
				Type eventType = event.getType();
				if (eventType == Type.STOP || eventType == Type.CLOSE) {
					done = true;
					notifyAll();
				}
			}

			public synchronized void waitUntilDone()
					throws InterruptedException {
				while (!done) {
					wait();
				}
			}
		}

		try {
			AudioListener listener = new AudioListener();

			InputStream is = ClassLoader.getSystemClassLoader()
					.getResourceAsStream("files/sounds/" + filename + ".wav");
			AudioInputStream audioInputStream = null;
			try {
				BufferedInputStream bs = new BufferedInputStream(is);
				audioInputStream = AudioSystem.getAudioInputStream(bs);

			} catch (UnsupportedAudioFileException | IOException e) {
				return;
			}

			try {
				Clip clip = AudioSystem.getClip();
				clip.addLineListener(listener);
				clip.open(audioInputStream);
				try {
					clip.start();
					listener.waitUntilDone();
				} finally {
					clip.close();
				}
			} finally {
				audioInputStream.close();
			}
		} catch (IOException | LineUnavailableException | InterruptedException e) {
			return;
		}
	}
	public static void main(String[] args) {
		new SoundPlayer(HTGEvent.DAMAGE_HIT);
	}
	
}
