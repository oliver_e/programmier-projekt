package gui.server;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.image.ColorModel;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import network.interfaces.ServerInterface;


@SuppressWarnings("serial")
public class OldGamesFrame extends JFrame {
	
	private JScrollPane mainScrollPanel;
	private JTable oldGamesJTable;
	private ServerInterface serverInterface;
	private DefaultTableModel tableModel;
	private final String[] columnIdentifiers = {"Gamename", "Team1", "Team2", "Team won"};
	private int lastAddedMaxSize;
	private int maxColumnSizeTeam1;
	private int maxColumnSizeTeam2;
	private int maxGameNameSize;
	
	public OldGamesFrame(ServerInterface serverInterface){
		this.serverInterface = serverInterface;
		JPanel panel = new JPanel();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		oldGamesJTable = new JTable();
		oldGamesJTable.setEnabled(false);
		oldGamesJTable.setFillsViewportHeight(false);
		tableModel = (DefaultTableModel) oldGamesJTable.getModel();
		tableModel.setColumnIdentifiers(columnIdentifiers);
		mainScrollPanel = new JScrollPane(oldGamesJTable);
		panel.setLayout(new GridLayout(1, 1));
		panel.add(mainScrollPanel);
		add(panel, BorderLayout.CENTER);
		setTitle("Old games");
		getOldGames();
		pack();
	}
	
	/**
	 * reads Array list of old Games and adds them to oldGamesList.
	 */
	public void getOldGames(){
		String[] lines = serverInterface.readOldGames();
		if (lines != null) {
			for (String line : lines) {
				String gamename = line.split(";")[0];
				String team1 = line.split(";")[1];
				String team2 = line.split(";")[2];
				String teamThatWon = line.split(";")[3];
				String[] row = {gamename, team1, team2, teamThatWon};
				tableModel.addRow(row);
				int team1Count = getFontMetrics(UIManager.getDefaults().getFont("TabbedPane.font")).stringWidth(team1);
				int team2Count = getFontMetrics(UIManager.getDefaults().getFont("TabbedPane.font")).stringWidth(team2);
				TableColumnModel columnModel = oldGamesJTable.getColumnModel();
				if (team1Count > maxColumnSizeTeam1) {
					maxColumnSizeTeam1 = team1Count;
					TableColumn column = columnModel.getColumn(1);
					column.setMinWidth(maxColumnSizeTeam1+5);
				}
				if (team2Count > maxColumnSizeTeam2) {
					maxColumnSizeTeam2 = team2Count;
					TableColumn column = columnModel.getColumn(2);
					column.setMinWidth(maxColumnSizeTeam2+5);
				}
				int gameNameSize = getFontMetrics(UIManager.getDefaults().getFont("TabbedPane.font")).stringWidth(gamename);
				if (gameNameSize > maxGameNameSize) {
					maxGameNameSize = gameNameSize;
					TableColumn column = columnModel.getColumn(0);
					column.setMinWidth(maxGameNameSize+5);
				}
			}
		}
		tableModel.fireTableDataChanged();
		Dimension d = oldGamesJTable.getPreferredSize();
		mainScrollPanel.setPreferredSize(
		    new Dimension(d.width,oldGamesJTable.getRowHeight()*tableModel.getRowCount()+1));
	}
	
}
