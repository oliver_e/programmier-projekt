package gui.server;

import java.util.ArrayList;

import javax.swing.*;

import network.server.Highscore;
import network.server.Highscore.Entry;

public class HighscoreFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JScrollPane mainScrollPanel;
	private DefaultListModel<String> highscoreList;
	private JList<String> highscoreJList;
	private ArrayList<Entry> HighscoreArray;
	
	public HighscoreFrame(){
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		highscoreList = new DefaultListModel<String>();
		highscoreJList = new JList<String>(highscoreList);
		mainScrollPanel = new JScrollPane(highscoreJList);
		add(mainScrollPanel);
		setTitle("Highscore");
		Highscore highscore = Highscore.load();
		HighscoreArray = highscore.getEntries();
		displayHighscore();
		pack();
	}
	
	/**
	 * Reads Highscore from external file and adds elements to highscoreList.
	 */
	public void displayHighscore(){
		for (Highscore.Entry entry : HighscoreArray) {
			addHighscoreEntry(entry.name + ":" + entry.wins);
		}
	}
	
	/**
	 * Adds name to DefaultListModel
	 * @param name
	 */
	public void addHighscoreEntry(String name){
		highscoreList.addElement(name);
		highscoreJList.updateUI();
	}
	
}
	

