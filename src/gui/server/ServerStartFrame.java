package gui.server;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import network.interfaces.ServerInterface;


public class ServerStartFrame extends JFrame implements ActionListener, MouseListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton sendMessage;
	private JTextField serverMessage;
	private JPanel messagePanel;
	private JPanel clientPanel;
	private JPanel openGamesPanel;
	private JButton shutDown;
	private JPanel shutDownPanel;
	private JPanel menuPanel;
	private JButton oldGames;
	private JButton highscore;
	private JLabel whosTurn;
	private JLabel allClientsLabel;
	private JLabel openGamesLabel;
	private JLabel messageLabel;
	private DefaultListModel<String> allClientsList;
	private DefaultListModel<String> openGamesList;
	private JList<String> allClientsJList;
	private JList<String> openGamesJList;
	private JScrollPane openGamesScrollPane;
	private JScrollPane allClientsScrollPane;
	private ServerInterface serverInterface;
	private String[] userString;
	private String[] openGamesArray;
	private JPopupMenu menu;
	private JMenuItem menuItemKickPlayer;

	
	public ServerStartFrame(ServerInterface serverInterface){
		this.serverInterface = serverInterface;
		setTitle("Server");
		GridLayout MyLayout = new GridLayout(2, 5, 10, 10);
		setLayout(MyLayout);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		sendMessage = new JButton("send");
		messageLabel = new JLabel("Message to all Clients");
		sendMessage.addActionListener(this);
		serverMessage = new JTextField();
		serverMessage.addActionListener(this);
		messagePanel = new JPanel();
		messagePanel.setLayout(new BoxLayout(messagePanel, BoxLayout.Y_AXIS));
		messagePanel.add(messageLabel);
		messagePanel.add(serverMessage);
		messagePanel.add(sendMessage);
		add(messagePanel);
		clientPanel = new JPanel();
		clientPanel.setLayout(new BoxLayout(clientPanel, BoxLayout.Y_AXIS));
		menuPanel = new JPanel();
		openGamesPanel = new JPanel();
		openGamesPanel.setLayout(new BoxLayout(openGamesPanel, BoxLayout.Y_AXIS));
		menuPanel.setLayout(new BoxLayout(menuPanel, BoxLayout.Y_AXIS));
		oldGames = new JButton("Old Games");
		oldGames.addActionListener(this);
		highscore = new JButton("Highscore");
		highscore.addActionListener(this);
		menuPanel.add(highscore);
		menuPanel.add(oldGames);
		whosTurn = new JLabel();
		shutDown = new JButton("Shutdown server");
		shutDownPanel = new JPanel();
		shutDown.addActionListener(this);
		shutDown.setBorderPainted(true);
		shutDownPanel.add(shutDown);
		shutDown.setBackground(new Color(250, 0, 0));
		allClientsLabel = new JLabel("Clients online");
		openGamesLabel = new JLabel("Open Games");
		allClientsList = new DefaultListModel<String>();
		openGamesList = new DefaultListModel<String>();
		allClientsJList = new JList<String>(allClientsList);
		allClientsJList.addMouseListener(this);
		
		menu = new JPopupMenu();
		menuItemKickPlayer = new JMenuItem("Kick player");
		menuItemKickPlayer.addActionListener(this);
		menu.add(menuItemKickPlayer);
		
		openGamesJList = new JList<String>(openGamesList);
		allClientsScrollPane = new JScrollPane(allClientsJList);
		openGamesScrollPane = new JScrollPane(openGamesJList);
		clientPanel.add(allClientsLabel);
		clientPanel.add(allClientsScrollPane);
		openGamesPanel.add(openGamesLabel);
		openGamesPanel.add(openGamesScrollPane);
		
		
		add(openGamesPanel);
		add(clientPanel);
		add(menuPanel);
		add(whosTurn);
		add(shutDownPanel);
		pack();
		setVisible(true);

	}
	/**
	 * Gets Array containing all online Clients from Server. Adds them to JList
	 */
	public void displayClients(){
		userString = serverInterface.getAllUsernames();
		allClientsList.removeAllElements();
		for(int i=0; i<userString.length; i++){
			addClient(userString[i]);
		}
	}
	/**
	 * Reads ArrayList containing open Games from Server. Adds them to JList
	 */
	public void displayOpenGames(){
		openGamesArray = serverInterface.getAllGameNames();
		openGamesList.removeAllElements();
		for(int i=0; i<openGamesArray.length; i++){
			openGamesList.addElement(openGamesArray[i].toString());
		}
	}
	
	public void addClient(String name){
		allClientsList.addElement(name);
	}
	
	public void addOpenGame(String name){
		openGamesList.addElement(name);
		openGamesJList.updateUI();
	}
	
	public void removeClient(String name){
		for(int i=0; i<allClientsList.getSize(); i++){
			if(allClientsList.getElementAt(i).equals(name)){
				allClientsList.remove(i);
				allClientsJList.updateUI();
			}
		}
	}
	
	public void removeOpenGame(String name){
		for(int i=0; i<openGamesList.getSize()-1; i++){
			if(openGamesList.getElementAt(i).equals(name)){
				openGamesList.remove(i);
				openGamesJList.updateUI();
			}
		}
	}
	
	/**
	 * Implements ActionListener
	 */
	public void actionPerformed(ActionEvent event){
		if(event.getSource()==shutDown){
			System.out.println("Server shut down!"); //this can stay
			serverInterface.sendServerMessage("Server shut down.");
			System.exit(0);
		} 
		else if(event.getSource()==sendMessage){
			String msg = serverMessage.getText();
			serverInterface.sendServerMessage(msg);
			serverMessage.setText("");
		}
		else if(event.getSource()==highscore){
				HighscoreFrame hsf = new HighscoreFrame();
				hsf.setVisible(true);
			}
		else if(event.getSource()==oldGames){
			OldGamesFrame ogl = new OldGamesFrame(serverInterface);
			ogl.setVisible(true);
		}
		else if (event.getSource() == serverMessage) {
			String msg = serverMessage.getText();
			serverInterface.sendServerMessage(msg);
			serverMessage.setText("");
		}
		else if (event.getSource() == menuItemKickPlayer) {
			serverInterface.kickPlayer(allClientsJList.getSelectedValue());
			serverInterface.sendServerMessage("Kicked player: " + allClientsJList.getSelectedValue());
		}
	}
	
	/**
	 * Updates OpenGames and Client ScrollPane. Called in case of client login or new game created
	 */
	public void update() {
		displayOpenGames();
		displayClients();
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			int clickedElement = allClientsJList.locationToIndex(e.getPoint());
			allClientsJList.setSelectedIndex(clickedElement);
			if (allClientsJList.getSelectedIndex() != -1) {
				menu.show(e.getComponent(), e.getX(), e.getY());
			}
		}
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {}
	@Override
	public void mouseExited(MouseEvent arg0) {}
	@Override
	public void mousePressed(MouseEvent arg0) {}
	@Override
	public void mouseReleased(MouseEvent arg0) {}
}
