package gui.lobby;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import network.Client;

public class PlayerListListener implements MouseListener, ActionListener{

	private static final String CHANGE_NAME = "Change name";
	private static final String WHISPER = "Whisper";
	private static final String HIGHSCORE = "Highscore";
	private JPopupMenu popupMenuWhisper;
	private JPopupMenu popupMenuNameChange;
	private JList<String> jList;
	private Client clientTest;
	private LobbyGui lobbyGui;
	private JMenuItem menuItemWhisper;
	private JMenuItem menuItemNameChange;
	private JMenuItem menuItemHighscoreWhisper;
	private JMenuItem menuItemHighscoreNameChange;
	
	PlayerListListener(JList<String> jList, Client clientTest, LobbyGui lobbyGui) {
		/*
		 * Der Konstruktor setzt das Individuelle Popupmenu um
		 */
		this.lobbyGui = lobbyGui;
		this.clientTest = clientTest;
		this.jList = jList;
		menuItemHighscoreWhisper = new JMenuItem(HIGHSCORE);
		menuItemHighscoreWhisper.addActionListener(this);
		popupMenuWhisper = new JPopupMenu();
		menuItemWhisper = new JMenuItem(WHISPER);
		menuItemWhisper.addActionListener(this);
		popupMenuWhisper.add(menuItemWhisper);
		popupMenuWhisper.add(menuItemHighscoreWhisper);
		menuItemHighscoreNameChange = new JMenuItem(HIGHSCORE);
		menuItemHighscoreNameChange.addActionListener(this);
		popupMenuNameChange = new JPopupMenu();
		menuItemNameChange = new JMenuItem(CHANGE_NAME);
		menuItemNameChange.addActionListener(this);
		popupMenuNameChange.add(menuItemNameChange);
		popupMenuNameChange.add(menuItemHighscoreNameChange);
		
	}


	/**
	 * opens pupupmenus on rightclick
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			int clickedElement = jList.locationToIndex(e.getPoint());
			jList.setSelectedIndex(clickedElement);
			if (jList.getSelectedIndex() != -1) {
				if (clientTest.isMyName(jList.getSelectedValue())) {
					showNameChangeMenu(e);
				} else {
					showWhisperToMenu(e);
				}
			}
		}
	}

	private void showWhisperToMenu(MouseEvent e) {
		popupMenuWhisper.show(e.getComponent(), e.getX(), e.getY());
	}

	private void showNameChangeMenu(MouseEvent e) {
		popupMenuNameChange.show(e.getComponent(), e.getX(), e.getY());
	}
	
	@Override
	public void mouseEntered(MouseEvent arg0) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {

	}

	@Override
	public void mousePressed(MouseEvent arg0) {

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == menuItemWhisper) {
			whisper();
		} else if (e.getSource() == menuItemNameChange) {
			lobbyGui.showNameChangeDialog();
		} else if (e.getSource() == menuItemHighscoreNameChange || e.getSource() == menuItemHighscoreWhisper) {
			clientTest.highscoreRequested(jList.getSelectedValue());
		}
	}
	
	private void whisper() {
		String username = lobbyGui.getSelectedIndex();
		lobbyGui.setText("/w " + username + " ");
	}
}