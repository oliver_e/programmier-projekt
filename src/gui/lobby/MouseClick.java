package gui.lobby;

import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

/** recognizes mouseclicks on jlists and open popupmenus
 */
public class MouseClick implements MouseListener{

	private Vector<JMenuItem> a;
	private JPopupMenu popupMenu;
	private JList<String> jList;
	private ActionListener popupListener;
	
	MouseClick(JList<String> jList, List<String> popupList, LobbyGui lobbyGui) {
		/*
		 * Der Konstruktor setzt das Individuelle Popupmenu um
		 */
		this.jList = jList;
		a = new Vector<JMenuItem>();
		popupMenu = new JPopupMenu();
		popupListener = new PopupListener(lobbyGui);
		for (int i = 0; i < popupList.size(); i++) {
			a.add(new JMenuItem(popupList.get(i)));
			popupMenu.add(a.get(i));
			a.get(i).addActionListener(popupListener);
		}
		
	}

	/*
	 * Wird ausgel�st wenn die Maus gedr�ckt wird und daraufhin wird dann entweder das Popupmenu angezeigt oder
	 * die ausgew�hlte Stelle in der Liste
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			int clickedElement = jList.locationToIndex(e.getPoint());
			jList.setSelectedIndex(clickedElement);
			if (jList.getSelectedIndex() != -1) {
				doPop(e);
			}
		}
	}

	/*
	 * Zeigt das Popupmenu an, an der Stelle der Maus.   
	 */
	private void doPop(MouseEvent e){
		popupMenu.show(e.getComponent(), e.getX(), e.getY());
	}
	
	
	

	@Override
	public void mouseEntered(MouseEvent arg0) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {

	}

	@Override
	public void mousePressed(MouseEvent arg0) {

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {

	}
}