package gui.lobby;

import gui.game.MapPanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import network.client.MapPicture;

@SuppressWarnings("serial")
public class MapPicker extends JDialog implements ActionListener, ListSelectionListener {

	private int mapID = 1;
	
	private LinkedList<MapPackage> mappackages = new LinkedList<MapPackage>();
	private JList<String> mapJList;
	private JTextPane descbox;
	private DefaultListModel<String>  mapList;
	private PreviewMiniMap minimap;
	private JButton okaybutton;
	
	
	/**
	 * Makes the user pick from all available map files.
	 * @param parent The JFrame from which this is called.
	 * @return the mapID or -1 when cancelled
	 */
	public static int pickMap(JFrame parent){
		MapPicker me = new MapPicker(parent);
		me.setLayout(new FlowLayout());
		me.setResizable(false);
		me.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		me.setTitle("Please select a map");
		
		me.mapList = new DefaultListModel<String>();
		int i = 1;
		while (true) { //breaks when no more maps to add
			InputStream resource = ClassLoader.getSystemClassLoader().getResourceAsStream("files/maps/"+ i + ".htgmap");
			if (resource != null){
				BufferedInputStream mapSteam = new BufferedInputStream (resource);
				byte[] data = new byte[10+MapPicture.MAPSIZEX*MapPicture.MAPSIZEY];
				try {
					for (int j = 0; j < data.length; j++) {
						data[j] = (byte) mapSteam.read();
					}
					mapSteam.close();
				} catch (IOException e2) {}
			
				String name = "Map #"+i;
				String desc = "[no description available]";
				InputStream descResource = ClassLoader.getSystemClassLoader().getResourceAsStream("files/maps/"+ i + ".txt");
				if (descResource != null){
					try {
						BufferedReader descReader = new BufferedReader(new InputStreamReader(descResource));
						name = descReader.readLine();
						desc = descReader.readLine();
					} catch (FileNotFoundException e1) {} catch (IOException e) {}
				}

				me.mappackages.add(me.new MapPackage(i, data, name, desc));
				me.mapList.addElement(name);
			} else {
				break;
			}
			i++;
		}
		me.mapJList = new JList<String>(me.mapList);
		me.mapJList.setSelectedIndex(0);
		me.mapJList.setPreferredSize(new Dimension(200, 360));
		me.mapJList.updateUI();
		me.mapJList.addListSelectionListener(me);
		
		me.descbox = new JTextPane();
		me.descbox.setEditable(false);
		me.descbox.setText(me.getByName(me.mapJList.getSelectedValue()).desc);
		JScrollPane scrolldescbox = new JScrollPane(me.descbox);
		
		me.minimap = readPreviewMiniMapFromByteArray(me.mappackages.getFirst().data, me);
		
		me.okaybutton = new JButton("Okay");
		me.okaybutton.setPreferredSize(new Dimension(200, 30));
		me.okaybutton.addActionListener(me);
		
		JPanel mapDisplayPanel = new JPanel();
		mapDisplayPanel.setLayout(new BoxLayout(mapDisplayPanel, BoxLayout.Y_AXIS));
		JPanel mapInputPanel = new JPanel();
//		mapInputPanel.setLayout(new BoxLayout(mapInputPanel, BoxLayout.Y_AXIS));
		
		mapInputPanel.setPreferredSize(new Dimension(200, 400));
		
		mapInputPanel.add(me.mapJList);
		
		mapDisplayPanel.setPreferredSize(new Dimension(me.minimap.getPreferredSize().width, mapInputPanel.getPreferredSize().height));
		me.descbox.setPreferredSize(new Dimension(me.minimap.getPreferredSize().width, mapInputPanel.getPreferredSize().height - me.minimap.getPreferredSize().height));
		
		mapDisplayPanel.add(me.minimap);
		mapDisplayPanel.add(scrolldescbox);
		
		mapInputPanel.add(me.okaybutton);
		
		me.add(mapInputPanel);
		me.add(mapDisplayPanel);
		
		me.addWindowListener(me.new MyWindowListener());
		me.pack();
		me.setVisible(true); //will wait here until window closed
		
		return me.mapID;
		
	}
	
	public MapPicker(JFrame parent) {
		super(parent, JDialog.DEFAULT_MODALITY_TYPE);
	}
	
	private MapPackage getByName(String name){
		for (MapPackage mapPackage : mappackages) {
			if (mapPackage.name.equals(name)){
				return mapPackage;
			}
		}
		return null;
	}
	
	private void updateDisplay(String name) {
		MapPackage newSet = getByName(name);
		updatePreviewMiniMapFromByteArray(minimap, newSet.data, this);
		descbox.setText(newSet.desc);
		mapID = newSet.id;
		minimap.repaint();
	}
	
	public static PreviewMiniMap readPreviewMiniMapFromByteArray(byte[] data, MapPicker me) {
		byte[][] mapArray = new byte[MapPicture.MAPSIZEX][MapPicture.MAPSIZEY];
		
		for (int i = 0; i < data.length - 10; i++) {
			mapArray[i%MapPicture.MAPSIZEX][i/MapPicture.MAPSIZEX] = data[i + 10];
		}
		
		return me.new PreviewMiniMap(mapArray, data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9]);
	}
	
	public static void updatePreviewMiniMapFromByteArray(PreviewMiniMap input, byte[] data, MapPicker me) {
		byte[][] mapArray = new byte[MapPicture.MAPSIZEX][MapPicture.MAPSIZEY];
		
		for (int i = 0; i < data.length - 10; i++) {
			mapArray[i%MapPicture.MAPSIZEX][i/MapPicture.MAPSIZEX] = data[i + 10];
		}
		
		input.update(mapArray, data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9]);
	}
	
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Okay")) {
			dispose();
		}
		
	}
	
	//Testing
	public static void main(String[] args) {
		System.out.println("Testrun: waiting for entry...");
		System.out.println("The entry was " + pickMap(null));
	}
	
	private class MyWindowListener extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			mapID = -1;
			dispose();
		}
	}
	
	private class MapPackage {
		public int id;
		public byte[] data;
		public String name;
		public String desc;
		public MapPackage(int id, byte[] data, String name, String desc) {
			this.id = id;
			this.data = data;
			this.name = name;
			this.desc = desc;
		}
	}
	
	private class PreviewMiniMap extends JPanel {

		private BufferedImage image;
		private int grailX;
		private int grailY;
		private int spawn1X;
		private int spawn1Y;
		private int spawn2X;
		private int spawn2Y;
		private int goal1X;
		private int goal1Y;
		private int goal2X;
		private int goal2Y;
		
		public PreviewMiniMap(byte[][] mapArray, int grailX, int grailY, int spawn1X, int spawn1Y, int spawn2X, int spawn2Y, int goal1X, int goal1Y, int goal2X, int goal2Y) {
			setPreferredSize(new Dimension(2*MapPicture.MAPSIZEX, 2*MapPicture.MAPSIZEY));
			update(mapArray, grailX, grailY, spawn1X, spawn1Y, spawn2X, spawn2Y, goal1X, goal1Y, goal2X, goal2Y);
		}
		
		
		@Override
		public void paint(Graphics g) {
			g.drawImage(image, 0, 0, null);
			g.setColor(new Color(255, 216, 0));
			g.fillRect(2*grailX, 2*grailY, 2, 2);
			g.setColor(new Color(0, 38, 255));
			g.fillRect(2*spawn1X, 2*spawn1Y, 2, 2);
			g.setColor(new Color(255, 0, 0));
			g.fillRect(2*spawn2X, 2*spawn2Y, 2, 2);
			g.setColor(new Color(0, 148, 255));
			g.fillRect(2*goal1X, 2*goal1Y, 2, 2);
			g.setColor(new Color(255, 106, 0));
			g.fillRect(2*goal2X, 2*goal2Y, 2, 2);
		}
		
		public void update(byte[][] mapArray, int grailX, int grailY, int spawn1X, int spawn1Y, int spawn2X, int spawn2Y, int goal1X, int goal1Y, int goal2X, int goal2Y) {
			image = new BufferedImage(2*MapPicture.MAPSIZEX, 2*MapPicture.MAPSIZEY, BufferedImage.TYPE_INT_RGB);
			int rgb = 0;
			for (int i = 0; i < MapPicture.MAPSIZEY; i++) {
				for (int j = 0; j < MapPicture.MAPSIZEX; j++) {	
					switch (MapPanel.terrainTypeToTileID(mapArray[i][j])) {
					case MapPanel.GRASS:
						rgb = Integer.parseInt("007F0E", 16);
						break;
					case MapPanel.WATER:
						rgb = Integer.parseInt("00137F", 16);
						break;
					case MapPanel.PAVEMENT:
						rgb = Integer.parseInt("808080", 16);
						break;
					case MapPanel.STONEFLOOR:
						rgb = Integer.parseInt("000000", 16);
						break;
					case MapPanel.BRIDGE:
						rgb = Integer.parseInt("7F3300", 16);
						break;
					case MapPanel.MOUNTAIN:
						rgb = Integer.parseInt("000000", 16);
						break;
					case MapPanel.WALL:
						rgb = Integer.parseInt("404040", 16);
						break;
					case MapPanel.MUD:
						rgb = Integer.parseInt("7F1C00", 16);
						break;
					case MapPanel.SAND:
						rgb = Integer.parseInt("7F6A00", 16);
						break;
					default:
						rgb = Integer.parseInt("808080", 16);
						break;
					}
					image.setRGB(2*i, 2*j, rgb);
					image.setRGB(2*i, 2*j+1, rgb);
					image.setRGB(2*i+1, 2*j, rgb);
					image.setRGB(2*i+1, 2*j+1, rgb);
				}
			}
			this.grailX = grailX;
			this.grailY = grailY;
			this.spawn1X = spawn1X;
			this.spawn1Y = spawn1Y;
			this.spawn2X = spawn2X;
			this.spawn2Y = spawn2Y;
			this.goal1X = goal1X;
			this.goal1Y = goal1Y;
			this.goal2X = goal2X;
			this.goal2Y = goal2Y;
		}
		
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		updateDisplay(mapJList.getSelectedValue());
	}

}
