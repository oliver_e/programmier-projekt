package gui.lobby;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Arrays;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.text.DefaultCaret;

import network.Client;

/** creates lobby gui and offers methods to communicate with the user
 */
public class LobbyGui implements ActionListener{
	private JFrame lobbyFrame;
	private JPanel mainPanel;
	private JList<String> openGamesJList;
	private DefaultListModel<String> openGamesList;
	private DefaultListModel<String> playerList;
	private JList<String> playerJList;
	private JTextArea chatArea;
	private JLabel jpLeftLabel;
	private JPanel openGamesPanel;
	private DefaultListModel<String> listTeam1;
	private DefaultListModel<String> listTeam2;
	private Client clientTest;
	private JButton buttonJoinGame;
	private JButton buttonCreateGame;
	private JButton buttonStartGame;
	private JButton buttonLeaveGame;
	private JPanel buttonPanelOpenGames;
	private JScrollPane scrollpaneOpenGames;
	private JTextField chatInput;
	private GuiThread guiThread;
	private boolean usernameWasAlreadyGiven;
	private boolean showNoBalancedTeamMessage;
	private boolean showHighscore;
	private String highscoreName;
	private int highscoreValue;

	/** this thread shows warnings and notifications which are triggered from client, so that the serverlistener wont be blocked.
	 */
	public class GuiThread extends Thread {;

		public GuiThread() {
		}
		
		@Override
		public void run() {
			while (true) {
				if (usernameWasAlreadyGiven) {
					usernameWasAlreadyGiven = false;
					JOptionPane.showMessageDialog(null, "Username was already given!",
							"", JOptionPane.WARNING_MESSAGE);
					showNameChangeDialog();
				} else if (showNoBalancedTeamMessage) {
					showNoBalancedTeamMessage = false;
					JOptionPane.showMessageDialog(null, "You can only start game with balanced teams", "Couldnt start game", JOptionPane.WARNING_MESSAGE);
				} else if (showHighscore) {
					showHighscore = false;
					JOptionPane.showMessageDialog(null, highscoreName + "'s highscore: " + highscoreValue, "Highscore", JOptionPane.WARNING_MESSAGE);
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	
	/** puts components into lobby frame
	 * @param width
	 * @param height
	 * @param clientTest
	 */
	public LobbyGui(int width, int height, Client clientTest) {
		
		this.clientTest = clientTest;
		
		guiThread = new GuiThread();
		guiThread.start();
		
		
		lobbyFrame = new JFrame("Gamelobby");
		lobbyFrame.addWindowListener(new OurWindowListener(clientTest));
		lobbyFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		lobbyFrame.setPreferredSize(new Dimension(width, height));
		lobbyFrame.setMinimumSize(new Dimension(800, 400));
		lobbyFrame.setLayout(new BorderLayout());
		mainPanel = new JPanel();
		lobbyFrame.add(mainPanel, BorderLayout.CENTER);
		mainPanel.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.BOTH;
		c.gridy = 0;
		c.gridx = 0;
		c.weightx = c.weighty = 0.5;
		c.ipady = 0;
		openGamesPanel = new JPanel();
		openGamesPanel.setLayout(new BorderLayout());
		openGamesList = new DefaultListModel<String>();
		openGamesJList = new JList<String>(openGamesList);

		scrollpaneOpenGames = new JScrollPane(openGamesJList);
		jpLeftLabel = new JLabel("Games open");
		openGamesPanel.add(jpLeftLabel, BorderLayout.PAGE_START);

		buttonPanelOpenGames = new JPanel(new GridLayout(2, 1));
		buttonJoinGame = new JButton("Join game");
		buttonJoinGame.addActionListener(this);
		buttonCreateGame = new JButton("Create game");
		buttonCreateGame.addActionListener(this);
		buttonPanelOpenGames.add(buttonJoinGame);
		buttonPanelOpenGames.add(buttonCreateGame);
		openGamesPanel.add(buttonPanelOpenGames, BorderLayout.PAGE_END);

		openGamesPanel.add(scrollpaneOpenGames, BorderLayout.CENTER);

		mainPanel.add(openGamesPanel, c);

		c.fill = GridBagConstraints.BOTH;
		c.gridy = 0;
		c.gridx = 1;
		c.gridwidth = 2;
		c.weightx = 1.3;
		JPanel chatPanel = new JPanel();
		chatPanel.setLayout(new BorderLayout());
		chatArea = new JTextArea();
		((DefaultCaret)chatArea.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		chatArea.setEditable(false);
		JScrollPane scrollPaneChat = new JScrollPane(chatArea);
		chatPanel.add(scrollPaneChat, BorderLayout.CENTER);
		chatPanel.add(new JLabel("Chat"), BorderLayout.PAGE_START);
		chatInput = new JTextField();
		chatInput.addKeyListener(new MyChatKeyListener());
		chatInput.addActionListener(this);
		chatPanel.add(chatInput, BorderLayout.PAGE_END);
		mainPanel.add(chatPanel, c);

		c.fill = GridBagConstraints.BOTH;
		c.gridy = 0;
		c.gridx = 3;
		c.gridwidth = 1;
		c.weightx = 0.5;
		playerList = new DefaultListModel<String>();
		playerJList = new JList<String>(playerList);
		JPanel playerListPanel = new JPanel();
		playerListPanel.setLayout(new BorderLayout());
		JScrollPane scrollPanePlayerList = new JScrollPane(playerJList);
		playerListPanel.add(new JLabel("Players online"),
				BorderLayout.PAGE_START);
		playerListPanel.add(scrollPanePlayerList, BorderLayout.CENTER);
		mainPanel.add(playerListPanel, c);

		/*
		 * Die Funktion die die Maus beachtet und in welchem Fenster sie ist.
		 * Wichtig ist die Arrays.asList hier gibt man die Reiter im Popupmenu
		 * an
		 */
		
		openGamesJList.addMouseListener(new MouseClick(openGamesJList, Arrays
				.asList("Create game", "Join game"), this));
		playerJList.addMouseListener(new PlayerListListener(playerJList, clientTest, this));
		playerJList.setCellRenderer(new DefaultListCellRenderer() {
			
			private static final long serialVersionUID = 1L;
			private Client clientTest;
			
			public Component getListCellRendererComponent(javax.swing.JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				Component ret = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				
			    if (clientTest.isMyName((String)value)) {
			      ret.setBackground(new Color(255,215,0));
			    }

			    return ret;
			};

			private ListCellRenderer<? super String> initiate(Client clientTest) {
				this.clientTest = clientTest;
				return this;
			}
		}.initiate(clientTest));
		
		//fix layout bug
		openGamesList.addElement("");
		playerList.addElement("");

		lobbyFrame.pack();
		lobbyFrame.setVisible(true);
	}

	public void addGame(String gameName) {
		if (openGamesList.contains("")) {
			openGamesList.remove(0);
		}
		openGamesList.addElement(gameName);
		openGamesJList.updateUI();
	}

	public void removeGame(String gameName) {
		for (int i = 0; i < openGamesList.getSize(); i++) {
			if (openGamesList.getElementAt(i).equals(gameName)) {
				openGamesList.remove(i);
				if (openGamesList.isEmpty()) {
					openGamesList.addElement("");
				}
				return;
			}
		}
	}

	public void addPlayer(String playerName) {
		playerList.addElement(playerName);
	}

	public void removePlayer(String playerName) {
		for (int i = 0; i < playerList.getSize(); i++) {
			if (playerList.getElementAt(i).equals(playerName)) {
				playerList.remove(i);
				return;
			}
		}
	}

	public void addChatMessage(String message) {
		chatArea.append(message + System.getProperty("line.separator"));
	}

	/**
	 * Call this if you joined a game to show players of each team
	 */
	public void changeToJoinedGameMode(String[] playersOfTeam1,
			String[] playersOfTeam2) {
		openGamesPanel.removeAll();
		JPanel jpTeams = new JPanel();
		JPanel jpTeam1 = new JPanel();
		JPanel jpTeam2 = new JPanel();

		jpTeams.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.weighty = c.weightx = 1;
		jpTeams.add(jpTeam1, c);
		c.gridy = 1;
		jpTeams.add(jpTeam2, c);

		openGamesPanel.add(jpTeams, BorderLayout.CENTER);
		JLabel jlTeam1 = new JLabel("Team Blue");
		jlTeam1.setOpaque(true);
		jlTeam1.setBackground(new Color(76,162,255));
		JLabel jlTeam2 = new JLabel("Team Red");
		jlTeam2.setOpaque(true);
		jlTeam2.setBackground(Color.RED);
		JList<String> jlistTeam1 = new JList<String>();
		JList<String> jlistTeam2 = new JList<String>();
		listTeam1 = new DefaultListModel<String>();

		if (playersOfTeam1 != null) {
			for (String player : playersOfTeam1) {
				listTeam1.addElement(player);
			}
		}

		listTeam2 = new DefaultListModel<String>();

		if (playersOfTeam2 != null) {
			for (String player : playersOfTeam2) {
				listTeam2.addElement(player);
			}
		}

		jlistTeam1.setModel(listTeam1);
		jlistTeam2.setModel(listTeam2);

		jpTeam1.setLayout(new BorderLayout());
		jpTeam1.add(jlTeam1, BorderLayout.PAGE_START);
		jpTeam1.add(jlistTeam1, BorderLayout.CENTER);

		jpTeam2.setLayout(new BorderLayout());
		jpTeam2.add(jlTeam2, BorderLayout.PAGE_START);
		jpTeam2.add(jlistTeam2, BorderLayout.CENTER);
		
		JPanel buttonPanel = new JPanel(new GridLayout(2, 1));
		buttonStartGame = new JButton("Start game");
		buttonStartGame.addActionListener(this);
		buttonLeaveGame = new JButton("Leave game");
		buttonLeaveGame.addActionListener(this);
		buttonPanel.add(buttonStartGame);
		buttonPanel.add(buttonLeaveGame);
		
		openGamesPanel.add(buttonPanel, BorderLayout.PAGE_END);
		
		lobbyFrame.pack();
	}

	public void addPlayerToTeam1(String player) {
		listTeam1.addElement(player);
	}

	public void addPlayerToTeam2(String player) {
		listTeam2.addElement(player);
	}
	
	public void removePlayerFromTeam1(String player) {
		for (int i = 0; i < listTeam1.getSize(); i++) {
			if (listTeam1.getElementAt(i).equals(player)) {
				listTeam1.remove(i);
				return;
			}
		}
	}

	public void removePlayerFromTeam2(String player) {
		for (int i = 0; i < listTeam2.getSize(); i++) {
			if (listTeam2.getElementAt(i).equals(player)) {
				listTeam2.remove(i);
				return;
			}
		}
	}

	public void changeToNotInGameMode() {
		openGamesPanel.removeAll();
		openGamesPanel.setLayout(new BorderLayout());
		openGamesPanel.add(jpLeftLabel, BorderLayout.PAGE_START);
		openGamesPanel.add(scrollpaneOpenGames, BorderLayout.CENTER);
		openGamesPanel.add(buttonPanelOpenGames, BorderLayout.PAGE_END);
		lobbyFrame.pack();
	}

	public void deleteAllPlayers() {
		playerList.removeAllElements();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == buttonCreateGame) {
			askForGameNameAndMapIDAndCreateGame();
		} else if (arg0.getSource() == buttonJoinGame) {
			joinGame();
		} else if (arg0.getSource() == buttonStartGame) {
			clientTest.startButtonPressed();
		} else if (arg0.getSource() == buttonLeaveGame) {
			clientTest.leaveButtonPressed();
		} else if (arg0.getSource() == chatInput) {
			String msg = chatInput.getText();
			if (msg.length() > 0) {
				clientTest.chatMessageEntered(msg);
				chatInput.setText("");
			}
		}
	}
	
	void joinGame() {
		//gamename (1|1)
		String gameName = null;
		try {
			gameName = openGamesJList.getSelectedValue().split(" ")[0];
		} catch (NullPointerException e1) {
			// no game selected
			JOptionPane.showMessageDialog(null, "Please select a game before joining.", "Message", JOptionPane.WARNING_MESSAGE);
			return;
		}
		int teamNumber;
		while (true) { //TODO
			Object[] options = {"Blue", "Red"};
            int n = JOptionPane.showOptionDialog(lobbyFrame,
                            "What team do you want to join?",
                            "Hail The Grail",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            options,
                            options[0]);
            if (n == JOptionPane.YES_OPTION) {
            	teamNumber = 1;
            } else if (n == JOptionPane.NO_OPTION) {
            	teamNumber = 2;
            } else {
            	return;
            }
            clientTest.joinGame(gameName, teamNumber);
			return;
		}
	}

	
	public void askForGameNameAndMapIDAndCreateGame() {
		String gameName = "";
		
		while (gameName.equals("")) {
			gameName = JOptionPane.showInputDialog(null, "", "Enter game-name",
					JOptionPane.QUESTION_MESSAGE);
			if (gameName == null) {
				return;
			}
			if (gameName.equals("")) {
				JOptionPane.showMessageDialog(null, "Pls enter a correct gamename", "",
						JOptionPane.ERROR_MESSAGE);
			}
			
		}
		int mapID;
		
		mapID = MapPicker.pickMap(this.lobbyFrame);
		
		if (mapID == -1) {
			return;
		}
		
		clientTest.createGame(gameName, mapID);
	}

	public void askForNewGameName() {
		JOptionPane.showMessageDialog(null, "Your gamename was already taken.", "Message", JOptionPane.WARNING_MESSAGE);
		askForGameNameAndMapIDAndCreateGame();
	}

	public void updateOpenGames(String[] openGameNames) {
		openGamesList.removeAllElements();
		for (String openGame : openGameNames) {
			openGamesList.addElement(openGame);
		}
	}

	public void updatePlayersInGame(String[] team1, String[] team2) {
		listTeam1.removeAllElements();
		listTeam2.removeAllElements();
		for (String player : team1) {
			listTeam1.addElement(player);
		}
		for (String player : team2) {
			listTeam2.addElement(player);
		}
		
	}

	public void hide() {
		lobbyFrame.setVisible(false);
		
	}

	public void setVisible(boolean b) {
		lobbyFrame.setVisible(b);
	}

	public void showNoBalancedTeamsMessage() {
		showNoBalancedTeamMessage = true;
	}
	
	public void setText(String text){
		chatInput.setText(text);
		chatInput.requestFocus();
	}
	
	public String getSelectedIndex() {
		return playerJList.getSelectedValue();
	}
	
	
	private class MyChatKeyListener implements KeyListener {

		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER){
				e.consume();
				if (chatInput.getText().length() > 0) {
					clientTest.clientInterface.sendMessage(chatInput.getText());
					addOwnMessage(chatInput.getText());
					chatInput.setText("");
				}
			}
		}

		@Override
		public void keyReleased(KeyEvent e) {}

		@Override
		public void keyTyped(KeyEvent e) {}
		
	}


	public Client getClientTest() {
		return clientTest;
	}
	
	private void addOwnMessage(String msg) {
		// whisper
		if (msg.startsWith("/w ") && msg.length() > 5) {
			String receivingPerson = null;
			try {
				receivingPerson = msg.split(" ")[1];
				msg = msg.substring(receivingPerson.length() + 4, msg.length());
			} catch (StringIndexOutOfBoundsException e) {
				//server sends usage automatically
				return;
			}
			addChatMessage("[Private] (to " + receivingPerson + ") " + msg);
		}
		// game
		else if (msg.startsWith("/g ")) {
			addChatMessage("[Game] " + msg.substring(3));
		}
		// team
		else if (msg.startsWith("/t ")) {
			addChatMessage("[Team] " + msg.substring(3));
			
		} else {
			addChatMessage("[Global] " + msg);
		}
	}

	public void showNameChangeDialog() {
		while (true) {
			String newName = JOptionPane.showInputDialog(null, "",
					"Enter new Name", JOptionPane.QUESTION_MESSAGE);
			if (newName == null) {
				return;
			} else if (newName.equals("")) {
				JOptionPane.showMessageDialog(null, "Type in a correct name",
						"", JOptionPane.WARNING_MESSAGE);
			} else {
				clientTest.changeName(newName);
				break;
			}
		}
	}

	public void showUsernameWasAlreadyGivenDialogAndAskForNewOne() {
		usernameWasAlreadyGiven = true;
	}

	public void showHighscore(String name, int value) {
		showHighscore = true;
		highscoreName = name;
		highscoreValue = value;
	}
	

}
