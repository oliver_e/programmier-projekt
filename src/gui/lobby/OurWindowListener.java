package gui.lobby;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import network.Client;

/** tells client that lobby has been closed if X is pressed
 */
public class OurWindowListener implements WindowListener {

	private Client client;

	public OurWindowListener(Client clientTest) {
		this.client = clientTest;
	}

	@Override
	public void windowOpened(WindowEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {
		client.clientClosedLobby();
	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	@Override
	public void windowActivated(WindowEvent e) {

	}

	@Override
	public void windowDeactivated(WindowEvent e) {

	}

}
