package gui.lobby;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/** recognizes mousclicks on popupmenus
 */
public class PopupListener implements ActionListener {

	private LobbyGui lobbyGui;

	public PopupListener(LobbyGui lobbyGui) {
		this.lobbyGui = lobbyGui;
	}

	@Override
	public void actionPerformed(ActionEvent actionEvent) {

		if (actionEvent.getActionCommand().equals("Join game")){
			lobbyGui.joinGame();
		} else if (actionEvent.getActionCommand().equals("Create game")){
			lobbyGui.askForGameNameAndMapIDAndCreateGame();
		}
		
	}	
}
