package gui.client;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import startup.Main;
import network.Client;

public class ClientStartFrame extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private JTextField tfServerIP;
	private JTextField tfServerPort;
	private JTextField tfUsername;
	private JButton bLogin;
	private Client clientTest;

	public ClientStartFrame(final Client clientTest) {
		this.clientTest = clientTest;
		ButtonAndTextFieldListener ourListener = new ButtonAndTextFieldListener();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel jPanel = new JPanel();
		jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.Y_AXIS));
		add(jPanel, BorderLayout.CENTER);
		jPanel.add(new JLabel("Server port:"));
		tfServerPort = new JTextField();
		if (Main.port > 0) {
			tfServerPort.setText("" + Main.port);
		}
		tfServerPort.addActionListener(ourListener);
		jPanel.add(tfServerPort);
		jPanel.add(new JLabel("Server IP:"));
		tfServerIP = new JTextField();
		if (Main.ip != null) {
			tfServerIP.setText("" + Main.ip);
		}
		tfServerIP.addActionListener(ourListener);
		jPanel.add(tfServerIP);
		jPanel.add(new JLabel("Your username:"));
		tfUsername = new JTextField();
		tfUsername.setText(System.getProperty("user.name").replaceAll(" ", "_"));
		tfUsername.addActionListener(ourListener);
		jPanel.add(tfUsername);
		bLogin = new JButton("Login");
		bLogin.addActionListener(new ButtonAndTextFieldListener());
		add(bLogin, BorderLayout.PAGE_END);
		
		pack();
	}
	
	public class ButtonAndTextFieldListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			//login button pressed
			tryToLogin();
		}

		private void tryToLogin() {
			int serverPort;
			try {
				serverPort = Integer.parseInt(tfServerPort.getText());
			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(new JFrame(),
					    "Please type in a port NUMBER",
					    "",
					    JOptionPane.ERROR_MESSAGE);
				return;
			}
			if (tfUsername.getText().length() == 0) {
				JOptionPane.showMessageDialog(new JFrame(),
					    "Please type in a username",
					    "",
					    JOptionPane.ERROR_MESSAGE);
				return;
			}
			clientTest.loginButtonPressed(serverPort, tfServerIP.getText(), tfUsername.getText().replaceAll(" ", "_"));
		}
		
	}

	public void requestNewServerPort() {
		setVisible(true);
		JOptionPane.showMessageDialog(new JFrame(),
			    "Please type in a working Port",
			    "",
			    JOptionPane.ERROR_MESSAGE);
	}

	public void requestNewServerIP() {
		setVisible(true);
		JOptionPane.showMessageDialog(new JFrame(),
			    "Please type in the correct server ip",
			    "",
			    JOptionPane.ERROR_MESSAGE);
	}

	public void requestNewUsername() {
		setVisible(true);
		JOptionPane.showMessageDialog(new JFrame(),
			    "Username already given",
			    "",
			    JOptionPane.ERROR_MESSAGE);
	}
}
